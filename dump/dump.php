<?php
//MySQL connection parameters
$dbhost = 'localhost';
$dbuser = 'root';
$dbpsw = 'H8dh27dqw';
$dbname = 'softgal';


//Connects to mysql server
$connessione = @mysql_connect($dbhost,$dbuser,$dbpsw);

//Set encoding
mysql_query("SET CHARSET utf8");
mysql_query("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");

//Includes class
require_once('FKMySQLDump.php');

$filename = "backup-" . date("d-m-Y") . ".sql";

//Creates a new instance of FKMySQLDump: it exports without compress and base-16 file
$dumper = new FKMySQLDump($dbname,$filename,false,false);

$params = array(
    //'skip_structure' => TRUE, // if set true = only data is in result
    //'skip_data' => TRUE, // if set true = only structure and FKs are in result
);

//Make dump
$dumper->doFKDump($params);

?>
