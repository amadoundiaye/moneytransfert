package com.mobile.softgal.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.Session;


public abstract class BaseActivity extends ActionBarActivity{
    private static final String TAG = "BaseActivity";

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }


    public Session getSession() {
        return SoftgalApplication.getSession();
    }

}
