package com.mobile.softgal.task;

import java.util.ArrayList;

import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;

import com.mobile.softgal.R;
import com.mobile.softgal.activity.MainActivity;
import com.mobile.softgal.utils.MainUtils;

public class LoginTask extends BaseAsyncTask {

	private MainActivity mActivity;
	private String mLogin;
	private String mPassword;
	private TextView mResume;

	public LoginTask(MainActivity activity, TextView resume, String login, String password) {
		// TODO Auto-generated constructor stub
		super(activity);
		this.mActivity = activity;
		this.mLogin = login;
		this.mPassword = password;
		this.mResume = resume;
	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		//MainUtils.log(mPassword);
		return parser.parseJsonConnect(mActivity, mLogin, mPassword);
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		//MainUtils.log(result +"");
		if (result.booleanValue()) {
			if(MainUtils.isConnected(mActivity)){
				MainUtils.executeAsyncTask(new TransactionsTask(mActivity, mResume));
				//MainUtils.log(MainUtils.getCurrentUTC());
			}else{
				mActivity.startActivity(new Intent(mActivity, MainActivity.class));
				mActivity.finish();
			}
		} else {
			mActivity.startActivity(new Intent(mActivity, MainActivity.class));
			mActivity.finish();
		}
		mResume.setText(mResume.getText().toString() + "\n" + MainUtils.getCurrentUTC() + " ==> " + parser.getMessage());
	}
}	
