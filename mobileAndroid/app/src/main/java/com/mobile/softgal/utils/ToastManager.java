package com.mobile.softgal.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

public class ToastManager {

    public static Toast makeText(Context context, int text) {
        return makeText(context, context.getResources().getString(text));
    }

    @SuppressLint("InflateParams")
    public static Toast makeText(Context context, String text) {
        return Toast.makeText(context, text, Toast.LENGTH_LONG);
    }
}