package com.mobile.softgal.datasource;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.mobile.softgal.utils.MainUtils;

public class Session {
	SharedPreferences pref;
	Editor editor;

	private static final String PREF_NAME = "softgal_Preferences";
	private static final String KEY_URL = "urlKey";
	private static final String KEY_TOKEN = "keyToken";
	private static final String KEY_STARTED = "startedKey";
	private static final String KEY_PHONE = "phoneKey";
	private static final String KEY_INDICATIF = "indicatifKey";
	private static final String KEY_REPEAT_TIME = "repeatTimeKey";
	private static final String KEY_REFERENCE = "referenceKey";
	private static final String KEY_MODE = "modeKey";
	public static final String KEY_MODE_DEV = "dev";
	public static final String KEY_MODE_PROD = "prod";
	private static final int PRIVATE_MODE = 0;
	private static final String SEPARATOR = "|";
	private String HEADER_PREFIX = "Guedia ";

	private Context context;

	// Constructor
	public Session(Context context) {
		pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		this.context = context;
	}

	public void setURL(String url) {
		editor = pref.edit();
		editor.putString(KEY_URL, url);
		editor.commit();
	}

	public String getUrl(){
		return pref.getString(KEY_URL, "");
	}

	public void setToken(String token) {
		editor = pref.edit();
		editor.putString(KEY_TOKEN, HEADER_PREFIX + token);
		editor.commit();
	}

	public String getToken(){
		return pref.getString(KEY_TOKEN, "");
	}

	public void setFirstStart(String firstStart) {
		editor = pref.edit();
		editor.putString(KEY_STARTED, firstStart);
		editor.commit();
	}

	public String getPhone(){
		return pref.getString(KEY_PHONE, "");
	}

	public void setPhone(String phone) {
		editor = pref.edit();
		editor.putString(KEY_PHONE, phone);
		editor.commit();
	}

	public String getIndicatif(){
		return pref.getString(KEY_INDICATIF, "");
	}

	public void setIndicatif(String indicatif) {
		editor = pref.edit();
		editor.putString(KEY_INDICATIF, indicatif);
		editor.commit();
	}

	public String getFirstStart(){
		return pref.getString(KEY_STARTED, "");
	}

	public void setRepeatTime(int time) {
		editor = pref.edit();
		editor.putInt(KEY_REPEAT_TIME, time);
		editor.commit();
	}

	public int getRepeatTime(){
		return pref.getInt(KEY_REPEAT_TIME, -1);
	}

	public String getReference(){
		return pref.getString(KEY_REFERENCE, "");
	}

	public void setReference(String reference) {
		editor = pref.edit();
		editor.putString(KEY_REFERENCE, reference);
		editor.commit();
	}

	public String getMode(){
		return pref.getString(KEY_MODE, "");
	}

	public void setMode(String mode) {
		editor = pref.edit();
		editor.putString(KEY_MODE, mode);
		editor.commit();
	}
}