/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mobile.softgal.application;

import android.app.Application;
import android.content.Context;

import com.mobile.softgal.datasource.DataSource;
import com.mobile.softgal.datasource.Session;
import com.mobile.softgal.BaseData.Transactions;

import java.util.List;


public class SoftgalApplication extends Application {

	private static Session session;
	private static DataSource dataSource;
	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		session = new Session(this);
		dataSource = new DataSource(this);
		context = getApplicationContext();
	}

	public static Session getSession() {
		return session;
	}

	public static DataSource getDAO() {
		return dataSource;
	}

	public static List<Transactions> getTransactions() {
		return getDAO().getListTransactions();
	}

	public static Context getContext() {
		// TODO Auto-generated method stub
		return context;
	}
}