package com.mobile.softgal.utils;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.mobile.softgal.BaseData.Transactions;
import com.mobile.softgal.datasource.MySQLiteHelper;

public class UssdUtils {

	private Context context;

	public UssdUtils(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
	}

	private Uri ussdToCallableUri(String ussd) {
		String uriString = "";
		if(!ussd.startsWith("tel:"))
			uriString += "tel:";
		for(char c : ussd.toCharArray()) {
			if(c == '#')
				uriString += Uri.encode("#");
			else
				uriString += c;
		}
		return Uri.parse(uriString);
	}

	public Intent prepareUSSDQuery(String ussd) {
		Uri baseUssd = ussdToCallableUri(ussd);
		MainUtils.log(baseUssd.toString());
		Intent intent = new Intent(Intent.ACTION_CALL, baseUssd);
		return intent;
	}

	public boolean hasSim() {
		TelephonyManager telmgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		int simState = telmgr.getSimState();
		boolean hasSim = false;
		String text = null;
		switch (simState) {
		case TelephonyManager.SIM_STATE_ABSENT:
			text = "Veuillez inserer une carte SIM Orange dans le téléphone";
			break;
		case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
			text = "Carte SIM bloquée avec un code PIN";
			break;
		case TelephonyManager.SIM_STATE_PIN_REQUIRED:
			text = "Code PIN requis";
			break;
		case TelephonyManager.SIM_STATE_PUK_REQUIRED:
			text = "Code PUK requis";
			break;
		case TelephonyManager.SIM_STATE_READY:
			text = null;
			hasSim = true;
			break;
		case TelephonyManager.SIM_STATE_UNKNOWN:
			text = "Veuillez inserer une carte SIM Orange dans le téléphone";
			break;
		}
		if (!hasSim) {
			Toast.makeText(context, text, Toast.LENGTH_LONG).show();
		}
		return hasSim;
	}

}
