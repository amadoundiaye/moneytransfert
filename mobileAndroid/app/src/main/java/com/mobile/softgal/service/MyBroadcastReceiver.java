package com.mobile.softgal.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mobile.softgal.datasource.Session;
import com.mobile.softgal.utils.MainUtils;



/**
 * Created by mac on 18/08/2017.
 */

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.times.ussd.action.REFRESH")){
            MainUtils.log("onReceive ============================" + intent.getStringExtra("message"));
            //MainUtils.log("Reference ============================" + new Session(context).getReference());
            //TransactionsTask.mResume.setText(TransactionsTask.mResume.getText().toString() + "  ==> " + intent.getStringExtra("message"));
            String reference = new Session(context).getReference();
            String message = intent.getStringExtra("message");
            message = message.replace("[", "");
            message = message.replace("]", "");
            message = message.split(",")[0];
            message = message.split("- - -")[0];
            MainUtils.log("ThreatedMessage ============================" + message);

            Intent i = new Intent("ussdResult");
            i.putExtra("message", message);
            i.putExtra("reference", reference);
            context.sendBroadcast(i);


        }
    }


}