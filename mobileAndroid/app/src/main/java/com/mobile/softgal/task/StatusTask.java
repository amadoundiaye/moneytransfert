package com.mobile.softgal.task;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.widget.TextView;

import com.mobile.softgal.activity.MainActivity;
import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.MySQLiteHelper;
import com.mobile.softgal.datasource.Session;
import com.mobile.softgal.utils.MainUtils;
import com.mobile.softgal.utils.UssdUtils;
import com.mobile.softgal.ws.JSONParser;

import static com.mobile.softgal.task.TransactionsTask.mResume;

public class StatusTask extends BaseAsyncTask {

	private JSONParser parser;
	private MainActivity mActivity;
	private Context mContext;
	private String mReference;
	private String mStatus;
	private String mMessage;
	Handler h = new Handler();
	int delay = 60 * 3 * 1000; //60*1 seconds
	Runnable runnable;


	public StatusTask(Context context, String reference, String status, String message) {
		// TODO Auto-generated constructor stub
		super(context);
		this.mContext = context;
		this.mReference = reference;
		this.mStatus = status;
		this.mMessage = message;
	}

	public StatusTask(MainActivity activity, String reference, String status, String message) {
		// TODO Auto-generated constructor stub
		super(activity);
		this.mActivity = activity;
		this.mReference = reference;
		this.mStatus = status;
		this.mMessage = message;
		MainUtils.log("status task");
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		parser = new JSONParser();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		return parser.parseJsonStatus(mActivity, mReference, mStatus, mMessage);
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result.booleanValue()) {
			if(MainUtils.isConnected(mActivity)){
				MainUtils.executeAsyncTask(new TransactionsTask(mActivity, mResume));
			}
		} else {
			h.postDelayed(new Runnable() {
				public void run() {
					runnable = this;
					h.postDelayed(runnable, delay);
					mActivity.startActivity(new Intent(mActivity, MainActivity.class));
					mActivity.finish();
				}
			}, delay);
		}
		mResume.setText(mResume.getText().toString() + "\n" + MainUtils.getCurrentUTC() + " ==> " + parser.getMessage());
	}

}
