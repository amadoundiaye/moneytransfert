package com.mobile.softgal.ws;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.widget.TextView;

import com.mobile.softgal.BaseData.Params;
import com.mobile.softgal.BaseData.Transactions;
import com.mobile.softgal.activity.MainActivity;
import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.MySQLiteHelper;
import com.mobile.softgal.datasource.Session;
import com.mobile.softgal.service.Constant;
import com.mobile.softgal.utils.MainUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mobile.softgal.service.Constant.TRANSACTION_TYPE_STS;
import static com.mobile.softgal.service.Constant.TRANSACTION_TYPE_TRX;
import static com.mobile.softgal.utils.MainUtils.getCurrentUTC;
import static com.mobile.softgal.utils.MainUtils.hmacSha;

public class JSONParser {
	// json tag names
	private static final String TAG_ETAT = "etat";
	private static final String TAG_SIM_NUMBER = "sim_number";
	private static final String TAG_SIM_PASS = "ussdPassword";
	public static final String TAG_VERSION = "version";
	private static final String TAG_UNIQUE_ID = "unique_id";
	private static final String TAG_CODE_USSD = "ussdCode";
	private static final String TAG_TIMER = "timer";
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_TOKEN = "token";
	private static final String TAG_MESSAGE = "message";
	private static final String TAG_CODE = "code";
	private static final String TAG_DATA = "data";
	private static final String TAG_DATE = "dateCreation";
	private static final String TAG_ID = "id";
	private static final String TAG_REFERENCE = "reference";
	private static final String TAG_BENEFICIAIRE_TEL = "beneficiaireTelephone";
	private static final String TAG_BENEFICIAIRE_AMOUNT = "beneficiaireMontant";
	private static final String TAG_TRANSACTION_STATE = "status";

	private String URL_LOGIN = Constant.SOFTGAL_WEBSITE_BASE_URL + "login_check";
	private String URL_TRANSACTIONS = Constant.SOFTGAL_WEBSITE_BASE_URL + "list_pending_transaction";
	private String URL_TRANSACTIONS_STATUS =  Constant.SOFTGAL_WEBSITE_BASE_URL + "validate_transaction";

	private WebServiceHelper serviceHandler;
	private String message;

	// private Activity activity;

	public JSONParser() {
		// TODO Auto-generated constructor stub
		serviceHandler = new WebServiceHelper();
	}

	public ArrayList<Params> parseJsonParams(String jsonStr) {
		MainUtils.log("json = " + jsonStr);
		ArrayList<Params> params = new ArrayList<Params>();
		if (jsonStr != null && !jsonStr.equals("")) {
			/*try {
				JSONObject jsonObj = new JSONObject(jsonStr);
				String result = jsonObj.getString(TAG_RESULT);
				if (result.equals(RESULT_SUCCESS)) {
					JSONArray dataArray = jsonObj.getJSONArray(TAG_DATA);
					for (int i = 0; i < dataArray.length(); i++) {
						JSONObject obj = dataArray.getJSONObject(i);
						Params param = new Params(obj.getString(TAG_TITLE), obj.getString(TAG_VALUE));
						param.add(params);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}*/
		} else {
			MainUtils.log(Log.ERROR, "Get params JSON null");
		}
		return params;
	}

	public boolean parseJsonConnect(Context context, String login, String password) {
		String[] params = new String[2];
		params[0] = login;
		params[1] = password;
		String jsonStr = serviceHandler.makeServiceConnectCall(URL_LOGIN, params);
		MainUtils.log("json = " + jsonStr);
		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);
				Boolean result = jsonObj.getBoolean(TAG_SUCCESS);
				if (result) {
					// parse JSON object here
					message = "Parsing data for user " + login;
					Session session = new Session(context);
					session.setToken(jsonObj.getString(TAG_TOKEN));
					return true;
				} else {
					if (jsonObj.has(TAG_MESSAGE)) {
						message = jsonObj.getString(TAG_MESSAGE);
					}else{
						message = "Error Parsing data for user " + login;
						context.startActivity(new Intent(context, MainActivity.class));
						((MainActivity)context).finish();
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
				message = "Connect: Erreur de connexion. Réessayez !";
			}
		} else {
			message = "Login JSON null";
		}

		MainUtils.log(message);
		return false;
	}

	public boolean parseJsonTransactions(Context context) {
		String[] params = new String[5];
		Session session = new Session(context);

		String cle_secrete = MainUtils.getSecretKey(context);
		String cle_bin = MainUtils.pack(cle_secrete);
		String algo = "HmacSHA512";
		String content = "DATE=" + getCurrentUTC();
		content += "&PHONECODE=" + session.getIndicatif();
		content += "&SIM=" + session.getPhone();

		String signature = hmacSha(cle_bin, content, algo);

		params[0] = session.getIndicatif();
		params[1] = session.getPhone();
		params[2] = getCurrentUTC();
		params[3] = signature;
		params[4] = session.getToken();

		/*MainUtils.log("phonecode: " + params[0]);
		MainUtils.log("sim: " + params[1] + "\n");
		MainUtils.log("date: " + params[2] + "\n");
		MainUtils.log("signature: " + params[3] + "\n");
		MainUtils.log("token: " + params[4] + "\n");*/

		String jsonStr = serviceHandler.makeServiceTransactionsCall(URL_TRANSACTIONS, params);
		//MainUtils.log("trx json = " + jsonStr);
		if (jsonStr != null) {
			try {
				JSONObject jsonObj = new JSONObject(jsonStr);
				Boolean result = jsonObj.getInt(TAG_CODE) == 200 ? true : false;
				if (result) {
					message = "Parsing data";
					if(jsonObj.has(TAG_DATA)){
						JSONArray transactionsArray = jsonObj.getJSONArray(TAG_DATA);
						for (int i = 0; i < transactionsArray.length(); i++) {
							JSONObject transactionObj = transactionsArray.getJSONObject(i);

							int transactionId = transactionObj.getInt(TAG_ID);
							String token = jsonObj.getString(TAG_TOKEN);
							//MainUtils.log("trx Token " + token);

							Transactions transaction = new Transactions();
							transaction.setUniqueId(transactionId);
							transaction.setReference(transactionObj.getString(TAG_REFERENCE));
							transaction.setAmount(transactionObj.getInt(TAG_BENEFICIAIRE_AMOUNT));
							transaction.setDestinataire(transactionObj.getInt(TAG_BENEFICIAIRE_TEL));
							transaction.setCodeUssd(MainUtils.ussdDecode(context, transactionObj.getString(TAG_CODE_USSD), transactionObj.getInt(TAG_BENEFICIAIRE_AMOUNT), transactionObj.getInt(TAG_BENEFICIAIRE_TEL), MainUtils.decodeString(transactionObj.getString(TAG_SIM_PASS))));
							transaction.setSimNumber(session.getPhone());
							transaction.setSimPass(MainUtils.decodeString(transactionObj.getString(TAG_SIM_PASS)));
							transaction.setToken(token);
							transaction.setEtat(0);
							transaction.setDate(transactionObj.getInt(TAG_DATE));

							if (!SoftgalApplication.getDAO().exists(transactionId,
									MySQLiteHelper.TABLE_TRANSACTIONS)) {
								SoftgalApplication.getDAO().insert(transaction);
							}else{
								//SoftgalApplication.getDAO().update(transaction);
							}
						}
					}
					return true;
				} else {
					if (jsonObj.has(TAG_MESSAGE)) {
						message = jsonObj.getString(TAG_MESSAGE);
					}else if (jsonObj.has(TAG_CODE)) {
						message = "Erreur: " + jsonObj.getString(TAG_CODE) + " -> " + jsonObj.getString("0");
					}else{
						message = "Error Parsing data";
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
				message = "Transactions: Erreur de connexion. Réessayez !";
			}
		} else {
			message = "Transaction JSON null";
		}
		//MainActivity.resume.setText(message);
		//MainUtils.log(message);
		return false;
	}

	public boolean parseJsonStatus(Context context, String reference, String status, String ussdMessage) {
		MainUtils.log("parseJsonStatus");
		String[] params = new String[9];
		Session session = new Session(context);
		String cle_secrete = MainUtils.getSecretKey(context);
		String cle_bin = MainUtils.pack(cle_secrete);
		String algo = "HmacSHA512";
		String content = "DATE=" + getCurrentUTC();
		content += "&PHONECODE=" + session.getIndicatif();
		content += "&SIM=" + session.getPhone();

		Cursor trx = SoftgalApplication.getDAO().getData(reference);
		if(trx.getCount() > 0 ){
			if (trx.moveToFirst()) {
				content += "&TOKEN=" + trx.getString(trx.getColumnIndex(MySQLiteHelper.COLUMN_TOKEN));
				String signature = hmacSha(cle_bin, content, algo);
				params[0] = session.getIndicatif();
				params[1] = session.getPhone();
				params[2] = getCurrentUTC();
				params[3] = signature;
				params[4] = session.getToken();
				params[5] = reference;
				params[6] = status;
				params[7] = ussdMessage;
				params[8] = trx.getString(trx.getColumnIndex(MySQLiteHelper.COLUMN_TOKEN));
				/*
				MainUtils.log("status phonecode --> " + params[0]);
				MainUtils.log("status sim --> " + params[1]);
				MainUtils.log("status date --> " + params[2]);
				MainUtils.log("status signature --> " + params[3]);
				MainUtils.log("status Authorization --> " + params[4]);
				MainUtils.log("status reference --> " + params[5]);
				MainUtils.log("status status --> " + params[6]);
				MainUtils.log("status message --> " + params[7]);
				MainUtils.log("status token --> " + params[8]);*/

				String jsonStr = serviceHandler.makeServiceStatusCall(URL_TRANSACTIONS_STATUS, params);
				MainUtils.log("status json = " + jsonStr);
				if (jsonStr != null) {
					Transactions transaction = SoftgalApplication.getDAO().getTransactions(reference);
					try {
						JSONObject jsonObj = new JSONObject(jsonStr);
						Boolean result = jsonObj.getInt(TAG_CODE) == 200 ? true : false;
						if (result) {
							message = "Parsing data";
							SoftgalApplication.getDAO().deleteTransactions(transaction);
							return true;
						} else {
							SoftgalApplication.getDAO().deleteTransactions(transaction);
							if (jsonObj.has(TAG_MESSAGE)) {
								message = jsonObj.getString(TAG_MESSAGE);
							}else{
								message = "Error Parsing data";
							}
							return false;
						}
					} catch (JSONException e) {
						e.printStackTrace();
						SoftgalApplication.getDAO().deleteTransactions(transaction);
						message = "Status: Erreur de connexion. Réessayez !";
					}
				}
				return false;
			}
		}else{
			return false;
		}
		return false;

	}


	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

}

/*MainUtils.log("UniqueId => " + transactionObj.getString(TAG_ID) + "\n" +
			  "Reference => " + transactionObj.getString(TAG_REFERENCE) + "\n" +
			  "Amount => " + transactionObj.getInt(TAG_BENEFICIAIRE_AMOUNT) + "\n" +
			  "Destinataire => " + transactionObj.getInt(TAG_BENEFICIAIRE_TEL) + "\n" +
			  "SimNumber => " + session.getIndicatif() + session.getPhone() + "\n" +
			  "SimPass => " + MainUtils.decodeString(transactionObj.getString(TAG_SIM_PASS)) + "\n" +
			  "CodeUssd => " + MainUtils.ussdDecode(transactionObj.getString(TAG_CODE_USSD), transactionObj.getInt(TAG_BENEFICIAIRE_AMOUNT), transactionObj.getInt(TAG_BENEFICIAIRE_TEL), MainUtils.decodeString(transactionObj.getString(TAG_SIM_PASS))) + "\n" +
			  "Token => " + jsonObj.getString(TAG_TOKEN) + "\n" +
			  "Etat => " + "0" + "\n" +
			  "Date => " + transactionObj.getInt(TAG_DATE)
);*/

/*MainUtils.log("indicatif: " + params[0]);
		MainUtils.log("phone: " + params[1] + "\n");
		MainUtils.log("date: " + params[2] + "\n");
		MainUtils.log("signature: " + params[3] + "\n");
		MainUtils.log("token: " + params[4] + "\n");*/

/*

MainUtils.log("status phonecode --> " + params[0]);
		MainUtils.log("status sim --> " + params[1]);
		MainUtils.log("status date --> " + params[2]);
		MainUtils.log("status signature --> " + params[3]);
		MainUtils.log("status Authorization --> " + params[4]);
		MainUtils.log("status reference --> " + params[5]);
		MainUtils.log("status status --> " + params[6]);
		MainUtils.log("status message --> " + params[7]);
		MainUtils.log("status token --> " + params[8]);
*/