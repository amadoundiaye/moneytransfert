package com.mobile.softgal.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.mobile.softgal.BaseData.BaseData;
import com.mobile.softgal.BaseData.Transactions;
import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.service.Constant;
import com.mobile.softgal.utils.MainUtils;

import java.util.ArrayList;
import java.util.List;

public class DataSource {

	private static final String TAG_EQUAL = " = ";
	// Database fields
	public static SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private static final String[] paramsColumns = new String[] {
			MySQLiteHelper.COLUMN_ID,
			MySQLiteHelper.COLUMN_UNIQUE_ID,
			MySQLiteHelper.COLUMN_TIMER,
			MySQLiteHelper.COLUMN_VERSION
	};
	
	private static String[] transactionsColumns = new String[] {
			MySQLiteHelper.COLUMN_ID,
			MySQLiteHelper.COLUMN_UNIQUE_ID,
			MySQLiteHelper.COLUMN_REFERENCE,
			MySQLiteHelper.COLUMN_AMOUNT,
			MySQLiteHelper.COLUMN_DESTINATAIRE,
			MySQLiteHelper.COLUMN_CODE_USSD,
			MySQLiteHelper.COLUMN_SIM_NUMBER,
			MySQLiteHelper.COLUMN_SIM_PASS,
			MySQLiteHelper.COLUMN_TOKEN,
			MySQLiteHelper.COLUMN_ETAT,
			MySQLiteHelper.COLUMN_DATE
	};

	private ArrayList<Transactions> transactions;

	private int fetchStart = 0;
	private int fetchNumber = 1;

	public DataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
		open();
	}

	public void open() throws SQLException {
		try {
			if (database == null || !database.isOpen()) {
				database = dbHelper.getWritableDatabase();
			}
			MainUtils.log("ouverture bdd réussie");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MainUtils.log(Log.ERROR, "ouverture bdd échec");
		}
	}

	public void close() {
		try {
			dbHelper.close();
			database.close();
			MainUtils.log("CLOSE BDD : SUCCESS");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			MainUtils.log(Log.ERROR, "CLOSE BDD : FAIL");
		}
	}

	public void updateDataVersion() {
		try {
			database.execSQL("DELETE FROM " + MySQLiteHelper.TABLE_TRANSACTIONS
					+ " WHERE " + MySQLiteHelper.COLUMN_ETAT
					+ TAG_EQUAL + Constant.TRANSACTION_VALIDATED);
			MainUtils
					.log("DELETE Data: toutes les transactions supprimees, sauf celles non exécutées.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MainUtils.log(Log.ERROR,
					"Update Data: echec lors de la suppression.");
		}
	}

	public void updateTransaction(Transactions transactions, String etat) {
		try {
			database.execSQL("UPDATE FROM " + MySQLiteHelper.TABLE_TRANSACTIONS
							+ " SET " + MySQLiteHelper.COLUMN_ETAT + TAG_EQUAL + etat
							+ " WHERE " + MySQLiteHelper.COLUMN_ETAT + TAG_EQUAL + Constant.TRANSACTION_PENDING
							+ " AND " + MySQLiteHelper.COLUMN_UNIQUE_ID + TAG_EQUAL + transactions.getUniqueId()
							);
			MainUtils
					.log("Update Data: changement état transaction exécutée.");
		} catch (Exception e) {
			e.printStackTrace();
			MainUtils.log(Log.ERROR,
					"Update Data: echec lors du changement d'état.");
		}
	}

	public void deleteTransactionsById(Transactions transaction) {
		long id = transaction.getUniqueId();
		try {
			database.delete(MySQLiteHelper.TABLE_TRANSACTIONS,
					MySQLiteHelper.COLUMN_UNIQUE_ID + TAG_EQUAL + id, null);

			MainUtils.log("suppression transaction num: " + id + " reussie");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MainUtils.log(Log.ERROR, "suppression transaction num: " + id + " echec");
		}
	}

	public void deleteTransactions(Transactions transaction) {
		try {
			database.delete(MySQLiteHelper.TABLE_TRANSACTIONS,
					MySQLiteHelper.COLUMN_REFERENCE + TAG_EQUAL + transaction.getReference(), null);
			MainUtils.log("suppression transaction num: " + transaction.getReference() + " reussie");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MainUtils.log(Log.ERROR, "suppression transaction num: " + transaction.getReference() + " echec");
			deleteTransactionsById(transaction);
		}
	}

	public void executeQuery(BaseData data, boolean isInsert) {
		if (!exists(data) || !isInsert) {
			ContentValues cv = new ContentValues();
			cv.put(MySQLiteHelper.COLUMN_UNIQUE_ID, data.getUniqueId());
			String tableName = null;
			if (data instanceof Transactions) {
				Transactions transaction = (Transactions) data;
				cv.put(MySQLiteHelper.COLUMN_REFERENCE, transaction.getReference());
				cv.put(MySQLiteHelper.COLUMN_AMOUNT, transaction.getAmount());
				cv.put(MySQLiteHelper.COLUMN_DATE, transaction.getDate());
				cv.put(MySQLiteHelper.COLUMN_DESTINATAIRE, transaction.getDestinataire());
				cv.put(MySQLiteHelper.COLUMN_CODE_USSD, transaction.getCodeUssd());
				cv.put(MySQLiteHelper.COLUMN_SIM_NUMBER, transaction.getSimNumber());
				cv.put(MySQLiteHelper.COLUMN_TOKEN, transaction.getToken());
				cv.put(MySQLiteHelper.COLUMN_ETAT, transaction.getEtat());
				tableName = transaction.getTableName();
				MainUtils.log("transactions : " + transaction.getReference());
			}

			if (isInsert) {
				database.insert(tableName, null, cv);
			} /*else {
				database.update(
						tableName,
						cv,
						MySQLiteHelper.COLUMN_UNIQUE_ID + " = "
								+ data.getUniqueId(), null);
			}*/
		}
	}

	/*
     * exists
     */
	public boolean exists(BaseData data) {
		Cursor cursor = null;
		if (data instanceof Transactions) {
			cursor = getData(data.getUniqueId());
		}
		return isCursorEmpty(cursor);
	}

	public boolean exists(int uniqueId, String tableName) {
		Cursor cursor = getData(uniqueId, tableName);
		return isCursorEmpty(cursor);
	}

	public boolean isCursorEmpty(Cursor cursor) {
		boolean mReturn = false;
		if (cursor.getCount() > 0) {
			mReturn = true;
		}
		// cursor.close();
		return mReturn;
	}

	/*
	 * Get data
	 */

	public Cursor getData(int uniqueId) {
		return getData(uniqueId, MySQLiteHelper.TABLE_TRANSACTIONS);
	}

	public Cursor getData(String reference) {
		return getData(reference, MySQLiteHelper.TABLE_TRANSACTIONS);
	}

	public Cursor getData(int uniqueId, String tableName) {
		String[] selection = null;
		if (tableName.equals(MySQLiteHelper.TABLE_TRANSACTIONS)) {
			selection = transactionsColumns;
		}

		Cursor cursor = database.query(tableName, selection,
				MySQLiteHelper.COLUMN_UNIQUE_ID + TAG_EQUAL + uniqueId,
				null, null, null, null);
		cursor.moveToFirst();

		return cursor;
	}

	public Cursor getData(String reference, String tableName) {
		String[] selection = null;
		if (tableName.equals(MySQLiteHelper.TABLE_TRANSACTIONS)) {
			selection = transactionsColumns;
		}

		Cursor cursor = database.query(tableName, selection,
				MySQLiteHelper.COLUMN_REFERENCE + TAG_EQUAL + "'" + reference + "'",
				null, null, null, null);
		cursor.moveToFirst();

		return cursor;
	}

	/*
     * get Transaction
     */
	public Transactions getTransactions(int id) {
		return new Transactions(getData(id));
	}

	public Transactions getTransactions(String reference) {
		return new Transactions(getData(reference));
	}

	/*
     * Fetching Transactions
     */

	public Cursor getTransactions() {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT "
				+ MainUtils.arrayToSelection(transactionsColumns, "trx"));
		queryBuilder.append(" FROM " + MySQLiteHelper.TABLE_TRANSACTIONS
				+ " as trx");

		queryBuilder.append(" WHERE trx." + MySQLiteHelper.COLUMN_SIM_NUMBER
				+ " = '"
				+ SoftgalApplication.getSession().getPhone() + "'");

		queryBuilder.append(" AND trx." + MySQLiteHelper.COLUMN_ETAT
				+ " = 0");

		queryBuilder.append(" ORDER BY trx." + MySQLiteHelper.COLUMN_DATE
				+ " ASC");

        queryBuilder.append(" LIMIT " + fetchNumber);

		//MainUtils.log(queryBuilder.toString());

		/*Cursor record=database.rawQuery(queryBuilder.toString(), null);
		if(record.getCount()!=0){
			if(record.moveToFirst()){
				do{
					MainUtils.log(record.getString(record.getColumnIndex(MySQLiteHelper.COLUMN_CODE_USSD)));
				}while(record.moveToNext());
			}
			//record.close();
		}*/

		return database.rawQuery(queryBuilder.toString(), null);
	}

	public void PurgeTransactions() {
		try {
			database.execSQL("DELETE FROM " + MySQLiteHelper.TABLE_TRANSACTIONS
					+ " WHERE " + MySQLiteHelper.COLUMN_ETAT
					+ TAG_EQUAL + Constant.TRANSACTION_VALIDATED);
			MainUtils
					.log("Purge Data: toutes les transactions réussies supprimées, sauf celles échouées.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MainUtils.log(Log.ERROR,
					"Purge Data: echec lors de la suppression.");
		}
	}

	public List<Transactions> getListTransactions() {
		// TODO Auto-generated method stub
		if (transactions == null) {
			Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSACTIONS,
					transactionsColumns, null, null, null, null, null);
			transactions = new ArrayList<Transactions>();
			if (cursor.getCount() != 0) {
				for (int i = 0; i < cursor.getCount(); i++) {
					cursor.moveToPosition(i);
					transactions.add(new Transactions(cursor));
				}
			}
		}
		return transactions;
	}

	/*
     * Add data
	 */

	public void insert(BaseData data) {
		executeQuery(data, true);
	}

	public void update(BaseData data) {
		executeQuery(data, false);
	}

	/*
     * Is database empty
     */
	public boolean isDatabaseEmpty() {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSACTIONS,
				transactionsColumns, null, null, null, null, null);
		return (cursor.getCount() == 0);
	}

	public int getFetchStart() {
		return fetchStart;
	}

	public int getFetchNumber() {
		return fetchNumber;
	}

	public void setFetchStart(int fetchStart) {
		this.fetchStart = fetchStart;
	}




	/*
     * Params
     */

	/*public void clearParams() {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					database.delete(MySQLiteHelper.TABLE_PARAMS, null, null);
					MainUtils.log("CLEAR BDD : SUCCESS");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MainUtils.log("CLEAR BDD : FAIL");
				}
				return null;
			}
		};
		task.execute();
	}

	public void addParams(int unique_id, int timer, int version) {
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper.COLUMN_UNIQUE_ID, unique_id);
		cv.put(MySQLiteHelper.COLUMN_TIMER, timer);
		cv.put(MySQLiteHelper.COLUMN_VERSION, version);
		database.insert(MySQLiteHelper.TABLE_PARAMS, null, cv);
	}

	public void putParams(Params params) {
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper.COLUMN_UNIQUE_ID, params.getUniqueId());
		cv.put(MySQLiteHelper.COLUMN_TIMER, params.getTimer());
		cv.put(MySQLiteHelper.COLUMN_VERSION, params.getVersion());

		if (!exists(params.getUniqueId())) {
			database.insert(MySQLiteHelper.TABLE_PARAMS, null, cv);
		} else {
			database.update(MySQLiteHelper.TABLE_PARAMS, cv, null, null);
		}
	}




	private boolean exists(int uniqueId) {
		// TODO Auto-generated method stub
		Cursor cursor = database.query(MySQLiteHelper.TABLE_PARAMS, paramsColumns,
				MySQLiteHelper.COLUMN_UNIQUE_ID + "=" + uniqueId, null, null, null, null);
		return cursor.getCount() == 1;
	}

	public Cursor getParams() {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_PARAMS, paramsColumns,
				null, null, null, null, null);
		return cursor;
	}



	public void clearTransactions() {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					database.delete(MySQLiteHelper.TABLE_TRANSACTIONS, null, null);
					MainUtils.log("CLEAR BDD : SUCCESS");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MainUtils.log("CLEAR BDD : FAIL");
				}
				return null;
			}
		};
		task.execute();
	}

	public void addTransactions(int unique_id, int amount, int destinataire, String code_ussd, int sim_number, int etat) {
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper.COLUMN_UNIQUE_ID, unique_id);
		cv.put(MySQLiteHelper.COLUMN_AMOUNT, amount);
		cv.put(MySQLiteHelper.COLUMN_DESTINATAIRE, destinataire);
		cv.put(MySQLiteHelper.COLUMN_CODE_USSD, code_ussd);
		cv.put(MySQLiteHelper.COLUMN_SIM_NUMBER, sim_number);
		cv.put(MySQLiteHelper.COLUMN_ETAT, etat);
		database.insert(MySQLiteHelper.TABLE_TRANSACTIONS, null, cv);
	}

	public void putTransactions(Transactions transactions) {
		ContentValues cv = new ContentValues();
		cv.put(MySQLiteHelper.COLUMN_UNIQUE_ID, transactions.getUniqueId());
		cv.put(MySQLiteHelper.COLUMN_AMOUNT, transactions.getAmount());
		cv.put(MySQLiteHelper.COLUMN_DESTINATAIRE, transactions.getDestinataire());
		cv.put(MySQLiteHelper.COLUMN_CODE_USSD, transactions.getCodeUssd());
		cv.put(MySQLiteHelper.COLUMN_SIM_NUMBER, transactions.getSimNumber());
		cv.put(MySQLiteHelper.COLUMN_ETAT, transactions.getEtat());

		if (!exists(transactions.getUniqueId())) {
			database.insert(MySQLiteHelper.TABLE_TRANSACTIONS, null, cv);
		} else {
			database.update(MySQLiteHelper.TABLE_TRANSACTIONS, cv, null, null);
		}
	}

	public Cursor getTransactions() {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT "
				+ arrayToSelection(transactionsColumns, "transaction"));
		queryBuilder.append(" FROM " + MySQLiteHelper.TABLE_TRANSACTIONS
				+ " as transaction");
		queryBuilder.append(" ORDER BY transaction." + MySQLiteHelper.COLUMN_UNIQUE_ID
				+ " ASC");
		MainUtils.log("searched: "+queryBuilder.toString()+"\n");
		return database.rawQuery(queryBuilder.toString(), null);
	}

	public List<Transactions> getListTransactions() {
		// TODO Auto-generated method stub
		if (transactions == null) {
			Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSACTIONS,
					transactionsColumns, null, null, null, null, null);
			transactions = new ArrayList<Transactions>();
			if (cursor.getCount() != 0) {
				for (int i = 0; i < cursor.getCount(); i++) {
					cursor.moveToPosition(i);
					transactions.add(new Transactions(cursor));
				}
			}
		}
		return transactions;
	}


	public Cursor getTransactions(int uniqueId) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSACTIONS, transactionsColumns,
				MySQLiteHelper.COLUMN_UNIQUE_ID + TAG_EQUAL + uniqueId, null, null, null,
				null);
		return cursor;
	}

	public boolean TransactionExists(int uniqueId) {
		// TODO Auto-generated method stub
		Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSACTIONS, transactionsColumns,
				MySQLiteHelper.COLUMN_UNIQUE_ID + "=" + uniqueId, null, null, null, null);
		return cursor.getCount() == 1;
	}
	*/


}