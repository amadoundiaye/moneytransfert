package com.mobile.softgal.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mobile.softgal.R;
import com.mobile.softgal.datasource.Session;

/**
 * Created by mac on 25/07/2017.
 */

public class PhoneNumberActivity extends Activity {

    private EditText indicatif, phone;
    private Button submit;
    //private RadioGroup rGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        indicatif =  (EditText) findViewById(R.id.indicatif);
        phone = (EditText) findViewById(R.id.phone);
        submit = (Button) findViewById(R.id.submit);
        // This will get the radiogroup
        /*rGroup = (RadioGroup)findViewById(R.id.radioGroup);
        // This will get the radiobutton in the radiogroup that is checked
        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            public void onCheckedChanged(RadioGroup group, int checkedId){
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked){
                    Session session = new Session(getApplicationContext());
                    // Changes the textview's text to "Checked: example radiobutton text"
                    if(checkedRadioButton.getId() == R.id.dev){
                        session.setMode(Session.KEY_MODE_DEV);
                    }else {
                        session.setMode(Session.KEY_MODE_PROD);
                    }
                }
            }
        });*/

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (v.getId() == R.id.submit){
                    if (indicatif.getText().toString().equals("")){
                        indicatif.setError("Veuillez saisir l'indicatif!");
                    }else if(phone.getText().toString().equals("")){
                        phone.setError("Veuillez saisir le numéro de téléphone!");
                    }else{
                        Session session = new Session(PhoneNumberActivity.this);
                        session.setPhone(phone.getText().toString());
                        session.setIndicatif(indicatif.getText().toString());
                        if (!session.getPhone().equals("") || session.getPhone() != null){
                            startActivity(new Intent(PhoneNumberActivity.this, MainActivity.class));
                            finish();
                        }
                    }
                }
            }
        });


    }

}
