package com.mobile.softgal.task;

import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.mobile.softgal.BaseData.Transactions;
import com.mobile.softgal.activity.MainActivity;
import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.MySQLiteHelper;
import com.mobile.softgal.datasource.Session;
import com.mobile.softgal.utils.MainUtils;
import com.mobile.softgal.utils.UssdUtils;
import com.mobile.softgal.ws.JSONParser;

import java.util.ArrayList;
import java.util.List;

public class TransactionsTask extends BaseAsyncTask {

	private JSONParser parser;
	private MainActivity mActivity;
	public static TextView mResume;
	Handler h = new Handler();
	int delay = 60 * 3 * 1000; //60*1 seconds
	Runnable runnable;

	public TransactionsTask(MainActivity activity, TextView resume) {
		// TODO Auto-generated constructor stub
		super(activity);
		this.mActivity = activity;
		this.mResume = resume;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		parser = new JSONParser();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		return parser.parseJsonTransactions(mActivity);
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		String message = "";

		if (result.booleanValue()) {
			Cursor transaction = SoftgalApplication.getDAO().getTransactions();
			UssdUtils ussdUtils = new UssdUtils(mActivity);
			if(transaction.getCount() <= 0 ){
				message = mResume.getText().toString() + "\n" + MainUtils.getCurrentUTC() + " ==> " + "Plus aucune transaction à traiter.";
				h.postDelayed(new Runnable() {
					public void run() {
						runnable = this;
						h.postDelayed(runnable, delay);
						mActivity.startActivity(new Intent(mActivity, MainActivity.class));
						mActivity.finish();
					}
				}, delay);

			} else if (transaction.moveToFirst()) {
				new Session(mActivity).setReference(transaction.getString(transaction.getColumnIndex(MySQLiteHelper.COLUMN_REFERENCE)));
				if (ussdUtils.hasSim()){
					Intent intent = ussdUtils.prepareUSSDQuery(transaction.getString(transaction.getColumnIndex(MySQLiteHelper.COLUMN_CODE_USSD)));
					mActivity.startActivity(intent);
					message = mResume.getText().toString() + "\n" + MainUtils.getCurrentUTC() + " ==> OK for " + transaction.getString(transaction.getColumnIndex(MySQLiteHelper.COLUMN_REFERENCE));
				}
			}else {
				message = mResume.getText().toString() + "\n" + MainUtils.getCurrentUTC() + " ==> " + parser.getMessage();
				h.postDelayed(new Runnable() {
					public void run() {
						runnable = this;
						h.postDelayed(runnable, delay);
						mActivity.startActivity(new Intent(mActivity, MainActivity.class));
						mActivity.finish();
					}
				}, delay);
			}
		} else {
			message = mResume.getText().toString() + "\n" + MainUtils.getCurrentUTC() + " ==> " + parser.getMessage();
			h.postDelayed(new Runnable() {
				public void run() {
					runnable = this;
					h.postDelayed(runnable, delay);
					mActivity.startActivity(new Intent(mActivity, MainActivity.class));
					mActivity.finish();
				}
			}, delay);
		}
		mResume.setText(message);
	}

}
