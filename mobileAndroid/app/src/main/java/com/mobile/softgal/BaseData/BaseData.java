package com.mobile.softgal.BaseData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.database.Cursor;

import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.DataSource;


public class BaseData  {

	public interface TableNameListner {
		public String getTableName();
	}

	protected String destinataire;
	protected int uniqueId;
	protected Cursor cursor;

	// protected String TABLE_NAME;

	public BaseData() {
		// TODO Auto-generated constructor stub

	}

	public BaseData(int uniqueId, String destinataire) {
		// TODO Auto-generated constructor stub
		this.uniqueId = uniqueId;
	}

	public BaseData(Cursor cursor) {
		// TODO Auto-generated constructor stub
		this.cursor = cursor;
	}

	public int getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}

	public Cursor getCursor() {
		return cursor;
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}

	public String getDate(long timestamp, String format) {
		SimpleDateFormat formater = new SimpleDateFormat(format,
				Locale.FRANCE);
		formater.setLenient(false);
		Date date = new Date(timestamp * 1000);
		return formater.format(date);
	}
	
	public String getDate(long timestamp) {
		return getDate(timestamp, "dd MMMM yyyy");
	}
	
	public static long dateTimestamp(String inputDate, String inputFormat) {
		SimpleDateFormat formater = new SimpleDateFormat(inputFormat,Locale.getDefault());
		Date date;
		try {
			date = formater.parse(inputDate);
			return date.getTime()/1000;
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public static long getCurrentDateTimeStamp(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.getDefault());
        long currentTimeStamp = dateTimestamp(dateFormat.format(new Date()),"yyyy-MM-dd HH:mm:ss"); // Find todays date
        return currentTimeStamp;	   
}
	
	protected static DataSource getDAO() {
		// TODO Auto-generated method stub
		return SoftgalApplication.getDAO();
	}
}
