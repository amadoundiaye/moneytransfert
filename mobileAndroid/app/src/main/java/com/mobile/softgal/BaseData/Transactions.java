package com.mobile.softgal.BaseData;

import android.database.Cursor;

import com.mobile.softgal.datasource.MySQLiteHelper;

import static com.mobile.softgal.utils.MainUtils.decodeString;

public class Transactions extends BaseData implements BaseData.TableNameListner {
	private int uniqueId;
	private int amount;
	private int destinataire;
	private String codeUssd;
	private String simNumber;
	private String simPass;
	private String reference;
	private String token;
	private int etat;
	private int date;

	public Transactions() {
		// TODO Auto-generated constructor stub
	}

	public Transactions(Cursor cursor) {
		// TODO Auto-generated constructor stub
		if (cursor.getPosition() < cursor.getCount()) {
			int columnUniqueId = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_UNIQUE_ID);
			int columnReference = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_REFERENCE);
			int columnAmount = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_AMOUNT);
			int columnDestinataire = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_DESTINATAIRE);
			int columnCodeUssd= cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_CODE_USSD);
			int columnSimNumber= cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_SIM_NUMBER);
			int columnSimPass= cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_SIM_PASS);
			int columnToken= cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_TOKEN);
			int columnEtat= cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_ETAT);
			int columnDate = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_DATE);

			this.uniqueId = cursor.getInt(columnUniqueId);
			this.reference = cursor.getString(columnReference);
			this.amount = cursor.getInt(columnAmount);
			this.destinataire = cursor.getInt(columnDestinataire);
			this.codeUssd = cursor.getString(columnCodeUssd);
			this.simNumber = cursor.getString(columnSimNumber);
			this.simPass = cursor.getString(columnSimPass);
			this.etat = cursor.getInt(columnEtat);
			this.date = cursor.getInt(columnDate);
		}
	}

	public int getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getDestinataire() {
		return destinataire;
	}

	public void setDestinataire(int destinataire) {
		this.destinataire = destinataire;
	}

	public String getCodeUssd() {
		return codeUssd;
	}

	public void setCodeUssd(String codeUssd) {
		this.codeUssd = codeUssd;
	}

	public String getSimNumber() {
		return simNumber;
	}

	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}

	public String getSimPass() {
		return simPass;
	}

	public void setSimPass(String simPass) {
		this.simPass = simPass;
	}

	public int getRealSimPass() {
		return Integer.parseInt(decodeString(simPass));
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	@Override
	public String getTableName() {
		return MySQLiteHelper.TABLE_TRANSACTIONS;
	}
}
