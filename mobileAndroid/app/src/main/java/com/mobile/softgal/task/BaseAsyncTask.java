package com.mobile.softgal.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;

import com.mobile.softgal.ws.JSONParser;


public class BaseAsyncTask extends AsyncTask<String, Void, Boolean> implements OnCancelListener {
	
	protected Context context;
	protected ProgressDialog progressDialog;
	protected JSONParser parser;

	public BaseAsyncTask(Activity activity) {
		// TODO Auto-generated constructor stub
		this.context = activity;
		parser = new JSONParser();
	}

	public BaseAsyncTask(Context context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		parser = new JSONParser();
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Patientez ...");
        progressDialog.setCancelable(true);
//        progressDialog.show();
        progressDialog.setOnCancelListener(this);
	}
	
	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (progressDialog.isShowing()) {
			try {
				progressDialog.dismiss();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		// TODO Auto-generated method stub
		this.cancel(true);
		progressDialog.dismiss();
	}
}
