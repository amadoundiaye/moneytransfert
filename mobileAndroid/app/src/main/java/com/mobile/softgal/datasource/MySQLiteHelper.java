package com.mobile.softgal.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mobile.softgal.utils.MainUtils;


public class MySQLiteHelper extends SQLiteOpenHelper {

	// noms des colonnes
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_UNIQUE_ID = "unique_id";
	public static final String COLUMN_REFERENCE = "reference";
	public static final String COLUMN_DESTINATAIRE = "destinataire";
	public static final String COLUMN_AMOUNT = "amount";
	public static final String COLUMN_CODE_USSD = "code_ussd";
	public static final String COLUMN_TOKEN = "token";
	public static final String COLUMN_ETAT = "etat";
	public static final String COLUMN_SIM_NUMBER = "sim_number";
	public static final String COLUMN_SIM_PASS = "sim_pass";
	public static final String COLUMN_TIMER = "timer";
	public static final String COLUMN_VERSION = "version";
	public static final String COLUMN_DATE = "date";

	// Noms des tables
	public static final String TABLE_PARAMS = "params";
	public static final String TABLE_TRANSACTIONS = "transactions";

	// DB infos
	private static final String DATABASE_NAME = "softgal_db";
	private static final int DATABASE_VERSION = 3;

	private Context context;

	// Database creation sql statement
	private static final String CREATE_TABLE_PARAMS = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_PARAMS + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
			                       + COLUMN_UNIQUE_ID + " INTEGER , "
			                       + COLUMN_TIMER + " INTEGER, "
			                       + COLUMN_VERSION + " INTEGER, "
			                       + COLUMN_DATE + " INTEGER "+ ")";


	private static final String CREATE_TABLE_TRANSACTIONS = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_TRANSACTIONS + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
										 + COLUMN_UNIQUE_ID + " INTEGER , "
										 + COLUMN_REFERENCE + " TEXT , "
										 + COLUMN_AMOUNT + " INTEGER, "
			                             + COLUMN_DESTINATAIRE + " TEXT, "
			                             + COLUMN_CODE_USSD + " TEXT,"
										 + COLUMN_SIM_NUMBER + " TEXT, "
			                             + COLUMN_SIM_PASS + " TEXT, "
										 + COLUMN_TOKEN + " TEXT, "
										 + COLUMN_ETAT + " INTEGER, "
										 + COLUMN_DATE + " INTEGER " + ")";
	/*
	MainUtils.log("UniqueId => " + transactionObj.getString(TAG_ID) + "\n" +
										  "Reference => " + transactionObj.getString(TAG_REFERENCE) + "\n" +
									      "Amount => " + transactionObj.getInt(TAG_BENEFICIAIRE_AMOUNT) + "\n" +
									      "Destinataire => " + transactionObj.getInt(TAG_BENEFICIAIRE_TEL) + "\n" +
									      "SimNumber => " + session.getIndicatif() + session.getPhone() + "\n" +
									      "SimPass => " + MainUtils.decodeString(transactionObj.getString(TAG_SIM_PASS)) + "\n" +
										  "CodeUssd => " + MainUtils.ussdDecode(transactionObj.getString(TAG_CODE_USSD), transactionObj.getInt(TAG_BENEFICIAIRE_AMOUNT), transactionObj.getInt(TAG_BENEFICIAIRE_TEL), MainUtils.decodeString(transactionObj.getString(TAG_SIM_PASS))) + "\n" +
									      "Etat => " + "0" + "\n" +
									      "Date => " + transactionObj.getInt(TAG_DATE)
	 */

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {

		try {
			database.execSQL(CREATE_TABLE_PARAMS);
			database.execSQL(CREATE_TABLE_TRANSACTIONS);
			MainUtils.log("Création bdd reussie ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			MainUtils.log(Log.ERROR, "Creation bdd echoué");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/*MainUtils.log("mise a jour bdd De la version " + oldVersion
				+ " à la version " + newVersion
				+ ", vous perdrez toutes les données" + existsColumnInTable(db,TABLE_MARCHAND,COLUMN_IS_SELECTED));
		if (oldVersion < 5) {
			if(existsColumnInTable(db,TABLE_MARCHAND,COLUMN_IS_SELECTED)){
				db.execSQL(DATABASE_ALTER_TEAM_1);
			}
		}*/

		try {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARAMS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTIONS);

			context.deleteDatabase("om_db");
			onCreate(db);
			MainUtils.log("Suppression bdd reussie");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MainUtils.log(Log.ERROR, "Suppression bdd echec");
		}
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		onUpgrade(db, oldVersion, newVersion);
	}

	private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
		Cursor mCursor = null;
		try {
			// Query 1 row
			mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

			// getColumnIndex() gives us the index (0 to ...) of the column - otherwise we get a -1
			if (mCursor.getColumnIndex(columnToCheck) != -1)
				return true;
			else
				return false;

		} catch (Exception Exp) {
			// Something went wrong. Missing the database? The table?
			MainUtils.log("... - existsColumnInTable When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
			return false;
		} finally {
			if (mCursor != null) mCursor.close();
		}
	}
}