package com.mobile.softgal.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.mobile.softgal.R;
import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.Session;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class MainUtils {
    public static String SOFTGAL = "SOFTGAL";
    public static boolean DEBUG = true; // set to true just when debugging
    public static boolean DEV = false;

    public static boolean isConnected(Activity activity) {
        return isConnected(activity, false);
    }

    public static boolean isConnected(final Activity activity, boolean showToast) {
        NetworkInfo ni = ((ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (ni == null) {
            if (showToast) {
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        ToastManager.makeText(activity,
                                "Veuillez vérifier votre connexion internet est activée.").show();
                    }
                });
            }
            return false;
        }

        if(ni.isConnected() && isOnline()){
            return true;
        }else {
            if (showToast) {
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        ToastManager.makeText(activity,
                                "Veuillez vérifier que vous avez accès à internet.").show();
                    }
                });
            }
            return false;
        }
        //return ni.isConnected();


    }

    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
        return false;
    }

    public static void log(int priority, String message) {
        if (DEBUG) {
            Log.println(priority, SOFTGAL, message);
        }
    }

    public static void log(String message) {
        MainUtils.log(Log.INFO, message);
    }

    public static int booleanToInteger(boolean bool) {
        return bool ? 1 : 0;
    }

    public static boolean integerToBoolean(int integer) {
        return integer == 1;
    }

    public static <T> void executeAsyncTask(AsyncTask<T, ?, ?> asyncTask) {
        executeAsyncTask(asyncTask, (T[]) null);
    }

    @TargetApi(11)
    public static <T> void executeAsyncTask(AsyncTask<T, ?, ?> asyncTask,
                                            T... params) {
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
          //  asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
        //else
            asyncTask.execute(params);
    }

    public static String arrayToSelection(String[] columns, String tableAlias) {
        return tableAlias.concat(".")
                + TextUtils.join(", ".concat(tableAlias).concat("."), columns);
    }

    // Function to convert and string to binary
    public static String pack(String hex) {
        String input = hex.length() % 2 == 0 ? hex : hex  + "0";
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < input.length(); i+=2) {
            String str = input.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    // Function to crypt and message
    public static String hmacSha(String KEY, String VALUE, String SHA_TYPE) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(KEY.getBytes("ISO-8859-1"), SHA_TYPE);
            Mac mac = Mac.getInstance(SHA_TYPE);
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(VALUE.getBytes("UTF-8"));

            byte[] hexArray = {
                    (byte)'0', (byte)'1', (byte)'2', (byte)'3',
                    (byte)'4', (byte)'5', (byte)'6', (byte)'7',
                    (byte)'8', (byte)'9', (byte)'a', (byte)'b',
                    (byte)'c', (byte)'d', (byte)'e', (byte)'f'
            };
            byte[] hexChars = new byte[rawHmac.length * 2];
            for ( int j = 0; j < rawHmac.length; j++ ) {
                int v = rawHmac[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return (new String(hexChars)).toUpperCase();
        }
        catch (Exception ex) {
            log(Log.ERROR, "\n exception during signature generation"+ ex.getMessage()+"\n");
            throw new RuntimeException(ex);
        }
    }

    public static boolean isThereChar(String[] chaArray, String response){
        boolean bool = false;
        for(int i=0; i < chaArray.length; i++)
        {
            //MainUtils.log("isThereChar -->  "+ chaArray[i] + " == " + response);
            if(response.contains(chaArray[i])){
                bool = true;
            }
        }
        return bool;
    }



    public static String getCurrentUTC(){
        Date time = Calendar.getInstance().getTime();
        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyyMMddHHmmss");
        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        return outputFmt.format(time);
    }

    public static String getSecretKey(Context context){
        return context.getResources().getString(R.string.secret_key);
    }

    public static String encodeString(String s) {
        byte[] data = new byte[0];

        try {
            data = s.getBytes("UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            String base64Encoded = Base64.encodeToString(data, Base64.DEFAULT);

            return base64Encoded;

        }
    }

    public static String decodeString(String encoded) {
        byte[] dataDec = Base64.decode(encoded, Base64.DEFAULT);
        String decodedString = "";
        try {

            decodedString = new String(dataDec, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } finally {

            return decodedString;
        }
    }

    public static String ussdDecode(Context context, String ussdCode, int valideMontant, int valideNumero, String valideCode) {
        int montant = 0;
        String valideUssd = ussdCode.replace("montant", String.valueOf(valideMontant));
        /*DEV = SoftgalApplication.getSession().getMode().equals(Session.KEY_MODE_DEV) ? true : false;*/

        if(DEV){
            int min = 0;
            int max = 100;

            Random r = new Random();
            montant = r.nextInt(max - min + 1) + min;
            valideUssd = ussdCode.replace("montant", String.valueOf(montant));
        }

        valideUssd = valideUssd.replace("numero", String.valueOf(valideNumero));
        valideUssd = valideUssd.replace("code", valideCode);
        return valideUssd;
    }


}
