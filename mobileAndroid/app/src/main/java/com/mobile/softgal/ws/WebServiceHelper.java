package com.mobile.softgal.ws;

import android.util.Base64;

import com.mobile.softgal.utils.MainUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WebServiceHelper {

	static String response = null;
	public final static int GET = 1;
	public final static int POST = 2;

	public WebServiceHelper() {

	}

	/*
	 * Making service call
	 * @url - url to make request
	 * @method - http request method
	 */
	public String makeServiceCall(int method, String url) {
		return this.makeServiceCall(method, url, null);
	}

	/*
	 * Making service call
	 * @url - url to make HTTP GET request
	 */
	public String makeServiceCall(String url) {
		return this.makeServiceCall(WebServiceHelper.GET, url, null);
	}

	/*
	 * Making service call
	 * @url - url to make HTTP GET request
	 * @params - http request params
	 */
	public String makeServiceCall(String url, HashMap<String, String> params) {
		return this.makeServiceCall(WebServiceHelper.GET, url, params);
	}

	/*
	 * Making service call
	 * @url - url to make request
	 * @method - http request method
	 * @params - http request params
	 */
	public String makeServiceCall(int method, String requestURL,
			HashMap<String, String> params) {

		URL url;
		String response = "";
		try {
			if (method == GET && params != null) {
				requestURL = requestURL + "?" + getQueryString(params);
			}
			MainUtils.log(requestURL);
			url = new URL(requestURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			if (method == POST) {
				conn.setReadTimeout(30000);
				conn.setConnectTimeout(30000);
				if (method == POST) {
					conn.setRequestMethod("POST");
				}
				conn.setDoInput(true);
				conn.setDoOutput(true);

				OutputStream os = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os, "UTF-8"));
				if (params != null) {
					writer.write(getQueryString(params));
				}
				writer.flush();
				writer.close();
				os.close();
			}

			int responseCode = conn.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				response = readStream(conn.getInputStream());
			} else {
				//
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}


	public String makeServiceConnectCall(String requestURL, String... params) {
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
		HttpConnectionParams.setSoTimeout(httpParameters, 30000);

		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpPost httpPost = new HttpPost(requestURL);

		String jsonResult = "";
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("_username", params[0]));
			nameValuePairs.add(new BasicNameValuePair("_password", params[1]));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = httpClient.execute(httpPost);
			jsonResult = inputStreamToString(response.getEntity().getContent()).toString();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonResult;
	}

	public String makeServiceTransactionsCall(String requestURL, String... params) {
		MainUtils.log(requestURL);
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
		HttpConnectionParams.setSoTimeout(httpParameters, 30000);

		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpPost httpPost = new HttpPost(requestURL);

		String jsonResult = "";
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
			nameValuePairs.add(new BasicNameValuePair("phonecode", params[0]));
			nameValuePairs.add(new BasicNameValuePair("sim", params[1]));
			nameValuePairs.add(new BasicNameValuePair("date", params[2]));
			nameValuePairs.add(new BasicNameValuePair("signature", params[3]));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Authorization", params[4]);

			HttpResponse response = httpClient.execute(httpPost);
			jsonResult = inputStreamToString(response.getEntity().getContent()).toString();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonResult;
	}

	public String makeServiceStatusCall(String requestURL, String... params) {
		MainUtils.log(requestURL);
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
		HttpConnectionParams.setSoTimeout(httpParameters, 30000);

		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpPost httpPost = new HttpPost(requestURL);

		String jsonResult = "";
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(9);
			nameValuePairs.add(new BasicNameValuePair("phonecode", params[0]));
			nameValuePairs.add(new BasicNameValuePair("sim", params[1]));
			nameValuePairs.add(new BasicNameValuePair("date", params[2]));
			nameValuePairs.add(new BasicNameValuePair("signature", params[3]));
			nameValuePairs.add(new BasicNameValuePair("reference", params[5]));
			nameValuePairs.add(new BasicNameValuePair("status", params[6]));
			nameValuePairs.add(new BasicNameValuePair("message", params[7]));
			nameValuePairs.add(new BasicNameValuePair("token", params[8]));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Authorization", params[4]);

			for (NameValuePair pair : nameValuePairs) {
				MainUtils.log(pair.getName() + ": " + pair.getValue());
			}
			MainUtils.log("Authorization" + ": " + params[4]);

			HttpResponse response = httpClient.execute(httpPost);
			jsonResult = inputStreamToString(response.getEntity().getContent()).toString();

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonResult;
	}

	private StringBuilder inputStreamToString(InputStream is) {
		String rLine = "";
		StringBuilder answer = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		try {
			while ((rLine = br.readLine()) != null) {
				answer.append(rLine);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return answer;
	}


	private String readStream(InputStream in) {
		BufferedReader reader = null;
		StringBuffer response = new StringBuffer();
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while ((line = reader.readLine()) != null) {
				response.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return response.toString();
	}

	private String getQueryString(HashMap<String, String> params)
			throws UnsupportedEncodingException {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (first)
				first = false;
			else
				result.append("&");

			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
		}
		return result.toString();
	}
}
