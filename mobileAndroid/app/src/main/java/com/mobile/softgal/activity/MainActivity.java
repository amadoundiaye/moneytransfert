package com.mobile.softgal.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.mobile.softgal.BaseData.Transactions;
import com.mobile.softgal.R;
import com.mobile.softgal.application.SoftgalApplication;
import com.mobile.softgal.datasource.DataSource;
import com.mobile.softgal.datasource.Session;
import com.mobile.softgal.service.MyBroadcastReceiver;
import com.mobile.softgal.task.LoginTask;
import com.mobile.softgal.task.StatusTask;
import com.mobile.softgal.utils.MainUtils;
import com.mobile.softgal.utils.ToastManager;

import static com.mobile.softgal.service.Constant.TRANSACTION_NO_CREDIT;
import static com.mobile.softgal.service.Constant.TRANSACTION_REJECTED;
import static com.mobile.softgal.service.Constant.TRANSACTION_USSD_ALERTE_FRAUDE;
import static com.mobile.softgal.service.Constant.TRANSACTION_USSD_ERROR;
import static com.mobile.softgal.service.Constant.TRANSACTION_USSD_SOLDE;
import static com.mobile.softgal.service.Constant.TRANSACTION_USSD_SUCCESS;
import static com.mobile.softgal.service.Constant.TRANSACTION_VALIDATED;
import static com.mobile.softgal.utils.MainUtils.isThereChar;


public class MainActivity extends BaseActivity {

    Handler h = new Handler();
    int delay = 60 * 2 * 1000; //60*1 seconds
    Runnable runnable;
    public static TextView resume;
    protected PowerManager.WakeLock mWakeLock;
    public static MainActivity activity;
    public String reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        this.mWakeLock.acquire();

        Session session = new Session(this);
        if (session.getPhone().equals("") || session.getPhone() == null){
            startActivity(new Intent(this, PhoneNumberActivity.class));
        }else{
            //MainUtils.log("MODE DEV:" + String.valueOf(SoftgalApplication.getSession().getMode().equals(Session.KEY_MODE_DEV)));

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            if (!SoftgalApplication.getSession().getFirstStart().equals("1")) {
                SoftgalApplication.getSession().setFirstStart("1");
                ToastManager.makeText(this, "Veuillez activer l'accessiblité de l'application.").show();
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                finish();
            }

            resume = (TextView)findViewById(R.id.resume);
            registerReceiver(broadcastReceiver, new IntentFilter("ussdResult"));
        }


    }

    // Add this inside your class
    BroadcastReceiver broadcastReceiver =  new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle b = intent.getExtras();
            String message = b.getString("message");
            reference = b.getString("reference");
            resume.setText(resume.getText().toString() + "  ==> " + message);
            //MainUtils.log(reference+ " -- " + message);

            if(isThereChar(TRANSACTION_USSD_SOLDE, message)){
                MainUtils.log("solde nok --> "+ message);
                MainUtils.executeAsyncTask(new StatusTask(MainActivity.this, reference, TRANSACTION_NO_CREDIT, message));
            }else if (isThereChar(TRANSACTION_USSD_ERROR, message)){
                MainUtils.log("trx nok --> "+ message);
                MainUtils.executeAsyncTask(new StatusTask(MainActivity.this, reference, TRANSACTION_REJECTED, message));
            }else if (isThereChar(TRANSACTION_USSD_SUCCESS, message)){
                MainUtils.log("trx ok --> "+ message);
                //Changement du message de successfull par "La transaction est complete"
                String custom_message = "La transaction est complete";
                MainUtils.executeAsyncTask(new StatusTask(MainActivity.this, reference, TRANSACTION_VALIDATED, custom_message));
            }else if (isThereChar(TRANSACTION_USSD_ALERTE_FRAUDE, message)){
                MainUtils.log("solde nok --> "+ message);
                Transactions transaction = SoftgalApplication.getDAO().getTransactions(reference);
                SoftgalApplication.getDAO().deleteTransactions(transaction);
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                finish();
                /*h.postDelayed(new Runnable() {
                    public void run() {
                        runnable = this;
                        h.postDelayed(runnable, delay);
                        Transactions transaction = SoftgalApplication.getDAO().getTransactions(reference);
                        SoftgalApplication.getDAO().deleteTransactions(transaction);
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        finish();
                    }
                }, delay);*/
            }else {
                h.postDelayed(new Runnable() {
                    public void run() {
                        runnable = this;
                        h.postDelayed(runnable, delay);
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        finish();
                    }
                }, delay);
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, PhoneNumberActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        //start handler as activity become visible
        /*delay = getSession().getRepeatTime() * 1000;
        h.postDelayed(new Runnable() {
            public void run() {
                //do something
                runnable = this;
                h.postDelayed(runnable, delay);
                if(MainUtils.isConnected(MainActivity.this)){
                    MainUtils.executeAsyncTask(new GetURLTask(MainActivity.this, resume));
                }
            }
        }, delay);*/

        //if (isAccessibilitySettingsOn(getApplicationContext())) {

        //}
        if (!isAccessibilitySettingsOn(getApplicationContext())) {
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            finish();
        }else{
            if(MainUtils.isConnected(MainActivity.this, true)){
                MainUtils.executeAsyncTask(new LoginTask(MainActivity.this, resume, getResources().getString(R.string.account), getResources().getString(R.string.entered)));
            }else{
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                finish();
            }
        }


        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.mWakeLock.acquire();
    }

    @Override
    public void onPause() {
        super.onPause();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.mWakeLock.acquire();
    }

    @Override
    public void finish() {
        System.out.println("finish activity");
        System.runFinalizersOnExit(true) ;
        super.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    protected void onDestroy() {
        this.mWakeLock.release();
        super.onDestroy();
    }

    // To check if service is enabled
    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = getPackageName()+ "/" + MyBroadcastReceiver.class.getCanonicalName();
        MainUtils.log(service);
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            MainUtils.log("accessibilityEnabled = " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            MainUtils.log("Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            MainUtils.log("***ACCESSIBILITY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    MainUtils.log("-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        MainUtils.log("We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            MainUtils.log("***ACCESSIBILITY IS DISABLED***");
            return false;
        }
        return true;

    }

}
