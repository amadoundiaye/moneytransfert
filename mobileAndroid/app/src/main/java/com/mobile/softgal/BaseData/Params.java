package com.mobile.softgal.BaseData;

import android.database.Cursor;

import com.mobile.softgal.datasource.MySQLiteHelper;

public class Params {
	private int uniqueId;
	private int timer;
	private int version;
	private int date;

	public Params(Cursor cursor) {
		// TODO Auto-generated constructor stub
		if (cursor.getPosition() < cursor.getCount()) {
			int columnUniqueId = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_UNIQUE_ID);

			int columnTimer = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_TIMER);
			int columnVersion= cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_VERSION);
			int columnDate = cursor
					.getColumnIndexOrThrow(MySQLiteHelper.COLUMN_DATE);

			this.uniqueId = cursor.getInt(columnUniqueId);
			this.timer = cursor.getInt(columnTimer);
			this.version = cursor.getInt(columnVersion);
			this.date = cursor.getInt(columnDate);
		}
	}

	public int getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		this.timer = timer;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

}
