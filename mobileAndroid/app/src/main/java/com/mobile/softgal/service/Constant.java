package com.mobile.softgal.service;

/**
 * Created by mac on 19/07/2017.
 */

public class Constant {
    // base urls
    public static final String SOFTGAL_WEBSITE_BASE_URL = "http://vps423165.ovh.net/backend/web/api/";
    public static final String TRANSACTION_TYPE_TRX = "0";
    public static final String TRANSACTION_TYPE_STS = "1";

    public static final String TRANSACTION_PENDING = "1";
    public static final String TRANSACTION_CONFIRMED = "2";
    public static final String TRANSACTION_VALIDATED = "3";
    public static final String TRANSACTION_CANCELLED = "4";
    public static final String TRANSACTION_REJECTED = "5";
    public static final String TRANSACTION_NO_CREDIT = "6";

    public static final String[] TRANSACTION_USSD_SUCCESS = {"La transaction est en cours. Merci de patienter.", "confirmation", "Transaction effectué.", "confirmation par SMS."};

    public static final String[] TRANSACTION_USSD_ERROR = {"erreur", "MMI", "Montant incorrect! Veuillez tapez un montant entre 0 et 1500001 FCFA", "Impossible de traiter la demande.", "initiateur", "Le destinataire n'est pas un client Orange Money", "Problème de connexion ou code IHM non valide.", "Le solde de votre compte ne vous permet pas", "incorrect", "9 chiffres", "Echec de la transaction", "le montant saisi est superieur au montant maximum autorise pour ce service", "superieur au montant maximum", "montant maximum"};

    public static final String[] TRANSACTION_USSD_SOLDE = {"Le solde de votre compte ne vous permet pas"};

    public static final String[] TRANSACTION_USSD_ALERTE_FRAUDE = {"Vous venez d'effectuer une transaction identique vers le meme numéro pour le meme montant", "Vous venez d'effectuer une transaction identique vers le meme numero pour le meme montant", "transaction identique"};

}