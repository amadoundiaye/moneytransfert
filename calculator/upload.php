<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
/*

if ( !empty( $_FILES ) ) {

    $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
    $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];

    move_uploaded_file( $tempPath, $uploadPath );

    $answer = array( 'answer' => 'File transfer completed' );
    $json = json_encode( $answer );

    echo $json;

} else {

    echo 'No files';

}*/

?>

<?php

function isDigit($var) {
    return ((string)$var == (float)$var);
}
//if form is submitted
if( !empty( $_FILES ) ) {
    $validator = array('success' => false, 'messages' => array());
    $filename = $_FILES['file']['tmp_name'];
    $mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
    if(in_array($_FILES['file']['type'],$mimes)){
        // do something
    } else {
        echo json_encode(array('success' => false, 'message' => "Invalid File:Please Upload CSV File!"));
        exit;
    }

    if($_FILES["file"]["size"] > 0){
        $file = fopen($filename, "r");
        require_once 'db_connect.php';
        $sql = "TRUNCATE TABLE frais";
        $query = $connect->query($sql);
        $emapData = fgetcsv($file, 10000, ";");
        while (($emapData = fgetcsv($file, 10000, ";")) !== FALSE){
            for($i=0; $i<count($emapData); $i++){
                if(!isDigit($emapData[$i])){  // return **TRUE** if it is numeric
                    echo json_encode(array('success' => false, 'message' => "Le fichier contient des données erronées!"));
                    exit;
                }
            }
            $sql = "INSERT into frais (id, montant_min, montant_max, tarif) values (NULL, $emapData[0], $emapData[1], $emapData[2])";
            $query = $connect->query($sql);
        }

        if($query === TRUE) {
            echo json_encode(array('success' => true, 'message' => "Terminé avec succès!"));
            exit;
        } else {
            echo json_encode(array('success' => false, 'message' => "Invalid File:Please Upload CSV File!"));
            exit;
        }
        fclose($file);
    }
    // close the database connection
    $connect->close();
}else{
    die("Missing parameters...");
}
?>
