<?php 

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

//if form is submitted
if(isset($_GET['montant']) && $_GET['montant'] > 0) {	

	error_reporting ( 1 );

	define(DEBUG, isset ($_GET ['debug']) ? $_GET ['debug'] : false);

	include 'db_connect.php';

	$output = array (
			'success' => false,
	);

	$sum = $_GET ['montant'];

	if (! isset ( $sum )) {
		$output['messages'] = "Sum value omitted.";
		exit ( json_encode ( $output ) );
	}

	if (trim ( $sum ) == "" || !is_numeric($sum)) {
		$output['messages'] = "Wrong value given.";
		exit ( json_encode ( $output ) );
	}

	//$query = "SELECT s.libelle AS service, d.tarif AS tarif, d.tarif_promo AS tarifPromo, UNIX_TIMESTAMP( p.debut ) AS debut, UNIX_TIMESTAMP( p.fin ) AS fin";
	$query = "SELECT * ";
	$query .= " FROM frais ";
	//$query .= " JOIN grille g ON ( d.grille_id = g.id )";
	//$query .= " JOIN promo p ON ( g.promo_id = p.id )";
	//$query .= " JOIN service_has_grille sg ON sg.grille_id = g.id";
	//$query .= " JOIN service s ON s.id = sg.service_id";
	$query .= " WHERE montant_min <= $sum AND montant_max >= $sum";
	$query .= " ORDER BY id ASC";

	$result = mysqli_query($connect, $query);

	if(!$result) {
		$output['messages'] = "Query error occured " . (DEBUG ?  mysqli_error($mysqli) : "");
		exit ( json_encode ( $output ) );
	}

	$output ['success'] = true;
	$output ['messages'] = array ();

	while ( $data = $result->fetch_object() ) {
		$feeRow = array();
		if($data->tarif >= 1){
			$feeRow["fees"] = $data->tarif;
		}else{
			$feeRow["fees"] = $data->tarif * $sum;
		}
		//$feeRow["value"] = (time() > $data->debut && time() < $data->fin) ? $data->tarifPromo : $data->tarif;
		
		if(!in_array($feeRow, $output['messages'])) {
			$output ['messages'] = $feeRow;
		}
	}

	/* free result set */
	$result->close();

	if(DEBUG) var_dump($output);

	else print (json_encode ( $output )) ;

	//mysqli_close ();

}
