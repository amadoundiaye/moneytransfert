<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

function exec_curl($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Softgal');
        $resultat = curl_exec ($ch);
        curl_close($ch);
        return $resultat;
        }

	/*$CADtoXOFURL = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D%22http%3A%2F%2Ffinance.yahoo.com%2Fd%2Fquotes.csv%3Fe%3D.csv%26f%3Dnl1d1t1%26s%3Dcadxof%3DX%22%3B&format=json&diagnostics=true&callback=';

	$XOFtoCADURL = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D%22http%3A%2F%2Ffinance.yahoo.com%2Fd%2Fquotes.csv%3Fe%3D.csv%26f%3Dnl1d1t1%26s%3Dxofcad%3DX%22%3B&format=json&diagnostics=true&callback=';


	$XOFtoCAD = exec_curl($XOFtoCADURL);
	$CADtoXOF = exec_curl($CADtoXOFURL);

	$jsonXOFtoCAD = json_decode($XOFtoCAD);
	$jsonCADtoXOF = json_decode($CADtoXOF);	
	

	$output ['success'] = true;
	$output ['XOFtoCAD'] = array ();
	$output ['CADtoXOF'] = array ();
	$output ['XOFtoCAD'] = $jsonXOFtoCAD->query->results->row->col1;
	$output ['CADtoXOF'] = $jsonCADtoXOF->query->results->row->col1;

	echo (json_encode ( $output )) ;*/

$key ="d68a395bd6ad6929d03894ff";
$CADtoXOFURL = "https://v3.exchangerate-api.com/bulk/".$key."/CAD";
$XOFtoCADURL = "https://v3.exchangerate-api.com/bulk/".$key."/XOF";
$XOFtoCAD = exec_curl($XOFtoCADURL);
$CADtoXOF = exec_curl($CADtoXOFURL);
$jsonXOFtoCAD = json_decode($XOFtoCAD);
$jsonCADtoXOF = json_decode($CADtoXOF);
$output ['success'] = true;
$output ['XOFtoCAD'] = array ();
$output ['CADtoXOF'] = array ();
$output ['XOFtoCAD'] = $jsonXOFtoCAD->rates->CAD;
$output ['CADtoXOF'] = $jsonCADtoXOF->rates->XOF;

echo (json_encode ( $output )) ;

?>
