<?php 

require_once 'db_connect.php';

$output = array('data' => array());

$sql = "SELECT d.id as id, d.montant_min as montant_min, d.montant_max as montant_max, d.tarif as tarif, d.tarif_promo as tarif_promo, d.grille_id as grille_id, g.libelle as libelle FROM details_grille d INNER JOIN grille g ON d.grille_id = g.id";
$query = $connect->query($sql);

$x = 1;
while ($row = $query->fetch_assoc()) {
	$grille = '';
	switch ($row['grille_id']) {
		case 1:
			$grille = '<label class="label label-success">'.$row['libelle'].'</label>';
			break;
		case 2:
			$grille = '<label class="label label-danger">'.$row['libelle'].'</label>';
			break;
		case 3:
			$grille = '<label class="label label-info">'.$row['libelle'].'</label>';
			break;
		case 4:
			$grille = '<label class="label label-warning">'.$row['libelle'].'</label>';
			break;
		default:
			# code...
			break;
	}

	$actionButton = '
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" data-target="#editMemberModal" onclick="editMember('.$row['id'].')"> <span class="glyphicon glyphicon-edit"></span> Edit</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeMemberModal" onclick="removeMember('.$row['id'].')"> <span class="glyphicon glyphicon-trash"></span> Remove</a></li>	    
	  </ul>
	</div>
		';

	$output['data'][] = array(
		$x,
		$row['montant_min'],
		$row['montant_max'],
		$row['tarif'],
		$row['tarif_promo'],
		$grille,
		$actionButton
	);

	$x++;
}

// database connection close
$connect->close();

echo json_encode($output);