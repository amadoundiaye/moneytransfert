<?php 

header('Access-Control-Allow-Origin: http://vps423165.ovh.net');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

//if form is submitted

	error_reporting ( 1 );

	define(DEBUG, isset ($_GET ['debug']) ? $_GET ['debug'] : false);

	include 'db_connect.php';

	$output = array (
			'success' => false,
	);

	$query = "SELECT * ";
	$query .= " FROM frais ";
	$query .= " ORDER BY id ASC";

	$result = mysqli_query($connect, $query);

	if(!$result) {
		$output['messages'] = "Query error occured " . (DEBUG ?  mysqli_error($mysqli) : "");
		exit ( json_encode ( $output ) );
	}

	$output ['success'] = true;
	$output ['messages'] = array ();

	while ( $data = $result->fetch_object() ) {
		$feeRow = array();
		$feeRow["montant_min"] = $data->montant_min;
		$feeRow["montant_max"] = $data->montant_max;
        $feeRow["tarif"] = $data->tarif;
		
		if(!in_array($feeRow, $output['messages'])) {
			$output ['messages'][] = $feeRow;
		}
	}

	/* free result set */
	$result->close();

	if(DEBUG) var_dump($output);

	else print (json_encode ( $output )) ;

	//mysqli_close ();
