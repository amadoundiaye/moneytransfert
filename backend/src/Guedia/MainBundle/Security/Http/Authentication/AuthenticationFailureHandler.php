<?php
namespace Guedia\MainBundle\Security\Http\Authentication;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler as FailureHandler;

/**
 * AuthenticationFailureHandler
 *
 * @author Dev Lexik <dev@lexik.fr>
 */
class AuthenticationFailureHandler extends FailureHandler
{

    /**
     * {@inheritDoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
	        	'success'  => false, 'isConnected' => false, 'locked' => false,
	            'message' => "Login ou mot de passe incorrect.",
	        );

        $response = new JsonResponse($data);
        $event = new AuthenticationFailureEvent($request, $exception, $response);

        $this->dispatcher->dispatch(Events::AUTHENTICATION_FAILURE, $event);

        return $event->getResponse();
    }
}
