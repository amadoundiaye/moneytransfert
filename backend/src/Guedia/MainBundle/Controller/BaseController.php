<?php
namespace Guedia\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Translation\Translator;

class BaseController extends Controller {
	
	protected function createCreateForm($entity, $formName, $options = array()) {
		$type = '\Guedia\MainBundle\Form\\'.$formName.'Type';
		$form = $this->createForm(new $type() , $entity, $options);
		return $form;
	}
	
	/**
	 * @param \Symfony\Component\Form\Form $form
	 * @param array $fields
	 */
	protected function useFormFields($form, $fields = array()) {
		foreach($form->all() as $name => $widget) {
			if(in_array($name, $fields)==false) {
				$form->remove($name);
			}
		}
	}
	
	/**
	 * @param \Symfony\Component\Form\Form $form
	 * @param array $fields
	 */
	protected function removeFormFields($form, $fields = array()) {
		foreach($form->all() as $name => $widget) {
			if(in_array($name, $fields)==true) {
				$form->remove($name);
			}
		}
	}
	
	/**
	 * @param \Symfony\Component\Form\Form $form
	 * @param array $fields
	 * @param string $translateDomain
	 */
	protected function getErrorsForm($form, $fields, $translationDomain = null) {
		$translator = new Translator('en');
		$errors = array();
		foreach($fields as $field) {
			foreach($form->get($field)->getErrors() as $error) {
				$errors[$field] = $translationDomain ? $translator->trans($error->getMessage(), array(), $translationDomain) : $error->getMessage();
			}
		}
		return $errors;
	}
	
	protected function paginate($request, QueryBuilder $queryBuilder, $rendererMethod = 'addRowInTable', $rootColumnName = 'id') {
		$query = $queryBuilder->getQuery();
		$paginator  = $this->get('knp_paginator');
		$numberPage = ((int)$request->query->get('start')/(int)$request->query->get('length'))+1;
		$this->setFilter($queryBuilder, array(), $request);
		$this->setOrder($queryBuilder, array(), $request);
		$query = $this->customResultsQuery($queryBuilder);
		$this->get('session')->set('query_client', $query->getDQL());
		$query->setHint('knp_paginator.count', $this->getLengthResults($queryBuilder, $rootColumnName));
		$pagination = $paginator->paginate($query, $numberPage, $request->query->get('length'), array('distinct' => false));
		$params = $pagination->getParams();
		// parameters to template
		$aaData = array();
		foreach ($pagination->getItems() as $entity) {
			$aaData[] = $this->{$rendererMethod}($entity);
		}
		$output = array(
				"draw" => $params['draw'],
				"recordsTotal" => $pagination->getTotalItemCount(),
				"recordsFiltered" => $pagination->getTotalItemCount(),
				"data" => $aaData
			);
		$response = new JsonResponse($output);
		return $response;
	}
	
	/**
	 * @param Mixed $entity
	 * @param string $column
	 * @return string
	 */
	public function showEntityStatus($entity, $column) {
		$reflect = new \ReflectionClass($entity);
		$template = $this->get('twig')->loadTemplate($this->getMyParameter('template_status'));
		return $template->renderBlock('status_'.strtolower($reflect->getShortName()).'_'.$column, array(
				'entity' => $entity, 'ids' => $this->getMyParameter('ids'), 'states' => $this->getMyParameter('states')
		));
	}
	
	
	/**
	 * @todo fait un tri sur le résultat
	 * @param sfWebRequest $request
	 */
	protected function setOrder(QueryBuilder $queryBuilder, $aColumns, Request $request) {
		$orders = $request->query->get('order');
		$columns = $request->query->get('columns');
		if($orders && is_array($orders)) {
			foreach($orders as $order) {
				if(isset($aColumns[$columns[$order['column']]['data']])) {
					$queryBuilder->orderBy($aColumns[$columns[$order['column']]['data']], $order['dir']);
				}
			}
		}
	}
	
	/**
	 * @todo ajoute un filtre
	 * @param sfWebRequest $request
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		$queryString = null;
		$search = $request->query->get('search');
		if(empty($search['value'])==false) {
			for($i=0 ;$i<count($aColumns);$i++) {
				$queryString .= ($queryString ? ' OR ' : null)."(".$aColumns[$i]." LIKE '%".str_replace("'", "", $search['value'])."%') ";
			}
			$queryBuilder->andWhere($queryString);
		}
		
	}
	
	/**
	 * @todo customize le résultat de la requête
	 * @param QueryBuilder $queryBuilder
	 */
	protected function customResultsQuery(QueryBuilder $queryBuilder) {
		return $queryBuilder->getQuery();
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param QueryBuilder $queryBuilder
	 * @param string $rootColumnName
	 * @return integer
	 */
	protected function getLengthResults(QueryBuilder $queryBuilder, $rootColumnName) {
		$data = $queryBuilder->select(sprintf('COUNT(%s.%s) as number', $queryBuilder->getRootAlias(), $rootColumnName))->getQuery()->execute();
		return $data[0]['number'];
	}
	
	protected function getMyParameter($name, $path = array()) {
		$data = $this->container->getParameter($name);
		foreach($path as $key) {
			$data = $data[$key];
		}
		return $data;
	}
	
	/**
	 * @param Request $request
	 * @param array $data
	 * @param Form $form
	 */
	public function modifyRequestForForm($request, $data, $form) {
		$arrData = $request->request->all();
		
		foreach($data as $key => $value) {
			$arrData[$form->getName()][$key] = $value;
		}
		
		$request->request->replace($arrData);
		$request->setMethod('POST');
		$form->handleRequest($request);
	}
}
