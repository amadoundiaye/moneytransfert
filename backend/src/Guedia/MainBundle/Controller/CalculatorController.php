<?php
namespace Guedia\MainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\DetailsGrille;

class CalculatorController extends Controller
{
	/**
	 * @Route ("/calculator/{amount}/withdrawal_fee", name="calculator_withdrawal_fee")
	 */
	public function withdrawalFeeAction(Request $request, $amount)
    {
    	$em = $this->getDoctrine()->getManager();
    	if(empty($amount) || !is_numeric($amount)) {
    		return new JsonResponse(array('success' => false, 'messages' => 'Wrong value given.'));
    	}
    	//s.libelle AS service, d.tarif AS tarif, d.tarif_promo AS tarifPromo, UNIX_TIMESTAMP( p.debut ) AS debut, UNIX_TIMESTAMP( p.fin ) AS fin
    	$result = $em->getRepository('GuediaMainBundle:DetailsGrille')->createQueryBuilder('dg')
    		->select('s.libelle, dg.tarif, dg.tarifPromo, p.debut, p.fin')
	    	->leftJoin('dg.grille', 'g')
	    	->leftJoin('g.promo', 'p')
	    	->leftJoin('g.service', 's')
	    	->where('dg.montantMin <= :sum AND dg.montantMax >= :sum')
	    	->orderBy('s.ordre', 'ASC')
	    	->setParameter('sum', $amount)
	    	->getQuery()
	    	->getResult();
    	$data = array ();
    	foreach($result as $value) {
    		$data[] = array(
    				'title' => $value['libelle'],
    				'value' => !$value['debut'] || !$value['fin'] || (time() > $value['debut']->getTimestamp() && time() < $value['fin']->getTimestamp()) ? $value['tarifPromo'] : $value['tarif']
    			);
    	}
    	return new JsonResponse(array('success' => true, 'messages' => $data));
	}
	
	/**
	 * @Route ("/calculator/change", name="calculator_change")
	 */
	public function changeAction(Request $request)
	{
		$change = $this->get('guedia_change');
		return new JsonResponse($change->toArray());
	}
	
	/**
	 * @param string $url
	 * @return string
	 */
	private function exec_curl($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Softgal');
		$resultat = curl_exec ($ch);
		curl_close($ch);
		return $resultat;
	}
}
