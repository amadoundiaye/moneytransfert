<?php
namespace Guedia\MainBundle\Controller;

use Guedia\MainBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Guedia\MainBundle\Annotation\QMLogger;
use Guedia\MainBundle\Entity\Utilisateur;

class UserController extends BaseController {
	
	/**
	 * @QMLogger(message="Liste des utilisateurs")
	 * @Route ("/user/list", name="user_list")
	 */
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$queryBuilder = $em->getRepository('GuediaMainBundle:Utilisateur')->listAllQueryBuilder(array());
		return $this->paginate($request, $queryBuilder);
	}
	
	/**
	 * @QMLogger(message="Visualisation du profil d'un utilisateur")
	 * @Route ("/show_user/{token}", name="show_user")
	 */
	public function showAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Utilisateur')->findByToken($token);
		$arrPuce = array();
		foreach($entity->getPuce() as $puce) {
			$arrPuce[] = $puce->getId();
		}
		if($entity) {
			return new JsonResponse(array('success' => true, 'data' => array(
					'firstname' => $entity->getFirstname(), 'lastname' => $entity->getLastname(), 'email' => $entity->getEmail(), 'role' => $entity->getUserRole(),
					'address' => $entity->getAddress(), 'phone' => $entity->getPhone(), 'username' => $entity->getusername(), 'puce' => $arrPuce
				)));
		} else {
			return new JsonResponse(array('success' => false, 'message' => "Erreur!!! Impossible de retrouver l'utilisateur recherché"));
		}
	}
	
	/**
	 * @QMLogger(message="Mise à jour d'un utilisateur")
	 * @Route ("/update_user/{token}", name="update_user")
	 * @Method("POST")
	 */
	public function updateAction(Request $request, $token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Utilisateur')->findByToken($token);
		if(!$entity) {
			return new JsonResponse(array('success' => false, 'message' => "Erreur!!! Impossible de retrouver l'utilisateur recherché"));
		}
		$form = $this->createForm(new \Guedia\MainBundle\Form\UserType(), $entity);
		$form->handleRequest($request);
		if($form->isValid()) {
			$em->persist($entity);
			$em->flush();
			return new JsonResponse(array('success' => true, 'message' => "La mise à jour de l'utilisateur s'est effectuée avec succés"));
		}
		$errors = $this->getErrorsForm($form, array('firstname', 'lastname', 'username', 'email', 'address', 'phone', 'puce'), 'FOSUserBundle');
		return new JsonResponse(array('success' => false, 'message' => "Erreur!!! Impossible d'effectuer la mise à jour de l'utilisateur", 'errors' => count($errors) ? $errors : null));
	}
	
	/**
	 * @QMLogger(message="Mise à jour du profil d'un utilisateur")
	 * @Route ("/profile/update", name="update_profile")
	 */
	public function updateProfileAction(Request $request) {
		$user = $this->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			throw new AccessDeniedException('This user does not have access to this section.');
		}
		/** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
		$formFactory = $this->get('fos_user.profile.form.factory');
		$form = $formFactory->createForm();
		$form->setData($user);
		$form->handleRequest($request);
		if ($form->isValid()) {
			/** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
			$userManager = $this->get('fos_user.user_manager');
			$userManager->updateUser($user);
			return new JsonResponse(array(
					'success' => true, 'message' => 'La modification a réussi.', 'isConnected' => true, 'userId' => $user->getId(), 'username' => $user->getUsername(), 
					'email' => $user->getEmail(), 'profil' => $user->getProfil(), 'role' => $user->getUserRole(), 'address' => $user->getAddress(), 
					'firstname' => $user->getFirstname(), 'lastname' => $user->getLastname(), 'locked' => false
				));
		}
		$errors = $this->getErrorsForm($form, array('firstname', 'lastname', 'username', 'email', 'address', 'phone', 'puce'), 'FOSUserBundle');
		return new JsonResponse(array('success' => false, 'message' => 'La modification a échoué.', 'errors' => count($errors) ? $errors : null));
	}
	
	/**
	 * @QMLogger(message="Mise à jour du mot de passe d'un utilisateur")
	 * @Route ("/change-password", name="change_password")
	 */
	public function changePasswordAction(Request $request)
	{
		$user = $this->getUser();
		if (!is_object($user) || !$user instanceof UserInterface) {
			throw new AccessDeniedException('This user does not have access to this section.');
		}
		/** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
		$formFactory = $this->get('fos_user.change_password.form.factory');
		$form = $formFactory->createForm();
		$form->setData($user);
		$form->handleRequest($request);
		if ($form->isValid()) {
			/** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
			$userManager = $this->get('fos_user.user_manager');
			$userManager->updateUser($user);
			return new JsonResponse(array('success' => true, 'message' => 'La Modification a réussie. Merci de vous reconnecter.', 'isConnected' => true, 'isConnected' => true));
		}
		return new JsonResponse(array('success' => false, 'message' => 'La modification a échoué. Veuillez réessayer.'));
	}
	
	/**
	 * @QMLogger(message="Réinitialisation du mot de passe d'un utilisateur")
	 * @Route("/forgot-password", name="forgot_password")
	 */
	public function forgotPasswordAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		/** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
		$formFactory = $this->get('fos_user.resetting.form.factory');
		/** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
		$userManager = $this->get('fos_user.user_manager');
		$user = $userManager->findUserByUsernameOrEmail($request->get('_email'));
		if (null === $user) {
			return new JsonResponse(array('success' => false, 'message' => "Nom d'utilisateur ou e-mail incorrect"));
		}
		$user->setPlainPassword($request->get('_password'));
		$this->get('guedia_mailer')->sendNewPassword($user);
		$userManager->updatePassword($user);
		$em->persist($user);
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "Your password has changed. A mail with the new password will send to you"));
	}
	
	/**
	 * @QMLogger(message="Contactez-nous")
	 * @Route("/contact_us", name="contact_us")
	 */
	public function contactUsAction(Request $request)
	{
		$data = $request->request->all();
		if(!isset($data['subject']) || empty($data['subject']) || !isset($data['message']) || empty($data['message'])) {
			return new JsonResponse(array('success' => false, 'message' => "Error!!! subject or message invalid"));
		}
		$admins = $this->getDoctrine()->getRepository('GuediaMainBundle:Utilisateur')->findAdmins();
		$this->get('guedia_mailer')->contactUs($admins, $data, $this->getUser());
		return new JsonResponse(array('success' => true, 'message' => "The mail has been sent to the administrator"));
	}
	
	/**
	 * @QMLogger(message="Envoyer une information de promotion")
	 * @Route("/send_promo", name="send_promo")
	 */
	public function sendPromoAction(Request $request)
	{
		$data = $request->request->all();
		if(!isset($data['subject']) || empty($data['subject']) || !isset($data['message']) || empty($data['message'])) {
			return new JsonResponse(array('success' => false, 'message' => "Error!!! subject or message invalid"));
		}
		$customers = $this->getDoctrine()->getRepository('GuediaMainBundle:Client')->findAll();
		$this->get('guedia_mailer')->sendPromo($customers, $data, $this->getUser());
		return new JsonResponse(array('success' => true, 'message' => "The mail has been sent to the administrator"));
	}
	
	/**
	 * @QMLogger(message="Enregistrement d'un nouvel utilisateur")
	 * @Route("/registration", name="registration")
	 */
	public function registrationAction(Request $request)
	{
		$formFactory = $this->get('fos_user.registration.form.factory');
		$userManager = $this->get('fos_user.user_manager');
		$user = $userManager->createUser();
		$user->setEnabled(true);
		$form = $formFactory->createForm();
		$form->setData($user);
		$form->handleRequest($request);
		$plainPassword = $user->getPlainPassword();
		if ($form->isValid()) {
			$userManager->updateUser($user);
			$user->setPlainPassword($plainPassword);
			$this->get('guedia_mailer')->sendNewAccount($user);
			return new JsonResponse(array('success' => true, 'message' => "The registration has done successfully. A mail will be send to the new user"));
		}
		$errors = $this->getErrorsForm($form, array('firstname', 'lastname', 'username', 'email', 'address', 'phone', 'puce'), 'FOSUserBundle');
		return new JsonResponse(array(
				'success' => false, 'message' => "The registraton has failed. Please try again or contact the administrator.", 'errors' => count($errors) ? $errors : null
			));
	}
	
	/**
	 * @QMLogger(message="Mise à jour du statut d'un utilisateur")
	 * @Route ("/change_status_user/{token}", name="change_status_user")
	 */
	public function changeStatusAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Utilisateur')->findByToken($token);
		if(!$entity) {
			return new JsonResponse(array('success' => false, 'message' => "Error!!! User not found"));
		}
		if($entity->isEnabled()) {
			$entity->setEnabled(false);
		} else {
			$entity->setEnabled(true);
		}
		$em->persist($entity);
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "The user has been updated successfully"));
	}
	
	/**
	 * @Route ("/token_check", name="token_check")
	 * @Method("POST")
	 */
	public function tokenCheckAction(Request $request) {
		return new JsonResponse();
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Guedia\MainBundle\Entity\Utilisateur $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		return array(
				'id'		=> $entity->getId(),
				'firstname'	=> $entity->getFirstName(),
				'lastname' 	=> $entity->getLastname(),
				'email' 	=> $entity->getEmail(),
				'username' 	=> $entity->getUsername(),
				'profil' 	=> $entity->getProfil(),
				'role' 		=> $entity->getUserRole(),
				'status' 	=> $this->showEntityStatus($entity, 'etat'),
				'action'	=> $this->get('guedia_action')->generateActionsForUser($entity)
			);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('q.firstname', 'q.lastname', 'q.email'), $request);
	}
}
