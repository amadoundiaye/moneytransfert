<?php
namespace Guedia\MainBundle\Controller;

use Guedia\MainBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\Country;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Guedia\MainBundle\Annotation\QMLogger;

class CountryController extends BaseController {
	
	/**
	 * @QMLogger(message="Liste des pays")
	 * @Route ("/country/list", name="country_list")
	 */
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$queryBuilder = $em->getRepository('GuediaMainBundle:Country')->listAllQueryBuilder(array());
		return $this->paginate($request, $queryBuilder);
	}
	
	/**
	 * @Route ("/country/list_select", name="country_select")
	 */
	public function listForSelectAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('GuediaMainBundle:Country')->createQueryBuilder('q')
			->getQuery()->getResult();
		$data = array();
		foreach($entities as $entity) {
			array_push($data, array('id' => $entity->getId(), 'libelle' => $entity->getName(), 'phonecode' => $entity->getPhonecode()));
		}
		return new JsonResponse($data);
	}
	
	/**
	 * @QMLogger(message="Ajout d'un pays")
	 * @Route("/country/create", name="create_country")
	 * @Method("POST")
	 */
	public function createAction(Request $request) {
		$entity = new Country();
		$form   = $this->createCreateForm($entity, 'Country');
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();
			return new JsonResponse(array('status' => 'success', 'text' => "The country has been created succesfully !!!"));
		}
	}
	
	/**
	 * @QMLogger(message="Visualisation d'un pays")
	 * @Route ("/show_country/{token}", name="show_country")
	 */
	public function showAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Country')->findByToken($token);
		return new JsonResponse(array('success' => true, 
				'data' => array(
					'id'			=> $entity->getId(),
					'iso'			=> $entity->getIso(),
					'iso3'			=> $entity->getIso3(),
					'name'			=> $entity->getName(),
					'classicalCode'	=> $entity->getClassicalCode(),
					'tacCode'		=> $entity->getTacCode()
				)
			));
	}
	
	/**
	 * @QMLogger(message="Mise à jour d'un pays")
	 * @Route ("/update_country/{token}", name="update_country")
	 * @Method("POST")
	 */
	public function updateAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Country')->findByToken($token);
		$form = $this->createCreateForm($entity, 'Country');
		$request = $this->get('request');
		$form->bind($request);
		if ($form->isValid()) {
			$em->persist($entity);
			$em->flush();
			return new JsonResponse(array('success' => true, 'message' => "The country has been updated succesfully !!!"));
		}
		return new JsonResponse(array('success' => false, 'message' => "The country has been updated succesfully !!!"));
	}
	
	/**
	 * @Route ("/reject_country/{token}", name="reject_country")
	 */
	public function rejectAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Country')->findByToken($token);
		if(!$entity) {
			return new JsonResponse(array('success' => false, 'message' => "The transaction is not found"));
		}
		$em->remove($entity);
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "The country has been cancelled succesfully !!!"));
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Orange\MainBundle\Entity\Country $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		return array(
				'id'		=> $entity->getId(),
				'tac'		=> $entity->getTacCode(),
				'classical' => $entity->getClassicalCode(),
				'name' 		=> $entity->getName(),
				'phonecode' => $entity->getPhonecode(),
				'status' 	=> $this->showEntityStatus($entity, 'etat'),
				'action'	=> $this->get('guedia_action')->generateActionsForCountry($entity)
			);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('bu.libelle'), $request);
	}
}
