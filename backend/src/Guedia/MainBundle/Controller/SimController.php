<?php
namespace Guedia\MainBundle\Controller;

use Guedia\MainBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\Country;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Guedia\MainBundle\Entity\Puce;
use Guedia\MainBundle\Annotation\QMLogger;

class SimController extends BaseController {
	
	/**
	 * @QMLogger(message="Liste des puces")
	 * @Route ("/sim/list", name="sim_list")
	 */
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$queryBuilder = $em->getRepository('GuediaMainBundle:Puce')->createQueryBuilder('q');
		return $this->paginate($request, $queryBuilder);
	}
	
	/**
	 * @Route ("/sim/list_select", name="sim_select")
	 */
	public function listForSelectAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('GuediaMainBundle:Puce')->createQueryBuilder('q')
		->getQuery()->getResult();
		$data = array();
		foreach($entities as $entity) {
			array_push($data, array('id' => $entity->getId(), 'number' => $entity->getTelephone(), 'phonecode' => $entity->getPays()->getPhonecode()));
		}
		return new JsonResponse($data);
	}
	
	/**
	 * @QMLogger(message="Ajout d'une puce")
	 * @Route("/sim/create", name="create_sim")
	 * @Method("POST")
	 */
	public function createAction(Request $request) {
		$entity = new Puce();
		$form   = $this->createCreateForm($entity, 'Sim');
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();
			return new JsonResponse(array('success' => true, 'message' => "The sim has been created succesfully !!!"));
		}
		$errors = $this->getErrorsForm($form, array('telephone', 'pays', 'motdepasse'));
		return new JsonResponse(array('success' => false, 'message' => "An error occured. Please try again or contact administrator !!!", 'errors' => count($errors) ? $errors : null));
	}
	
	/**
	 * @QMLogger(message="Visualisation d'une puce")
	 * @Route ("/show_sim/{token}", name="show_sim")
	 */
	public function showAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Puce')->findByToken($token);
		if($entity) {
			return new JsonResponse(array('success' => true, 'data' => array(
					'telephone' => $entity->getTelephone(), 'pays' => $entity->getPays()->getId(), 'motdepasse' => $entity->getMotdepasse()
				)));
		} else {
			return new JsonResponse(array('success' => false, 'message' => "Error!!! The sim is not found"));
		}
	}
	
	/**
	 * @QMLogger(message="Mise à jour d'une puce")
	 * @Route ("/update_sim/{token}", name="update_sim")
	 * @Method("POST")
	 */
	public function updateAction(Request $request, $token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Puce')->findByToken($token);
		if(!$entity) {
			return new JsonResponse(array('success' => false, 'message' => "Erreur!!! Impossible de retrouver l'utilisateur recherché"));
		}
		$form   = $this->createCreateForm($entity, 'Sim');
		$form->handleRequest($request);
		if($form->isValid()) {
			$em->persist($entity);
			$em->flush();
			return new JsonResponse(array('success' => true, 'message' => "The sim has been updated succesfully !!!"));
		} else {
			$errors = $this->getErrorsForm($form, array('telephone', 'pays'), 'FOSUserBundle');
			return new JsonResponse(array('success' => false, 'message' => "An error occured. Please try again or contact administrator !!!", 'errors' => count($errors) ? $errors : null));
		}
	}
	
	/**
	 * @Route ("/reject_sim/{token}", name="delete_sim")
	 */
	public function deleteAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Puce')->findByToken($token);
		if(!$entity) {
			return new JsonResponse(array('success' => false, 'message' => "The transaction is not found"));
		}
		$em->remove($entity);
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "The country has been cancelled succesfully !!!"));
	}
	
	/**
	 * @QMLogger(message="Mise à jour de statut d'une puce")
	 * @Route ("/change_status_sim/{token}", name="change_status_sim")
	 */
	public function changeStatusAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Puce')->findByToken($token);
		if(!$entity) {
			return new JsonResponse(array('success' => false, 'message' => "Error!!! SIM not found"));
		}
		if($entity->getEtat()) {
			$entity->setEtat(false);
		} else {
			$entity->setEtat(true);
		}
		$em->persist($entity);
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "The SIM has been updated successfully"));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('q.telephone'), $request);
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Guedia\MainBundle\Entity\Puce $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		return array(
				'id'			=> $entity->getId(),
				'phonenumber'	=> $entity->getTelephone(),
				'country' 		=> $entity->getPays()->getName(),
				'phonecode' 	=> $entity->getPays()->getPhonecode(),
				'status' 		=> $this->showEntityStatus($entity, 'etat'),
				'action'		=> $this->get('guedia_action')->generateActionsForSim($entity)
			);
	}
}
