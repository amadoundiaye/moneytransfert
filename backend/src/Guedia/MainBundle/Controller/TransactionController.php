<?php
namespace Guedia\MainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\Depot;
use Guedia\MainBundle\Entity\Puce;
use Guedia\MainBundle\Entity\Configuration;
use Guedia\MainBundle\Annotation\QMLogger;
use Guedia\MainBundle\Entity\Historique;
use Guedia\MainBundle\Entity\Client;
use Guedia\MainBundle\Entity\Trace;
use Guedia\MainBundle\Entity\Session;
use Doctrine\ORM\QueryBuilder;

class TransactionController extends BaseController {
	
	/**
	 * @Route ("/reset_filter_for_transaction", name="reset_filter_for_transaction")
	 */
	public function resetFilterAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$data = array('from' => strtotime('first day of this month'), 'end' => strtotime('now'));
		$em->merge(Session::create(Session::$depot, $this->getUser(), $data));
		$em->flush();
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @QMLogger(message="Filtre des transactions")
	 * @Route ("/filter_by_date_for_transaction", name="filter_by_date_for_transaction")
	 */
	public function filterBydateAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$criteria = $request->request->all();
		$data = array(
				'from'	=> \DateTime::createFromFormat('M d, Y', $criteria[0])->getTimestamp(),
				'end'	=> \DateTime::createFromFormat('M d, Y', $criteria[1])->getTimestamp()
		);
		$em->merge(Session::create(Session::$depot, $this->getUser(), $data));
		$em->flush();
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	/**
	 * @QMLogger(message="Liste des transactions")
	 * @Route("/transaction/list", name="transaction_list")
	 */
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$session = $em->getRepository('GuediaMainBundle:Session')->findOneBy(array('action' => Session::$depot, 'utilisateur' => $this->getUser()));
		$queryBuilder = $em->getRepository('GuediaMainBundle:Depot')->listQueryBuilder($session->getDataToArray());
		$qb = clone $queryBuilder;
		$qb->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', \Guedia\MainBundle\Entity\Etat::$ids['validate']);
		$this->get('session')->set('transaction_query', array('query' => $qb->getDql(), 'param' => $qb->getParameters()));
		return $this->paginate($request, $queryBuilder);
	}
	
	/**
	 * @QMLogger(message="Extraction des transactions")
	 * @Route("/transaction/export", name="export_transaction")
	 * @Template()
	 */
	public function exportAction() {
		$em = $this->getDoctrine()->getManager();
		$data = $this->get('session')->get('transaction_query', array());
		$query = $em->createQuery($data['query']);
		$query->setParameters($data['param']);
		$methodName = 'exportTransactionFor'.$this->getUser()->getProfil();
		$objWriter = $this->get('guedia_extraction')->{$methodName}($this->get('guedia_mapping')->listForExport($query->getResult()));
		$objWriter->save(WEB_DIR.'/download/transaction.xlsx');
		return new JsonResponse(array('success' => true, 'url' => $this->generateUrl('homepage')."../"));
	}
	
	/**
	 * @QMLogger(message="Ajout d'une transaction")
	 * @Route("/transaction/create", name="create_shop")
	 */
	public function createAction(Request $request) {
		$entity = new Depot();
		$form   = $this->createCreateForm($entity, 'Depot');
		$data = $request->request->all();
		$client = $this->getDoctrine()->getRepository('GuediaMainBundle:Client')->findOneBy(array(
				'identiteType' => $data['depot']['client']['identiteType'], 'identite' => $data['depot']['client']['identite']
			));
		$entity->setClient($client);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			
			$entity->setConfiguration($em->getRepository('GuediaMainBundle:Configuration')->findOneByTransfertType($entity->getTransfertType()));
			$em->persist($entity);
			$em->flush();
			$entity->setReference(sprintf('SG%.03d-%s-%s', $this->getUser()->getId(), $entity->getReference(), $entity->getId()));
			$entity->setEtat($em->getReference('GuediaMainBundle:Etat', $this->getMyParameter('ids', array('etat', 'pending'))));
			$entity->setPuce($em->getRepository('GuediaMainBundle:Puce')->findByCashierAndCountry($this->getUser(), $entity->getBeneficiairePays()));
			$entity->setVendeur($this->getUser());
			$em->persist($entity);
			$em->persist(Historique::createFromDepot($entity, $this->getUser()));
			$em->flush();
			return new JsonResponse(array('success' => true, 'text' => "The cashin has been traited succesfully !!!", 'data' => $entity->getData()));
		}
		$errors = $this->getErrorsForm($form, array(
				'clientPrenom', 'clientNom', 'clientAddress', 'clientIdentiteType', 'clientIdentite', 'clientTelephone', 
				'beneficiairePrenom', 'beneficiaireNom', 'beneficiairePays', 'beneficiaireTelephone'
			));
		return new JsonResponse(array('success' => false, 'message' => "An error occured. Please try again or contact administrator !!!", 'errors' => count($errors) ? $errors : null));
	}
	
	/**
	 * @QMLogger(message="Visualisation d'une transaction")
	 * @Route ("/show_transaction/{token}", name="show_transaction")
	 * @Template()
	 */
	public function showAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Depot')->findByToken($token);
		return new JsonResponse(array('success' => true, 'text' => "The cashin has been traited succesfully !!!", 'data' => $entity->getData()));
	}
	
	/**
	 * @QMLogger(message="Envoi par mail d'une transaction")
	 * @Route ("/send_transaction/{token}/{exchangerate}", name="send_transaction")
	 * @Template()
	 */
	public function sendAction($token, $exchangerate) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Depot')->findByToken($token);
		$this->get('guedia_mailer')->sendTransaction($entity, $exchangerate);
		return new JsonResponse(array('success' => true, 'text' => "The mail was send to the customer and the cashier succesfully !!!", 'data' => $entity->getData()));
	}
	
	/**
	 * @QMLogger(message="Confirmation d'une transaction")
	 * @Route ("/confirm_transaction/{token}/{exchangerate}", name="confirm_transaction")
	 * @Template()
	 */
	public function confirmAction($token, $exchangerate) {
		$ids = $this->getParameter('ids');
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Depot')->findByToken($token);
		$entity->setEtat($em->getRepository('GuediaMainBundle:Etat')->find($ids['etat']['confirmed']));
		$em->persist($entity);
		$em->persist(Historique::createFromDepot($entity, $this->getUser()));
		$this->get('guedia_mailer')->confirmTransaction($entity, $exchangerate);
		\Unirest::post("http://37.187.37.240/calculator/sms.php", array(), array(
				'to_number' => sprintf("%s%s", $entity->getBeneficiairePays()->getPhonecode(), $entity->getBeneficiaireTelephone()), 
				'content' => sprintf('Vous allez bientot recevoir un transfert Orange Money de %s FCFA de la part de %s', $entity->getBeneficiaireMontant(), $entity->getClient())
			));
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "The transaction has been confirmed succesfully !!!", 'data' => $entity->getData()));
	}
	
	/**
	 * @QMLogger(message="Rejet d'une transaction")
	 * @Route ("/reject_transaction/{token}", name="reject_transaction")
	 * @Template()
	 */
	public function rejectAction($token) {
		$ids = $this->getParameter('ids');
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Depot')->findByToken($token);
		$entity->setEtat($em->getRepository('GuediaMainBundle:Etat')->find($ids['etat']['cancelled']));
		$em->persist($entity);
		$em->persist(Historique::createFromDepot($entity, $this->getUser()));
		$em->flush();
		return new JsonResponse(array('success' => true, 'message' => "The transaction has been cancelled by the cashier !!!", 'data' => $entity->getData()));
	}
	
	/**
	 * @QMLogger(message="Execution d'une transaction")
	 * @Route ("/validate_transaction", name="validate_transaction")
	 * @Template()
	 */
	public function executeAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$phonecode = $request->request->get('phonecode');
		$token = $request->request->get('token');
		$puce = $request->request->get('sim');
		$date = $request->request->get('date');
		if(null == $country = $em->getRepository('GuediaMainBundle:Country')->findOneByPhonecode($phonecode)) {
			return new JsonResponse(array('code' => 101, 'The phonecode is undefined'));
		}
		if(null == $sim = $em->getRepository('GuediaMainBundle:Puce')->findOneByTelephone($request->request->get('sim'))) {
			return new JsonResponse(array('code' => 102, 'The sim is undefined'));
		}
		if(null == $em->getRepository('GuediaMainBundle:Puce')->findOneBy(array('telephone' => $request->request->get('sim'), 'pays' => $country->getId()))) {
			return new JsonResponse(array('code' => 103, 'The sim is undefined for this country'));
		}
		if(null == $depot = $em->getRepository('GuediaMainBundle:Depot')->findOneBy(array('token' => $token, 'reference' => $request->request->get('reference')))) {
			return new JsonResponse(array('code' => 103, 'The transaction is undefined'));
		}
		if(\DateTime::createFromFormat('YmdHis', $request->request->get('date')) == false) {
			return new JsonResponse(array('code' => 104, 'The date format is invalid'));
		}
		$interval = abs(strtotime($request->request->get('date')) - time());
		if($interval > 300) {
			return new JsonResponse(array('code' => 105, 'The request has expired'));
		}
		$message = null;
		$signature_expected = $this->get('guedia_signature')->generateToken($puce, $phonecode, $date, $token, $message);		
		$em->persist(Trace::logRequest($sim, $signature_expected, $request->request->get('signature'), $request->request->get('message')));
		if($signature_expected!=$request->request->get('signature')) {
			return new JsonResponse(array('code' => 106, 'The request has rejected caused by the invalid given signature'));
		}
		$depot->setEtat($em->getRepository('GuediaMainBundle:Etat')->find($request->request->get('status')));
		$depot->setResultat($request->request->get('message'));
		$em->persist($depot);
		$em->persist(Historique::createFromDepot($depot, $this->getUser()));
		$em->flush();
		$this->get('guedia_mailer')->sendAfterExecuteTransaction($depot, $this->getMyParameter('ids', array('etat')));
		return new JsonResponse(array('code' => 200, 'token' => $signature_expected, 'message' => "The trasaction has been validate successfully"));
	}
	
	/**
	 * @QMLogger(message="Récupération des transactions en cours")
	 * @Route ("/list_pending_transaction", name="list_pending_transaction")
	 */
	public function listPendingAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$phonecode = $request->request->get('phonecode');
		$date = $request->request->get('date');
		if(null == $country = $em->getRepository('GuediaMainBundle:Country')->findOneByPhonecode($request->request->get('phonecode'))) {
			return new JsonResponse(array('code' => 101, 'The phonecode is undefined'));
		}
		if(null == $sim = $em->getRepository('GuediaMainBundle:Puce')->findOneByTelephone($request->request->get('sim'))) {
			return new JsonResponse(array('code' => 102, 'The sim is undefined'));
		}
		if(null == $sim = $em->getRepository('GuediaMainBundle:Puce')->findOneBy(array('telephone' => $request->request->get('sim'), 'pays' => $country->getId()))) {
			return new JsonResponse(array('code' => 103, 'The sim is undefined for this country'));
		}
		if(\DateTime::createFromFormat('YmdHis', $request->request->get('date')) == false) {
			return new JsonResponse(array('code' => 104, 'The date format is invalid'));
		}
		$interval = abs(strtotime($request->request->get('date')) - time());
		if($interval > 300) {
			return new JsonResponse(array('code' => 105, 'The request has expired'));
		}
		$message = null;
		$signature_expected = $this->get('guedia_signature')->generateToken($request->request->get('sim'), $phonecode, $date, null, $message);
		$em->persist(Trace::logRequest($sim, $signature_expected, $request->request->get('signature')));
		if($signature_expected!=$request->request->get('signature')) {
			return new JsonResponse(array('code' => 106, 'The request has rejected caused by the invalid given signature.\n'.sprintf(
					'SIM = %s | PHONECODE = %s | DATE = %s.\n Signature attendue = %s', $request->request->get('sim'), $phonecode, $date, $signature_expected
				)));
		}
		$arData = array();
		$entities = $em->getRepository('GuediaMainBundle:Depot')->findBy(array('puce' => $sim->getId(), 'etat' => $this->getMyParameter('ids', array('etat', 'confirmed'))));
		foreach($entities as $entity) {
			$entity->setToken($signature_expected);
			$em->persist($entity);
			$arData[] = $entity->getDataForPhone();
		}
		$em->flush();
		return new JsonResponse(array('code' => 200, 'token' => $signature_expected, 'data' => $arData));
	}
	
	/**
	 * @Route ("/transaction/list_identity_type", name="list_identity_type")
	 */
	public function listIdentityTypeAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('GuediaMainBundle:IdentiteType')->createQueryBuilder('q')
		->getQuery()->getResult();
		$data = array();
		foreach($entities as $entity) {
			array_push($data, array('id' => $entity->getId(), 'name' => $entity->getName()));
		}
		return new JsonResponse($data);
	}
	
	/**
	 * @Route ("/transaction/list_type_transfer", name="list_transfer_type")
	 */
	public function listTransferTypeAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('GuediaMainBundle:TransfertType')->createQueryBuilder('q')
			->getQuery()->getResult();
		$data = array();
		foreach($entities as $entity) {
			array_push($data, array('id' => $entity->getId(), 'name' => $entity->getName()));
		}
		return new JsonResponse($data);
	}
	
	/**
	 * @Route ("/transaction/customer_by_identity", name="customer_by_identity")
	 */
	public function customerByIdentityAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$data = $request->request->all();
		$entity = $em->getRepository('GuediaMainBundle:Client')->findByCriteria(
				isset($data['clientIdentiteType']) ? $data['clientIdentiteType'] : null, 
				isset($data['clientIdentite']) ? $data['clientIdentite'] : null, $this->getUser()
			);
		return new JsonResponse($entity==null ? array('result' => false, 'data' => array()) : array('result' => true, 'data' => array(
				'prenom' => $entity->getPrenom(), 'nom' => $entity->getNom(), 'telephone' => $entity->getTelephone(),
				'email' => $entity->getEmail(), 'address' => $entity->getAddress(), 'identite' => $entity->getIdentite()
			)));
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Guedia\MainBundle\Entity\Depot $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		$intl = new \IntlDateFormatter($this->get('request')->getLocale(), \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);
		return array(
				'id'		=> $entity->getId(),
				'reference'	=> sprintf('<a callback="showTransaction" class="link-effect link-effect-3"><span data-hover="%s">%s</span></a>', $entity->getReference(), $entity->getReference()),
				'sender' 	=> $entity->getClient() ? sprintf('%s %s', $entity->getClient()->getPrenom(), $entity->getClient()->getNom()) : 'Non renseigné',
				'recipient' => sprintf('%s %s', $entity->getBeneficiairePrenom(), $entity->getBeneficiaireNom()),
				'date' 		=> $intl->format($entity->getDate()),
				'amount' 	=> number_format($entity->getMontantPaye(), 2).' $',
				'status' 	=> $this->showEntityStatus($entity, 'etat'),
				'resultat'	=> $entity->getResultat()
			);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setOrder(QueryBuilder $queryBuilder, $aColumns, Request $request) {
		parent::setOrder($queryBuilder, array('reference' => 'q.reference', 'date' => 'q.date', 'amount' => 'q.montantPaye'), $request);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('q.reference', 'c.prenom', 'c.nom', 'v.firstname', 'v.lastname', 'q.beneficiairePrenom', 'q.beneficiaireNom', 'e.libelle'), $request);
	}
}
