<?php
namespace Guedia\MainBundle\Controller;

use Guedia\MainBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Annotation\QMLogger;
use Guedia\MainBundle\Entity\Frais;

class FeeController extends BaseController {
	
	/**
	 * @QMLogger(message="Liste des puces")
	 * @Route ("/fee/list", name="fee_list")
	 */
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$queryBuilder = $em->getRepository('GuediaMainBundle:Frais')->createQueryBuilder('q');
		return $this->paginate($request, $queryBuilder);
	}
	
	/**
	 * @Route ("/{amount}/fee_get", name="fee_get")
	 */
	public function getAction($amount) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Frais')->createQueryBuilder('q')
			->andWhere('q.montantMin <= :amount AND q.montantMax >= :amount')->setParameter('amount', $amount)
			->getQuery()->getOneOrNullResult();
		return new JsonResponse($entity ? array('success' => true, 'tarif' => $entity->getTrueTarif($amount)) : array('success' => false, 'tarif' => null));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('q.telephone'), $request);
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Guedia\MainBundle\Entity\Frais $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		return array(
				'id'			=> $entity->getId(),
				'montant_min'	=> $entity->getMontantMin(),
				'montant_max' 	=> $entity->getMontantMax(),
				'tarif' 		=> $entity->getTarif()
			);
	}
}
