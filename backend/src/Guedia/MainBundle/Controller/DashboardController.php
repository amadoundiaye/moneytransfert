<?php
namespace Guedia\MainBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\Country;
use Guedia\MainBundle\Annotation\QMLogger;
use Guedia\MainBundle\Entity\Session;

class DashboardController extends BaseController
{
	
	/**
	 * @Route ("/", name="homepage")
	 */
	public function indexFilterAction(Request $request)
	{
		return new JsonResponse();
	}
	
	/**
	 * @Route ("/reset_filter", name="reset_filter")
	 */
	public function resetFilterAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$data = array('from' => strtotime('first day of this month'), 'end' => strtotime('now'));
		$em->merge(Session::create(Session::$dashboard, $this->getUser(), $data));
		$em->flush();
		$this->get('session')->set('dashboard_criteria', $data);
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @QMLogger(message="Filtre du tableau de bord")
	 * @Route ("/filter_by_date", name="filter_by_date")
	 */
	public function filterBydateAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$criteria = $request->request->all();
		$data = array(
				'from'	=> \DateTime::createFromFormat('M d, Y', $criteria[0])->getTimestamp(),
				'end'	=> \DateTime::createFromFormat('M d, Y', $criteria[1])->getTimestamp()
			);
		$em->merge(Session::create(Session::$dashboard, $this->getUser(), $data));
		$em->flush();
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @Route ("/synthese_per_number", name="synthese_per_number")
	 */
	public function synthesePerNumberAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$session = $em->getRepository('GuediaMainBundle:Session')->findOneBy(array('action' => Session::$dashboard, 'utilisateur' => $this->getUser()));
		$data = $em->getRepository('GuediaMainBundle:Depot')->synthesePerNumber($this->getUser(), $session->getDataToArray());
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @Route ("/commission_per_country", name="commission_per_country")
	 */
	public function commissionPerCountryAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$session = $em->getRepository('GuediaMainBundle:Session')->findOneBy(array('action' => Session::$dashboard, 'utilisateur' => $this->getUser()));
		$arrCountry = $this->getDoctrine()->getRepository('GuediaMainBundle:Country')->findAll();
		$data = $em->getRepository('GuediaMainBundle:Depot')->commissionPerCountry($this->getUser(), $session->getDataToArray());
		$data = $this->get('guedia_mapping')->commissionPerCountry($data, $arrCountry);
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @Route ("/commission_and_amount_per_month", name="commission_and_amount_per_month")
	 */
	public function commissionAndAmountPerMonthAction(Request $request)
	{
		$data = $this->getDoctrine()->getRepository('GuediaMainBundle:Depot')->commissionAndAmountPerMonth($this->getUser());
		$data = $this->get('guedia_mapping')->commissionAndAmountPerMonth($data);
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @Route ("/commission_and_amount_per_cashier", name="commission_and_amount_per_cashier")
	 */
	public function commissionAndAmountPerCashierAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$session = $em->getRepository('GuediaMainBundle:Session')->findOneBy(array('action' => Session::$dashboard, 'utilisateur' => $this->getUser()));
		$data = $em->getRepository('GuediaMainBundle:Depot')->commissionAndAmountPerCashier($this->getUser(), $session->getDataToArray());
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @Route ("/evolution_in_one_month", name="evolution_in_one_month")
	 */
	public function evolutionInOneMonthAction(Request $request)
	{
		$data = $this->getDoctrine()->getRepository('GuediaMainBundle:Depot')->evolutionInOneMonth($this->getUser());
		$data = $this->get('guedia_mapping')->evolutionInOneMonth($data);
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
}