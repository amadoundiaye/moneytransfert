<?php
namespace Guedia\MainBundle\Controller;

use Guedia\MainBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\Country;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Guedia\MainBundle\Entity\Configuration;
use Guedia\MainBundle\Annotation\QMLogger;

class ConfigurationController extends BaseController {
	
	/**
	 * @QMLogger(message="Visualisation des paramétres")
	 * @Route ("/show_config", name="show_config")
	 */
	public function showAction() {
		$em = $this->getDoctrine()->getManager();
		$arrData = array();
		$entities = $em->getRepository('GuediaMainBundle:Configuration')->findAll();
		foreach($entities as $entity) {
			if($this->getMyParameter('ids', array('transfertType', 'classic'))==$entity->getId()) {
				$label = 'classic';
			} else {
				$label = 'tac';
			}
			$arrData[$label] = array(
					'percentile' => array('shop' => $entity->getPourcentageBoutique(), 'society' => $entity->getPourcentageSociete()),
					'rate' => array('change' => $entity->getTauxChange(), 'commission' => $entity->getTauxTransfert())
			);
		}
		return new JsonResponse($arrData);
	}
	
	/**
	 * @QMLogger(message="Visualisation d'un paramétre")
	 * @Route ("/show_one_config/{token}", name="show_one_config")
	 */
	public function showOneAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Configuration')->createQueryBuilder('q')
			->innerJoin('q.transfertType', 'tt')
			->andWhere('md5(CONCAT(:salt, tt.id)) = :token')
			->setParameters(array('salt' => $this->getUser()->getSalt(), 'token' => $token))
			->getQuery()->getOneOrNullResult();
		return new JsonResponse($entity->toArray());
	}
	
	/**
	 * @QMLogger(message="Mise à jour des paramétres")
	 * @Route ("/update_config/{token}", name="update_config")
	 * @Method("POST")
	 */
	public function updateAction($token) {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GuediaMainBundle:Configuration')->findByToken($token);
		$form = $this->createCreateForm($entity, 'Configuration');
		$request = $this->get('request');
		$form->bind($request);
		if ($form->isValid()) {
			$em->persist($entity);
			$em->flush();
			return new JsonResponse(array('success' => true, 'message' => "The fee's parameters has been updated succesfully !!!"));
		}
		return new JsonResponse(array('success' => false, 'message' => "Error occured when user attempt to update fee's parameters !!!"));
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Orange\MainBundle\Entity\Country $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		return array(
				'id'		=> $entity->getId(),
				'iso'		=> $entity->getIso(),
				'iso3' 		=> $entity->getIso3(),
				'name' 		=> $entity->getName(),
				'phonecode' => $entity->getPhonecode(),
				'status' 	=> $this->showEntityStatus($entity, 'etat'),
				'action'	=> $this->get('guedia_action')->generateActionsForCountry($entity)
			);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('bu.libelle'), $request);
	}
}
