<?php
namespace Guedia\MainBundle\Controller;

use Guedia\MainBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Guedia\MainBundle\Entity\Depot;
use Guedia\MainBundle\Annotation\QMLogger;
use Guedia\MainBundle\Entity\Trace;
use Guedia\MainBundle\Entity\Historique;
use Guedia\MainBundle\Entity\Session;
use Doctrine\ORM\QueryBuilder;

class TraceController extends BaseController {
	
	
	/**
	 * @Route ("/reset_filter_for_trace", name="reset_filter_for_trace")
	 */
	public function resetFilterAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$data = array('from' => strtotime('first day of this month'), 'end' => strtotime('now'));
		$em->merge(Session::create(Session::$trace, $this->getUser(), $data));
		$em->flush();
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @QMLogger(message="Filtre du tableau de bord")
	 * @Route ("/filter_by_date_for_trace", name="filter_by_date_for_trace")
	 */
	public function filterBydateAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$criteria = $request->request->all();
		$data = array(
				'from'	=> \DateTime::createFromFormat('M d, Y', $criteria[0])->getTimestamp(),
				'end'	=> \DateTime::createFromFormat('M d, Y', $criteria[1])->getTimestamp()
		);
		$em->merge(Session::create(Session::$trace, $this->getUser(), $data));
		$em->flush();
		return new JsonResponse(array('success' => true, 'data' => $data));
	}
	
	/**
	 * @QMLogger(message="Liste des traces")
	 * @Route("/trace/list", name="trace_list")
	 */
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$session = $em->getRepository('GuediaMainBundle:Session')->findOneBy(array('action' => Session::$trace, 'utilisateur' => $this->getUser()));
		$queryBuilder = $em->getRepository('GuediaMainBundle:Historique')->listQueryBuilder($session->getDataToArray());
		$this->get('session')->set('transaction_query', array('query' => $queryBuilder->getDql(), 'param' => $queryBuilder->getParameters()));
		return $this->paginate($request, $queryBuilder);
	}
	
	/**
	 * @QMLogger(message="Extraction des traces")
	 * @Route("/trace/export", name="export_trace")
	 * @Template()
	 */
	public function exportAction() {
		$em = $this->getDoctrine()->getManager();
		$qb = $em->getRepository('GuediaMainBundle:Historique')->listForExportBuilder($this->getUser());
		$objWriter = $this->get('guedia_extraction')->exportTransaction($this->get('guedia_mapping')->listForExport($qb->getQuery()->getResult()));
		$objWriter->save(WEB_DIR.'/download/transaction.xlsx');
		return new JsonResponse(array('success' => true, 'url' => $this->generateUrl('homepage')."../"));
	}
	
	/**
	 * @todo retourne le nombre d'enregistrements renvoyer par le résultat de la requête
	 * @param \Guedia\MainBundle\Entity\Historique $entity
	 * @param QueryBuilder $queryBuilder
	 * @return array
	 */
	protected function addRowInTable($entity) {
		$intl = new \IntlDateFormatter($this->get('request')->getLocale(), \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);
		$depot = $entity->getDepot();
		return array(
				'id'		=> $entity->getId(),
				'transactionId'	=> $depot->getId(),
				'reference'	=> sprintf('<a callback="showTransaction" class="link-effect link-effect-3"><span data-hover="%s">%s</span></a>', $depot->getReference(), $depot->getReference()),
				'date' 		=> $intl->format($entity->getDate()),
				'user' 		=> $entity->getUtilisateur() ? $entity->getUtilisateur()->getFirstName().' '.$entity->getUtilisateur()->getLastname() : null,
				'status' 	=> $this->showEntityStatus($entity, 'etat')
			);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setOrder(QueryBuilder $queryBuilder, $aColumns, Request $request) {
		parent::setOrder($queryBuilder, array('reference' => 'd.reference', 'date' => 'q.date'), $request);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Orange\MainBundle\Controller\BaseController::setFilter()
	 */
	protected function setFilter($queryBuilder, $aColumns, $request) {
		parent::setFilter($queryBuilder, array('u.firstname', 'u.lastname', 'd.reference'), $request);
	}
}
