<?php
namespace Guedia\MainBundle\Service;

class Signature {
	
	/**
	 * @return string
	 */
	function generateToken($sim, $phonecode, $request_date, $token = null, &$mess) {
		$cle_bin = pack("H*", '8E953A24656C7909214D134F45B5687AC98E908FE675C54F5325A45B657676FF');
		$data = array();
		$data['SIM'] = $sim;
		$data['PHONECODE'] = $phonecode;
		$data['DATE'] = $request_date;
		if($token) {
			$data['TOKEN'] = $token;
		}
		ksort($data);
		$message = http_build_query($data);
		//$mess = $message.'  ---  '.$cle_bin;
		return strtoupper(hash_hmac('sha512', $message, $cle_bin));
	}
	
}
