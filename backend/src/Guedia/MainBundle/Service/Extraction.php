<?php
namespace Guedia\MainBundle\Service;

class Extraction extends Excel {
	
	/**
	 * @param array $arrData
	 * @return \PHPExcel_Writer_IWriter
	 */
	public function exportTransactionForAdministrator($arrData, $row = 1) {
		$excelMap = array(
				'A'=>'numero', 'B'=>'date', 'C'=>'heure', 'D'=>'reference', 'E' => 'vendeur', 'F'=>'beneficiairePays', 'G'=>'transfertMode', 
				'H'=>'change', 'I'=>'effectiveChange', 'J'=>'montantPaye', 'K'=>'commissions', 'L'=>'beneficiaireTelephone',
				'M'=>'beneficiaireNom', 'N'=>'beneficiairePrenom', 'O'=>'beneficiaireMontant', 'P'=>'clientTelephone', 'Q'=>'clientNom', 'R' => 'clientPrenom',
				'S'=>'clientIdentite', 'T'=>'montantEnvoye', 'U'=>'commisionChange', 'V'=>'commissionSoftGal', 'W'=>'reversementMarchand'
			);
		$label = array(
				'D'=>'Référence', 'E'=>'Nom Marchand', 'F'=>'Pays', 'G'=>'Type', 'H'=>'Change', 'I'=>'effective Change', 'J'=>'Montant Reçu', 
				'K'=>'Commissions', 'L'=>'Phone', 'M'=>'Nom', 'N'=>'Prénom', 'O'=>'Montant reçu', 'P'=>'Phone', 'Q'=>'Nom', 
				'R'=>'Prénom', 'S'=>'ID', 'T'=>'Montant Envoyé', 'U'=>'Commission change', 'V'=>'Commission SOFTGAL', 'W'=>'Reversement Marchant '
			);
		$this->setColumnWidth(array(
				'A' => 5, 'B' => 10, 'C' => 10, 'D' => 20, 'E' => 20, 'F' => 10, 'G' => 10, 'H' => 10, 'I' => 15, 'J' => 15, 'K' => 15, 'L' => 10, 
				'M' => 10, 'N' => 15, 'O' => 15, 'P' => 12, 'Q' => 10, 'R' => 15, 'S' => 15, 'T' => 15, 'U' => 20, 'V' => 20, 'W' => 20
			));
		$this->addRowLabel(array('A'=>'N°', 'B'=>'Date', 'C'=>'Heure'), 1, array('size' => 18));
		$this->addRowLabel(array('D' => 'Marchands', 'L' => 'Bénéficiaire', 'P' => 'Sender', 'U' => 'SOFTGAL'), 1, array('size' => 18));
		$this->addRowLabel($label, 2, array('size' => 13));
		$this->mergeCells(array(array('A1', 'A2'), array('B1', 'B2'), array('C1', 'C2'), array('D1', 'K1'), array('L1', 'O1'), array('P1', 'T1'), array('U1', 'W1')));
		$row = 2;
		$sum = array('MR'=>0, 'CM'=>0, 'CC'=>0, 'CS'=>0, 'RM'=>0);
		foreach($arrData as $data) {
			$row++;
			$this->addRowData($data, $excelMap, $row);
			$sum = array(
					'MR'=>$sum['MR']+$data['montantPaye'], 'CM'=>$sum['CM']+$data['commissions'], 
					'CC'=>$sum['CC']+$data['commisionChange'], 'CS'=>$sum['CS']+$data['commissionSoftGal'], 'RM'=>$sum['RM']+$data['reversementMarchand']
				);
		}
		$this->addRowData($sum, array('J'=>'MR', 'K'=>'CM', 'U'=>'CC', 'V'=>'CS', 'W'=>'RM'), $row+2);
		$this->applyStyleRanges(array(sprintf('A%s:W%s', $row+2, $row+2)), array('size'=>20, 'bold'=>true));
		$this->applyStyleRanges(
				array('A1:W'.$row, 'A1:W2', 'D1:W1', 'A1:A'.$row, 'B1:B'.$row, 'C1:C'.$row, 'D1:K'.$row, 'L1:O'.$row, 'P1:T'.$row, 'U1:W'.$row),
				array('outline_border' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))
			);
		$this->applyStyleRanges(array('A1:W'.$row), array('inline_border' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));
		return \PHPExcel_IOFactory::createWriter($this, 'Excel2007');
	}
	
	/**
	 * @param array $arrData
	 * @return \PHPExcel_Writer_IWriter
	 */
	public function exportTransactionForCashier($arrData, $row = 1) {
		$excelMap = array(
				'A'=>'numero', 'B'=>'date', 'C'=>'heure', 'D'=>'reference', 'E'=>'beneficiairePays', 'F'=>'transfertMode', 'G'=>'effectiveChange', 
				'H'=>'montantPaye', 'I'=>'commissions', 'J'=>'reversementMarchand', 'K'=>'beneficiaireTelephone', 'L'=>'beneficiaireNom', 'M'=>'beneficiairePrenom', 
				'N'=>'beneficiaireMontant', 'O'=>'clientTelephone', 'P'=>'clientNom', 'Q' => 'clientPrenom', 'R'=>'clientIdentite', 'S'=>'montantEnvoye'
			);
		$label = array(
				'D'=>'Référence', 'E'=>'Pays', 'F'=>'Type', 'G'=>'effective Change', 'H'=>'Montant Reçu', 'I'=>'Commissions', 'J'=>'Reversement SOFTGAL', 
				'K'=>'Phone', 'L'=>'Nom', 'M'=>'Prénom', 'N'=>'Montant reçu', 'O'=>'Phone', 'P'=>'Nom', 'Q'=>'Prénom', 'R'=>'ID', 'S'=>'Montant Envoyé'
			);
		$this->setColumnWidth(array(
				'A' => 5, 'B' => 10, 'C' => 10, 'D' => 20, 'E' => 20, 'F' => 10, 'G' => 10, 'H' => 10, 'I' => 15, 'J' => 20, 
				'K' => 15, 'L' => 15, 'M' => 15, 'N' => 15, 'O' => 20, 'P' => 20, 'Q' => 20, 'R' => 20, 'S' => 20
			));
		$this->addRowLabel(array('A'=>'N°', 'B'=>'Date', 'C'=>'Heure'), 1, array('size' => 18));
		$this->addRowLabel(array('D' => 'Marchands', 'K' => 'Bénéficiaire', 'O' => 'Sender'), 1, array('size' => 18));
		$this->addRowLabel($label, 2, array('size' => 13));
		$this->mergeCells(array(array('A1', 'A2'), array('B1', 'B2'), array('C1', 'C2'), array('D1', 'J1'), array('K1', 'O1'), array('P1', 'S1')));
		$row = 2;
		foreach($arrData as $data) {
			$row++;
			$this->addRowData($data, $excelMap, $row);
		}
		$this->applyStyleRanges(
				array('A1:S'.$row, 'A1:S2', 'D1:S1', 'A1:A'.$row, 'B1:B'.$row, 'C1:C'.$row, 'D1:J'.$row, 'K1:O'.$row, 'P1:S'.$row),
				array('outline_border' => array('style' => \PHPExcel_Style_Border::BORDER_MEDIUM))
			);
		$this->applyStyleRanges(array('A1:S'.$row), array('inline_border' => array('style' => \PHPExcel_Style_Border::BORDER_THIN)));
		return \PHPExcel_IOFactory::createWriter($this, 'Excel2007');
	}
	
	/**
	 * @return \PHPExcel_Writer_IWriter
	 */
	public function oldExportTransaction($arrData, $row = 1) {
		$excelMap = array(
				'A'=>'clientPrenom', 'B'=>'clientNom', 'C'=>'clientTelephone', 'D'=>'clientIdentite',
				'E'=>'beneficiairePrenom', 'F'=>'beneficiaireNom', 'G'=>'beneficiairePays', 'H'=>'beneficiaireTelephone',
				'I'=>'montantPaye', 'J'=>'commissionSociete', 'K'=>'devise', 'L'=>'transfertMode', 'M'=>'transfertType', 'N'=>'montantSaisi', 'O'=>'beneficiaireMontant'
		);
		$label = array(
				'A'=>'Firstname', 'B'=>'Name', 'C'=>'Phone', 'D'=>'Identity',
				'E'=>'Firstname', 'F'=>'Name', 'G'=>'Country', 'H'=>'Phone',
				'I'=>'Amount paid', 'J'=>'Softgal commission', 'K'=>'currency', 'L'=>'Transfert type', 'M'=>'Transfert mode', 'N'=>'Written amount', 'O'=>'Received amount'
		);
		$this->applyStyleRanges(array('A1:D1'), array('background' => 'ECECEC'));
		$this->applyStyleRanges(array('E1:H1'), array('background' => 'FFF8DC'));
		$this->applyStyleRanges(array('I1:O1'), array('background' => 'F08080'));
		$this->applyStyleRanges(array('A2:O2'), array('background' => '4682B4'));
		$this->setColumnWidth(array(
				'A' => 40, 'B' => 30, 'C' => 15, 'E' => 40, 'F' => 30, 'H' => 10, 'I' => 15,
				'J' => 20, 'K' => 15, 'K' => 15, 'L' => 15, 'M' => 15, 'N' => 15, 'O' => 20
		));
		$this->mergeCells(array(array('A1', 'D1'), array('E1', 'H1'), array('I1', 'O1')));
		$this->addRowLabel(array('A' => 'Client', 'E' => 'Bénéficiaire', 'I' => 'Montant'), 1, array('size' => 18));
		$this->addRowLabel($label, 2, array('size' => 13));
		$row = 3;
		foreach($arrData as $data) {
			$this->addRowData($data, $excelMap, $row);
			$row++;
		}
		return \PHPExcel_IOFactory::createWriter($this, 'Excel2007');
	}
	
}
