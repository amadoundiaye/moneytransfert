<?php
namespace Guedia\MainBundle\Service;

class Action {
	const ACTION_TEMPLATE = '<a href="#" type="button" callback="%s"> <span class="glyphicon %s"></span>%s</a>';
	const BUTTON_TEMPLATE = '<a type="button" callback="%s"><button type="button" class="btn btn-sm %s"> <span class="glyphicon %s"></span>%s</button></a>';
	const ACTION_MODAL_TEMPLATE = '<a title="%" href="#myModal" class="actionLink" modal-url="%s" data-target="#myModal" data-toggle="modal"><img src="%s" /></a>';
	const BUTTON_MODAL_TEMPLATE = '<a title="%" href="#myModal" class="actionLink" modal-url="%s" data-target="#myModal" data-toggle="modal"><img src="%s" /></a>';
	
	/**
	 * @var \Twig_Environment
	 */
	private $twig;
	
	/**
	 * @var \Symfony\Component\Routing\Router
	 */
	private $router;
	
	/**
	 * @var \Orange\MainBundle\Entity\Utilisateur
	 */
	private $user;
	
	/**
	 * @var array
	 */
	private $states;
	
	/**
	 * @var string
	 */
	private $actions;
	
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $em;
	
	/**
	 * @param \Twig_Environment $twig
	 * @param \Symfony\Component\Routing\Router $router
	 * @param array $states
	 * @param \Symfony\Component\Security\Core\SecurityContext $security_context
	 * @param \Orange\QuickMakingBundle\Model\EntityManager $em
	 */
	public function __construct($twig, $router, $states, $security_context, $em) {
		$this->twig = $twig;
		$this->router = $router;
		$this->states = $states;
		$this->user = $security_context->getToken()->getUser();
		$this->em = $em;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $entity
	 * @return string
	 */
	public function generateActionsForUser($entity) {
		$this->actions = null;
		$this->addButton('editUser', 'btn-default', 'glyphicon-edit', 'Edit');
		if($entity->isEnabled()) {
			$this->addButton('statusUser', 'btn-red', 'glyphicon-ban-circle', 'Disable');
		} else {
			$this->addButton('statusUser', 'btn-success', 'glyphicon-ok-circle', 'Enable');
		}
		return $this->actions;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Puce $entity
	 * @return string
	 */
	public function generateActionsForSim($entity) {
		$this->actions = null;
		$this->addButton('editSim', 'btn-default', 'glyphicon-edit', 'Edit');
		if($entity->getEtat()) {
			$this->addButton('statusSim', 'btn-red', 'glyphicon-ban-circle', 'Disable');
		} else {
			$this->addButton('statusSim', 'btn-success', 'glyphicon-ok-circle', 'Enable');
		}
		return $this->actions;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Country $entity
	 * @return string
	 */
	public function generateActionsForCountry($entity) {
		$this->actions = null;
		$this->addButton('showCountry', 'btn-default', 'glyphicon-search', 'Details');
		$this->addButton('editCountry', 'btn-default', 'glyphicon-edit', 'Edit');
		return $this->actions;
	}
	
	/**
	 * @param string $target
	 * @param string $callback
	 * @param string $color
	 * @param string $icon
	 * @param string $label
	 * @param string $isModal
	 */
	private function addAction($callback, $icon, $color, $label, $isModal = false) {
		$this->actions .= sprintf($isModal ? self::ACTION_MODAL_TEMPLATE : self::ACTION_TEMPLATE, $callback, $color, $icon, $label);
	}
	
	/**
	 * @param string $target
	 * @param string $callback
	 * @param string $icon
	 * @param string $label
	 * @param string $isModal
	 */
	private function addButton($callback, $color, $icon, $label, $isModal = false) {
		$this->actions .= sprintf($isModal ? self::BUTTON_MODAL_TEMPLATE : self::BUTTON_TEMPLATE, $callback, $color, $icon, $label);
	}
	
	/**
	 * @return string
	 */
	private function showActions() {
		return sprintf('<div class="btn-group">
		  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    Action <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu">%s
		  </ul></div>', $this->actions);
	}
}
