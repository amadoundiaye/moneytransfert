<?php
namespace Guedia\MainBundle\Service;

class Mailer {
	
	/**
	 * @var string
	 */
	private $from = "contact@softgal.ca";
	
	/**
	 * @var array
	 */
	private $bcc = array("madiorfaye11@gmail.com", "samdroid299@gmail.com");
	
	/**
	 * @var string
	 */
	private $name = " Transfert Mobile";
	
	/**
	 * @var \Swift_Mailer
	 */
	protected $mailer;
	
	/**
	 * @var \Twig_Environment
	 */
	private $twig;
	
	/**
	 * @param \Twig_Environment $twig
	 * @param \Symfony\Component\Routing\Router $router
	 */
	public function __construct($mailer, $twig) {
		$this->mailer = $mailer;
		$this->twig = $twig;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @return string
	 */
	public function sendNewPassword($user) {
		return $this->sendMail(
				'Réinitialisation de mot de passe', $this->twig->render('GuediaMainBundle:User:new_password.html.twig', array('user' => $user)), $user->getEmail(), null, null, 5
			);
	}
	
	/**
	 * @param array $admins
	 * @param array $data
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @return string
	 */
	public function contactUs($admins, $data, $user) {
		$emails = array();
		foreach($admins as $admin) {
			$emails[] = $admin->getEmail();
		}
		$message = sprintf('%s<br><br>%s<br>%s', $data['message'], $user->getFirstName().' '.$user->getLastname(), $user->getEmail());
		return $this->sendMail($data['subject'], $message, $emails, null, null, 5);
	}
	
	/**
	 * @param array $customers
	 * @param array $data
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @return string
	 */
	public function sendPromo($customers, $data, $user) {
		$emails = array();
		$number = 0;
		foreach($customers as $customer) {
			$emails[] = $customer->getEmail();
			if($number==25) {
				$this->sendMail($data['subject'], $data['message'], $emails, null, null, 5);
				$emails = array();
			}
		}
		if(count($emails) > 0) {
			$this->sendMail($data['subject'], $data['message'], $emails, null, null, 5);
		}
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @return string
	 */
	public function sendNewAccount($user) {
		return $this->sendMail(
				"Création d'un compte", $this->twig->render('GuediaMainBundle:User:new_account.html.twig', array('user' => $user)), $user->getEmail(), null, null, 5
			);
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Depot $depot
	 * @param array $admins
	 * @param float $exchangerate
	 * @return string
	 */
	public function confirmTransaction($depot, $exchangerate) {
		$emails = array('contact@softgal.ca');
		$this->bcc[] = 'amadou.ndiaye@gmail.com';
		$subject = sprintf('Confirmation transaction %s', $depot->getReference());
		$body = $this->twig->render('GuediaMainBundle:Transaction:confirm.html.twig', array('depot' => $depot, 'exchangerate' => $exchangerate));
		return $this->sendMail($subject, $body, $emails, null, null, 5);
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Depot $depot
	 * @param number $exchangerate
	 * @return string
	 */
	public function sendTransaction($depot, $exchangerate) {
		$subject = sprintf('Transaction %s', $depot->getReference());
		$body = $this->twig->render('GuediaMainBundle:Transaction:send.html.twig', array('depot' => $depot, 'exchangerate' => $exchangerate));
		return $this->sendMail($subject, $body, $depot->getClient()->getEmail(), $depot->getVendeur()->getEmail(), null, 5);
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Depot $depot
	 * @param array $admins
	 * @param array $etats
	 * @return string
	 */
	public function sendAfterExecuteTransaction($depot, $etats) {
		$subject = 'Execution of transaction :: ';
		$to = $cc = $emails = array();
		$emails[] = 'contact@softgal.ca';
		if(in_array($depot->getEtat()->getId(), array($etats['validate'], $etats['rejected']))) {
			$to = ($depot->getClient()->getEmail() && !empty($depot->getClient()->getEmail())) ? array($depot->getClient()->getEmail()) : array();
			$cc = array($depot->getVendeur()->getEmail());
		}
		if($depot->getEtat()->getId()==$etats['validate']) {
			$subject .= 'Done';
		} elseif($depot->getEtat()->getId()==$etats['rejected']) {
			$subject .= 'Rejected';
		} elseif($depot->getEtat()->getId()==$etats['non_credit']) {
			$subject .= 'Non credit';
			$to = array($depot->getVendeur()->getEmail());
			$cc = $emails;
			$this->bcc[] = 'contact@softgal.ca';
			$this->bcc[] = 'amadou.ndiaye@gmail.com';
		}
		$body = $this->twig->render('GuediaMainBundle:Transaction:execute.html.twig', array('depot' => $depot, 'etats' => $etats));
		return $this->sendMail($subject, $body, $to, $cc, null, 5);
	}
	
	/**
	 * @param string $subject
	 * @param mixed $body
	 * @param mixed $toEmail
	 * @param string $attach
	 * @param number $to_send
	 * @return boolean
	 */
	public function sendMail($subject, $body, $toEmail, $ccEmail = array(), $attach = null, $to_send = 7) {
		if($toEmail==null || !count($toEmail)) {
			return false;
		}
		$message = \Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array($this->from => $this->name))
			->setContentType('text/html')
			->setBody($body);
		if($to_send >= 4) {
			$message->setTo($toEmail);
		}
		if(($to_send / 2) % 2 == 1) {
			$message->setCc($ccEmail);
		}
		if($to_send % 2 == 1) {
			$message->setBcc($this->bcc);
		}
		if($attach) {
			if(is_array($attach)) {
				foreach($attach as $filename => $fiche) {
					$message->attach(\Swift_Attachment::fromPath($fiche)->setFilename($filename));
				}
			} else {
				$message->attach(\Swift_Attachment::fromPath($attach));
			}
		}
		try {
			$isSend = $this->mailer->send($message);
		} catch(\Swift_TransportException $e) {
			$isSend = false;
		}
		return $isSend;
	}
	
}
