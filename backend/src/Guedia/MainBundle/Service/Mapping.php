<?php
namespace Guedia\MainBundle\Service;

class Mapping {
	
	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function listForExport($collection) {
		$arrData = array();
		$index = 1;
		foreach($collection as $depot) {
			$arrData[] = array(
					'numero' => $index,
					'reference' => $depot->getReference(),
					'date' => $depot->getDate()->format('d/m/Y'),
					'heure' => $depot->getDate()->format('H:i'),
					'vendeur' => $depot->getVendeur()->__toString(),
					'clientPrenom' => $depot->getClient()->getPrenom(),
					'clientNom' =>  $depot->getClient()->getNom(), 
					'clientTelephone' =>  $depot->getClient()->getTelephone(),
					'clientIdentite' =>  $depot->getClient()->getIdentite(),
					'change' =>  round($depot->getDevise(), 2),
					'effectiveChange' =>  round($depot->getEffectiveChange(), 2),
					'commissions' =>  round($depot->getCommissionBoutique(), 2),
					'beneficiairePrenom' => $depot->getBeneficiairePrenom(), 
					'beneficiaireNom' => $depot->getBeneficiaireNom(),
					'beneficiairePays' => $depot->getBeneficiairePays() ? $depot->getBeneficiairePays()->getName() : 'Non renseigné', 
					'beneficiaireTelephone' => $depot->getBeneficiaireTelephone(),
					'beneficiaireMontant' => $depot->getBeneficiaireMontant(),
					'montantPaye' => $depot->getMontantPaye(),
					'montantEnvoye' => round($depot->getMontantEnvoye(), 2),
					'commissionBoutique' => $depot->getCommissionBoutique(),
					'commissionSociete' => $depot->getCommissionSociete(), 
					'devise' => $depot->getDevise(), 
					'transfertMode' => $depot->getTransfertMode() ? 'à envoyer' : 'à recevoir', 
					'transfertType' => $depot->getTransfertType() ? $depot->getTransfertType()->getName() : 'Non renseigné', 
					'montantSaisi' => $depot->getMontant(),
					'status' => $depot->getEtat()->getLibelle(),
					'commisionChange' => round($depot->getCommissionChange(), 2),
					'commissionSoftGal' => round($depot->getCommissionBoutique() + $depot->getCommissionChange(), 2),
					'reversementMarchand' => $depot->getMontantPaye() - $depot->getCommissionBoutique()
				);
			$index += 1;
		}
		return $arrData;
	}
	
	/**
	 * @return array
	 */
	public function commissionAndAmountPerMonth($data) {
		$arrData = array();
		$date = \DateTime::createFromFormat('Y-m-d', date('Y-m-d'))->sub(new \DateInterval('P11M'));
		$montNames = ['', 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
		foreach($data as $month) {
			$arrData[$month['yearmonth']] = $month;
		}
		$data = $arrData;
		$arrData = array('month' => array(), 'amount' => array(), 'commission' => array());
		for($i=1;$i<=12;$i++) {
			$arrData['amount'][] = array($i, isset($data[$date->format('Ym')]) ? $data[$date->format('Ym')]['amount'] : 0);
			$arrData['commission'][] = array($i, isset($data[$date->format('Ym')]) ? $data[$date->format('Ym')]['commission'] : 0);
			$arrData['month'][] = array($i, $montNames[(int)$date->format('m')]);
			$date->add(new \DateInterval('P1M'));
		}
		ksort($arrData);
		return $arrData;
	}
	
	/**
	 * @return array
	 */
	public function commissionPerCountry($data, $arrCountry) {
		$arrData = array();
		$total = 0;
		foreach($data as $country) {
			$total += $country['value'];
		}
		foreach($data as $country) {
			$arrData[$country['id']] = array(
					'name' => $country['name'], 'value' => $country['value'], 'percent' => $total > 0 ? round($country['value']*100/$total) : 0
				);
		}
		foreach($arrCountry as $country) {
			if(isset($arrData[$country->getId()])==false) {
				$arrData[$country->getId()] = array('name' => $country->getName(), 'percent' => 0, 'value' => 0);
			}
		}
		return $arrData;
	}
	
	/**
	 * @return array
	 */
	public function evolutionInOneMonth($data) {
		$last = $data[count($data) - 1];
		if(count($data) > 0) {
			$arrData = array(
					'amount' => array('value' => $last['amount']), 'commission' => array('value' => $last['commission']), 
					'customer' => array('value' => $last['customer']), 'cashier' => array('value' => $last['cashier'])
				);
		} else {
			$arrData = array(
					'amount' => array('value' => 0, 'percent' => 0), 'commission' => array('value' => 0, 'percent' => 0),
					'customer' => array('value' => 0, 'percent' => 0), 'cashier' => array('value' => 0, 'percent' => 0)
				);
		}
		if(count($data)==2) {
			$arrData['amount']['percent'] = $data[0]['amount']!=0 ? ($data[1]['amount']-$data[0]['amount'])*100/$data[0]['amount']: 0;
			$arrData['commission']['percent'] = $data[0]['commission']!=0 ? ($data[1]['commission']-$data[0]['commission'])*100/$data[0]['commission']: 0;
			$arrData['customer']['percent'] = $data[0]['customer']!=0 ? ($data[1]['customer']-$data[0]['customer'])*100/$data[0]['customer']: 0;
			$arrData['cashier']['percent'] = $data[0]['cashier']!=0 ? ($data[1]['cashier']-$data[0]['cashier'])*100/$data[0]['cashier']: 0;
		} elseif(count($data)==1) {
			$arrData['amount']['percent'] = 100;
			$arrData['customer']['percent'] = 100;
			$arrData['cashier']['percent'] = 100;
			$arrData['commission']['percent'] = 100;
		}
		return $arrData;
	}
	
}
