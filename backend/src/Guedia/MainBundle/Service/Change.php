<?php
namespace Guedia\MainBundle\Service;

class Change {
	
	/**
	 * @return float
	 */
	public function getXOFtoCAD() {
		$XOFtoCADURL = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D%22http%3A%2F%2Ffinance.yahoo.com%2Fd%2Fquotes.csv%3Fe%3D.csv%26f%3Dnl1d1t1%26s%3Dxofcad%3DX%22%3B&format=json&diagnostics=true&callback=';
		$jsonXOFtoCAD = json_decode($this->exec_curl($XOFtoCADURL));
		if(is_object($jsonXOFtoCAD->query->results->row) && property_exists($jsonXOFtoCAD->query->results->row, 'col1')) {
			$change = $jsonXOFtoCAD->query->results->row->col1;
		} else {
			$XOFtoCADURL = 'https://www.google.com/finance/info?q=CURRENCY:CADXOF';
			$jsonXOFtoCAD = json_decode(substr($this->exec_curl($XOFtoCADURL), 4));
			$change = 1 / (float)$jsonXOFtoCAD[0]->l;
		}
		return $change;
	}
	
	/**
	 * @return float
	 */
	public function getCADtoXOF() {
		$CADtoXOFURL = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D%22http%3A%2F%2Ffinance.yahoo.com%2Fd%2Fquotes.csv%3Fe%3D.csv%26f%3Dnl1d1t1%26s%3Dcadxof%3DX%22%3B&format=json&diagnostics=true&callback=';
		$jsonCADtoXOF = json_decode($this->exec_curl($CADtoXOFURL));
		if(is_object($jsonCADtoXOF->query->results->row) && property_exists($jsonCADtoXOF->query->results->row, 'col1')) {
			$change = $jsonCADtoXOF->query->results->row->col1;
		} else {
			$XOFtoCADURL = 'https://www.google.com/finance/info?q=CURRENCY:CADXOF';
			$jsonXOFtoCAD = json_decode(substr($this->exec_curl($XOFtoCADURL), 4));
			$change = (float)$jsonXOFtoCAD[0]->l;
		}
		return $change;
	}
	
	/**
	 * @return float
	 */
	public function toArray() {
		return json_decode($this->exec_curl('http://127.0.0.1/calculator/currency.php'));
	}
	
	/**
	 * @param string $url
	 * @return string
	 */
	private function exec_curl($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Softgal');
		$resultat = curl_exec ($ch);
		curl_close($ch);
		return $resultat;
	}
	
}
