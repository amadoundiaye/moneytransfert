<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Puce
 * @ORM\Table(name="puce")
 * @ORM\Entity(repositoryClass="\Guedia\MainBundle\Repository\PuceRepository")
 * @UniqueEntity(fields="telephone", message="The sim already exists")
 */
class Puce
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var number
     * @ORM\Column(name="telephone", type="integer", length=11, nullable=true)
     * @Assert\NotNull(message="The number phone cannot be empty")
     */
    private $telephone;

    /**
     * @var \Pays
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @Assert\NotNull(message="The country cannot be empty")
     */
    private $pays;
    
    /**
     * @var string
     * @ORM\Column(name="motdepasse", type="string", length=45, nullable=true)
     * @Assert\NotNull(message="The pass ussd cannot be empty")
     */
    private $motdepasse;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Utilisateur", mappedBy="puce")
     */
    private $utilisateur;
    
    /**
     * @var boolean
     * @ORM\Column(name="etat", type="boolean")
     */
    private $etat = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->utilisateur = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set telephone
     * @param number $telephone
     * @return Puce
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     * @return number 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set pays
     * @param \Guedia\MainBundle\Entity\Country $pays
     * @return Puce
     */
    public function setPays(\Guedia\MainBundle\Entity\Country $pays = null)
    {
        $this->pays = $pays;
        return $this;
    }

    /**
     * Get pays
     * @return \Guedia\MainBundle\Entity\Country 
     */
    public function getPays()
    {
        return $this->pays;
    }
    
    /**
     * Set motdepasse
     * @param string $motdepasse
     * @return Puce
     */
    public function setMotdepasse($motdepasse)
    {
    	$this->motdepasse= $motdepasse;
    	return $this;
    }
    
    /**
     * Get motdepasse
     * @return string
     */
    public function getMotdepasse()
    {
    	return $this->motdepasse;
    }

    /**
     * Add utilisateur
     * @param \Guedia\MainBundle\Entity\Utilisateur $utilisateur
     * @return Puce
     */
    public function addUtilisateur(\Guedia\MainBundle\Entity\Utilisateur $utilisateur)
    {
        $this->utilisateur[] = $utilisateur;

        return $this;
    }

    /**
     * Remove utilisateur
     *
     * @param \Guedia\MainBundle\Entity\Utilisateur $utilisateur
     */
    public function removeUtilisateur(\Guedia\MainBundle\Entity\Utilisateur $utilisateur)
    {
        $this->utilisateur->removeElement($utilisateur);
    }

    /**
     * Get utilisateur
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
	
	/**
	 * check etat
	 * @return boolean
	 */
	public function getEtat() {
		return $this->etat;
	}
	
	/**
	 * set etat
	 * @param boolean $etat
	 * @return Puce
	 */
	public function setEtat($etat) {
		$this->etat = $etat;
		return $this;
	}
	
}
