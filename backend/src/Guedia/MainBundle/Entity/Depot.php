<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Depot
 * @ORM\Table(name="depot", indexes={@ORM\Index(name="fk_depot_utilisateur_idx", columns={"vendeur"}), @ORM\Index(name="fk_depot_etat1_idx", columns={"etat"}), @ORM\Index(name="fk_depot_puce1_idx", columns={"puce_id"})})
 * @ORM\Entity(repositoryClass="\Guedia\MainBundle\Repository\DepotRepository")
 */
class Depot
{
	static $transfertModes;
	
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var string
	 * @ORM\Column(name="reference", type="string", length=45, nullable=false)
	 */
	private $reference;
	
	/**
	 * @var string
	 * @ORM\Column(name="token", type="string", length=255, nullable=true)
	 */
	private $token;
	
	/**
	 * @var string
	 * @ORM\Column(name="resultat", type="string", length=255, nullable=true)
	 */
	private $resultat;

    /**
     * @var string
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;
    
    /**
     * @var float
     * @ORM\Column(name="montant", type="float", precision=10, nullable=false)
     */
    private $montant;
    
    /**
     * @var float
     * @ORM\Column(name="montant_paye", type="float", precision=10, nullable=false)
     */
    private $montantPaye;
    
    /**
     * @var float
     * @ORM\Column(name="devise", type="float", precision=10, nullable=false)
     */
    private $devise;
    
    /**
     * @var float
     * @ORM\Column(name="commission_boutique", type="float", precision=10, nullable=false)
     */
    private $commissionBoutique;
    
    /**
     * @var float
     * @ORM\Column(name="commission_societe", type="float", precision=10, nullable=false)
     */
    private $commissionSociete;
    
    /**
     * @var float
     * @ORM\Column(name="commission_om", type="float", precision=10, nullable=true)
     */
    private $commissionOM;
    
    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="Client", cascade={"persist", "merge"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;
    
    /**
     * @var TransfertType
     * @ORM\ManyToOne(targetEntity="TransfertType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transfert_type", referencedColumnName="id")
     * })
     */
    private $transfertType;
    
    /**
     * @var number
     * @ORM\Column(name="transfert_mode", type="smallint", nullable=true)
     */
    private $transfertMode;
    
    /**
     * @var string
     * @ORM\Column(name="client_identite", type="string", length=45, nullable=true)
     */
    private $clientIdentite;
    
    /**
     * @var string
     * @ORM\Column(name="beneficiaire_prenom", type="string", length=100, nullable=true)
     */
    private $beneficiairePrenom;
    
    /**
     * @var string
     * @ORM\Column(name="beneficiaire_nom", type="string", length=100, nullable=true)
     */
    private $beneficiaireNom;
    
    /**
     * @var string
     * @ORM\Column(name="beneficiaire_telephone", type="string", length=45, nullable=true)
     */
    private $beneficiaireTelephone;
    
    /**
     * @var string
     * @ORM\Column(name="beneficiaire_montant", type="float", precision=10, nullable=true)
     */
    private $beneficiaireMontant;
    
    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="beneficiaire_pays_id", referencedColumnName="id")
     * })
     */
    private $beneficiairePays;

    /**
     * @var Etat
     * @ORM\ManyToOne(targetEntity="Etat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="etat", referencedColumnName="id")
     * })
     */
    private $etat;

    /**
     * @var Puce
     * @ORM\ManyToOne(targetEntity="Puce")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="puce_id", referencedColumnName="id")
     * })
     */
    private $puce;

    /**
     * @var Utilisateur
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vendeur", referencedColumnName="id")
     * })
     */
    private $vendeur;
    
    /**
     * @var \Guedia\MainBundle\Entity\Configuration
     */
    private $configuration;


    public function __construct() {
    	$this->date = new \DateTime('NOW');
    	$this->reference = md5(date('YmdHis'));
    }
    
    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return array
     */
    public function getData() {
    	return array(
    			'id' => $this->id,
    			'reference' => $this->reference,
    			'commissionBoutique' => $this->commissionBoutique,
    			'commissionSociete' => $this->commissionSociete,
    			'devise' => $this->devise,
    			'clientPrenom' => $this->client->getPrenom(), 
    			'clientNom' => $this->client->getNom(),
    			'clientAddress' => $this->client->getAddress(),
    			'clientEmail' => $this->client->getEmail(), 
    			'clientIdentiteType' => $this->client->getIdentiteType() ? $this->client->getIdentiteType()->getId() : null,
    			'clientIdentite' => $this->client->getIdentite(), 
    			'clientTelephone' => $this->client->getTelephone(),
    			'beneficiairePrenom' => $this->beneficiairePrenom, 
    			'beneficiaireNom' => $this->beneficiaireNom,
    			'beneficiairePays' => $this->beneficiairePays ? $this->beneficiairePays->getName() : null, 
    			'beneficiaireTelephone' => $this->beneficiaireTelephone,
    			'transfertType' => $this->transfertType ? $this->transfertType->getId() : null, 
    			'transfertMode' => $this->transfertMode,
    			'montantSaisi' => $this->montant,
    			'montantEnvoye' => $this->montantPaye - $this->commissionBoutique - $this->commissionSociete,
    			'montantPaye' => $this->montantPaye,
    			'beneficiaireMontant' => $this->beneficiaireMontant, 
    			'montantChange' => $this->getMontantChange(),
    			'dateCreation' => $this->date->getTimestamp().'000',
    			'etat' => $this->etat ? array('id' => $this->etat->getId(), 'libelle' => $this->etat->getLibelle()) : null
    		);
    }
    
    /**
     * @return array
     */
    public function getDataForPhone() {
    	return array(
    			'id' => $this->id,
    			'reference' => $this->reference,
    			'clientPrenom' => $this->client->getPrenom(),
    			'clientNom' => $this->client->getNom(),
    			'clientEmail' => $this->client->getEmail(),
    			'clientTelephone' => $this->client->getTelephone(),
    			'beneficiairePrenom' => $this->beneficiairePrenom,
    			'beneficiaireNom' => $this->beneficiaireNom,
    			'ussdCode' => ($this->beneficiairePays && $this->transfertType && $this->transfertType->getId()==TransfertType::$ids['classic']) 
    				? $this->beneficiairePays->getClassicalCode()
    				: $this->beneficiairePays->getTacCode(),
    			'ussdPassword' => $this->puce ? $this->puce->getMotdepasse() : null,
    			'beneficiaireTelephone' => $this->beneficiaireTelephone,
    			'montantSaisi' => $this->montant,
    			'montantPaye' => $this->montantPaye,
    			'montantEnvoye' => $this->montantPaye - $this->commissionBoutique - $this->commissionSociete,
    			'dateCreation' => $this->date->getTimestamp(),
    			'beneficiaireMontant' => $this->beneficiaireMontant
    		);
    }

    /**
     * Set reference
     * @param string $reference
     * @return Depot
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Get reference
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }
    
    /**
     * get token
     * @return string
     */
    public function getToken() {
    	return $this->token;
    }
    
    /**
     * set token
     * @param string $token
     * @return Depot
     */
    public function setToken($token) {
    	$this->token = $token;
    	return $this;
    }
    
    /**
     * get resultat
     * @return string
     */
    public function getResultat() {
    	return $this->resultat;
    }
    
    /**
     * set resultat
     * @param string $resultat
     * @return Depot
     */
    public function setResultat($resultat) {
    	$this->resultat = $resultat;
    	return $this;
    }
	
    /**
     * Set date
     * @param \DateTime $date
     * @return Depot
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Set CADtoXOF
     * @param float $CADtoXOF
     * @return Depot
     */
    public function setCommission($CADtoXOF)
    {
    	$montant = $this->getMontant();
    	$this->devise = $CADtoXOF;
    	if($this->transfertMode==self::$transfertModes['toSend']) {
    		$this->montantPaye = $montant;
	    	$commission = $montant * $this->configuration->getTauxTransfert() / 100;
	    	$this->commissionSociete = $commission * $this->configuration->getPourcentageSociete() / 100;
	    	$this->commissionBoutique = $commission * $this->configuration->getPourcentageBoutique() / 100;
	    	$this->beneficiaireMontant = $montant * $CADtoXOF * (100 - $this->configuration->getTauxTransfert()) * (100 - $this->configuration->getTauxChange()) / 10000;
    	} elseif($this->transfertMode==self::$transfertModes['toReceive']) {
    		$this->beneficiaireMontant = $montant;
    		$toSend = $montant * 10000 / $CADtoXOF / (100 - $this->configuration->getTauxTransfert()) / (100 - $this->configuration->getTauxChange());
    		$commission = $toSend * $this->configuration->getTauxTransfert() / 100;
    		$this->commissionBoutique = $commission * $this->configuration->getPourcentageBoutique() / 100;
    		$this->commissionSociete = $commission * $this->configuration->getPourcentageSociete() / 100;
    		$this->montantPaye = $toSend;
    	}
    	return $this;
    }
    
    /**
     * Set montant
     * @param float $montant
     * @return Depot
     */
    public function setMontant($montant)
    {
    	$this->montant = $montant;
    	return $this;
    }
    
    /**
     * Get montant
     * @return float
     */
    public function getMontant()
    {
    	return $this->montant;
    }
    
    /**
     * Get montant change
     * @return float
     */
    public function getMontantChange()
    {
    	return $this->montantPaye - ($this->beneficiaireMontant / $this->devise - $this->commissionOM) - $this->commissionBoutique - $this->commissionSociete;
    }
    
    /**
     * Set montant payé
     * @param float $montantPaye
     * @return Depot
     */
    public function setMontantPaye($montantPaye)
    {
    	$this->montantPaye = $montantPaye;
    	return $this;
    }
    
    /**
     * Get montant envoyé
     * @return float
     */
    public function getMontantPaye()
    {
    	return $this->montantPaye;
    }
    
    /**
     * Get montant envoyé
     * @return float
     */
    public function getMontantEnvoye()
    {
    	return $this->montantPaye - $this->commissionBoutique - $this->commissionSociete;
    }
    
    /**
     * Get amount to send
     * @return float
     */
    public function getAmountToSend($CADtoXOF)
    {
    	return $this->montant * (100 - $this->configuration->getTauxTransfert()) * (100 - $this->configuration->getTauxChange()) * $CADtoXOF / 10000;
    }
	
	/**
	 * get devise
	 * @return float
	 */
	public function getDevise() {
		return $this->devise;
	}
	
	/**
	 * set devise
	 * @param float $devise
	 * @return Depot
	 */
	public function setDevise($devise) {
		$this->devise = $devise;
		return $this;
	}
	
	/**
	 * get boutique commission
	 * @return float
	 */
	public function getCommissions() {
		return $this->commissionBoutique + $this->commissionSociete + ($this->devise * $this->transfertType->getConfiguration()->getTauxChange() / 100);
	}
	
	/**
	 * get boutique commission
	 * @return float
	 */
	public function getCommissionBoutique() {
		return $this->commissionBoutique;
	}
	
	/**
	 * set boutique commission
	 * @param float $commissionBoutique
	 * @return Depot
	 */
	public function setCommissionBoutique($commissionBoutique) {
		$this->commissionBoutique = $commissionBoutique;
		return $this;
	}
	
	/**
	 * get societe commission
	 * @return float
	 */
	public function getCommissionSociete() {
		return $this->commissionSociete;
	}
	
	/**
	 * set societe cmmission
	 * @param float $commissionSociete
	 * @return Depot
	 */
	public function setCommissionSociete($commissionSociete) {
		$this->commissionSociete = $commissionSociete;
		return $this;
	}
	
	/**
	 * get om commission
	 * @return float
	 */
	public function getCommissionOM() {
		return $this->commissionOM;
	}
	
	/**
	 * set om commission
	 * @param float $commissionOM
	 * @return Depot
	 */
	public function setCommissionOM($commissionOM) {
		$this->commissionOM = $commissionOM;
		return $this;
	}
    
    /**
     * Set client
     * @param client $client
     * @return Depot
     */
    public function setClient($client)
    {
    	$this->client = $client;
    	return $this;
    }
    
    /**
     * Get client
     * @return Client
     */
    public function getClient()
    {
    	return $this->client;
    }
    
    /**
     * Set transfertType
     * @param TransfertType $transfertType
     * @return Depot
     */
    public function setTransfertType($transfertType)
    {
    	$this->transfertType = $transfertType;
    	return $this;
    }
    
    /**
     * Get transfertType
     * @return TransfertType
     */
    public function getTransfertType()
    {
    	return $this->transfertType;
    }
    
    /**
     * Set transfertMode
     * @param number $transfertMode
     * @return Depot
     */
    public function setTransfertMode($transfertMode)
    {
    	$this->transfertMode= $transfertMode;
    	return $this;
    }
    
    /**
     * Get transfertMode
     * @return number
     */
    public function getTransfertMode()
    {
    	return $this->transfertMode;
    }
    
    /**
     * Set beneficiairePrenom
     * @param string $beneficiairePrenom
     * @return Depot
     */
    public function setBeneficiairePrenom($beneficiairePrenom)
    {
    	$this->beneficiairePrenom = $beneficiairePrenom;
    	return $this;
    }
    
    /**
     * Get beneficiairePrenom
     * @return string
     */
    public function getBeneficiairePrenom()
    {
    	return $this->beneficiairePrenom;
    }

    /**
     * Set beneficiaireNom
     * @param string $beneficiaireNom
     * @return Depot
     */
    public function setBeneficiaireNom($beneficiaireNom)
    {
        $this->beneficiaireNom = $beneficiaireNom;
        return $this;
    }

    /**
     * Get beneficiaireNom
     * @return string 
     */
    public function getBeneficiaireNom()
    {
        return $this->beneficiaireNom;
    }

    /**
     * Set beneficiairePays
     * @param Country $beneficiairePays
     * @return Depot
     */
    public function setBeneficiairePays($beneficiairePays)
    {
        $this->beneficiairePays = $beneficiairePays;
        return $this;
    }

    /**
     * Get beneficiairePays
     * @return Country 
     */
    public function getBeneficiairePays()
    {
        return $this->beneficiairePays;
    }

    /**
     * Set beneficiaireTelephone
     * @param string $beneficiaireTelephone
     * @return Depot
     */
    public function setBeneficiaireTelephone($beneficiaireTelephone)
    {
        $this->beneficiaireTelephone = $beneficiaireTelephone;
        return $this;
    }

    /**
     * Get beneficiaireTelephone
     * @return string 
     */
    public function getBeneficiaireTelephone()
    {
        return $this->beneficiaireTelephone;
    }
	
	/**
	 * get montant beneficiaire
	 * @return float
	 */
	public function getBeneficiaireMontant() {
		return $this->beneficiaireMontant;
	}
	
	/**
	 *set montant beneficiaire
	 * @param float $beneficiaireMontant
	 * @return Depot
	 */
	public function setBeneficiaireMontant($beneficiaireMontant) {
		$this->beneficiaireMontant = $beneficiaireMontant;
		return $this;
	}
	
    /**
     * Set etat
     *
     * @param \Guedia\MainBundle\Entity\Etat $etat
     * @return Depot
     */
    public function setEtat(\Guedia\MainBundle\Entity\Etat $etat = null)
    {
        $this->etat = $etat;
        return $this;
    }

    /**
     * Get etat
     *
     * @return \Guedia\MainBundle\Entity\Etat 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set puce
     *
     * @param \Guedia\MainBundle\Entity\Puce $puce
     * @return Depot
     */
    public function setPuce(\Guedia\MainBundle\Entity\Puce $puce = null)
    {
        $this->puce = $puce;

        return $this;
    }

    /**
     * Get puce
     *
     * @return \Guedia\MainBundle\Entity\Puce 
     */
    public function getPuce()
    {
        return $this->puce;
    }

    /**
     * Set vendeur
     *
     * @param \Guedia\MainBundle\Entity\Utilisateur $vendeur
     * @return Depot
     */
    public function setVendeur(\Guedia\MainBundle\Entity\Utilisateur $vendeur = null)
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    /**
     * Get vendeur
     *
     * @return \Guedia\MainBundle\Entity\Utilisateur 
     */
    public function getVendeur()
    {
        return $this->vendeur;
    }
    
    /**
     * @return \Guedia\MainBundle\Entity\Configuration
     */
	public function getConfiguration() {
		return $this->configuration;
	}
	
	/**
	 * @param Configuration $configuration
	 * @return \Guedia\MainBundle\Entity\Depot
	 */
	public function setConfiguration($configuration) {
		$this->configuration = $configuration;
		return $this;
	}
	
	/**
	 * @return float
	 */
	public function getEffectiveChange() {
		return $this->devise * (100 - $this->transfertType->getConfiguration()->getTauxChange()) / 100;
	}
	
	/**
	 * @return float
	 */
	public function getCommissionChange() {
		return $this->getMontantEnvoye() * $this->transfertType->getConfiguration()->getTauxChange() / 100;
	}
	
    
}
