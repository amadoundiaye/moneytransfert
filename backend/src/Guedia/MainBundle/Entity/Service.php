<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 * @ORM\Table(name="service")
 * @ORM\Entity
 */
class Service
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var string
	 * @ORM\Column(name="libelle", type="string", length=100, nullable=true)
	 */
	private $libelle;
	
	/**
	 * @var number
	 * @ORM\Column(name="ordre", type="smallint", length=2, nullable=true)
	 */
	private $ordre;


}
