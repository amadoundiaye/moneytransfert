<?php

namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Machine
 *
 * @ORM\Table(name="configuration")
 * @ORM\Entity
 */
class Configuration
{
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 * @var number
	 * @ORM\Column(name="taux_change", type="float", nullable=true)
	 */
	private $tauxChange;
	
	/**
	 * @var number
	 * @ORM\Column(name="taux_transfert", type="float", nullable=true)
	 */
	private $tauxTransfert;
	
	/**
	 * @var number
	 * @ORM\Column(name="pourcentage_boutique", type="float", nullable=true)
	 */
	private $pourcentageBoutique;
	
	/**
	 * @var number
	 * @ORM\Column(name="pourcentage_societe", type="float", nullable=true)
	 */
	private $pourcentageSociete;
	
	/**
	 * @var TransfertType
	 * @ORM\ManyToOne(targetEntity="TransfertType")
	 * @ORM\JoinColumns({
	 * 	@ORM\JoinColumn(name="transfert_type_id", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $transfertType;
	
	
	/**
	 * Get id
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	public function toArray() {
		return array(
				'pourcentageBoutique' => $this->pourcentageBoutique,
				'pourcentageSociete' => $this->pourcentageSociete,
				'transfertType' => $this->transfertType ? $this->transfertType->getId() : null,
				'tauxChange' => $this->tauxChange,
				'tauxTransfert' => $this->tauxTransfert
			);
	}
	
	/**
	 * @return number
	 */
	public function getTauxChange() {
		return $this->tauxChange;
	}
	
	/**
	 *
	 * @param number $tauxChange
	 * @return Configuration
	 */
	public function setTauxChange($tauxChange) {
		$this->tauxChange = $tauxChange;
		return $this;
	}
	
	/**
	 *
	 * @return number
	 */
	public function getTauxTransfert() {
		return $this->tauxTransfert;
	}
	
	/**
	 *
	 * @param number$tauxTransfert
	 * @return Configuration
	 */
	public function setTauxTransfert($tauxTransfert) {
		$this->tauxTransfert = $tauxTransfert;
		return $this;
	}
	
	/**
	 *
	 * @return number
	 */
	public function getPourcentageBoutique() {
		return $this->pourcentageBoutique;
	}
	
	/**
	 *
	 * @param number $pourcentageBoutique
	 * @return Configuration
	 */
	public function setPourcentageBoutique($pourcentageBoutique) {
		$this->pourcentageBoutique = $pourcentageBoutique;
		return $this;
	}
	
	/**
	 *
	 * @return number
	 */
	public function getPourcentageSociete() {
		return $this->pourcentageSociete;
	}
	
	/**
	 *
	 * @param number $pourcentageSociete
	 * @return Configuration
	 */
	public function setPourcentageSociete($pourcentageSociete) {
		$this->pourcentageSociete = $pourcentageSociete;
		return $this;
	}
	
	/**
	 * get type transfert
	 * @return TransfertType
	 */
	public function getTransfertType() {
		return $this->transfertType;
	}
	
	/**
	 * set type transfert
	 * @param TransfertType $transfertType  
	 * @return Configuration      	
	 */
	public function setTransfertType(TransfertType $transfertType) {
		$this->transfertType = $transfertType;
		return $this;
	}


}
