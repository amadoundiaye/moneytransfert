<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DetailsGrille
 * @ORM\Table(name="details_grille")
 * @ORM\Entity(repositoryClass="\Guedia\MainBundle\Repository\DetailsGrilleRepository")
 */
class DetailsGrille
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var float
	 * @ORM\Column(name="montant_min", type="decimal", length=10)
	 */
	private $montantMin;
	
	/**
	 * @var float
	 * @ORM\Column(name="montant_max", type="decimal", length=10)
	 */
	private $montantMax;
	
	/**
	 * @var float
	 * @ORM\Column(name="tarif", type="decimal", length=10)
	 */
	private $tarif;
	
	/**
	 * @var float
	 * @ORM\Column(name="tarif_promo", type="decimal", length=10)
	 */
	private $tarifPromo;
	
	/**
	 * @var Grille
	 * @ORM\ManyToOne(targetEntity="Grille")
	 * @ORM\JoinColumns({
	 * 	@ORM\JoinColumn(name="grille_id", referencedColumnName="id")
	 * })
	 */
	private $grille;
	
	/**
	 * @return float
	 */
	public function getMontantMax() {
		return $this->montantMax;
	}

	
}
