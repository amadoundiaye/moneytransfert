<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Puce
 * @ORM\Table(name="transfert_type")
 * @ORM\Entity
 */
class TransfertType
{
	static $ids;
	
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var number
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;
    
    /**
	 * @var Configuration
	 * @ORM\OneToOne(targetEntity="Configuration", mappedBy="transfertType")
     */
    private $configuration;


    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return TransfertType
     */
    public function setName($name)
    {
    	$this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string 
     */
    public function getName()
    {
    	return $this->name;
    }
    
    /**
     * get configuration
     * @return string
     */
    public function getConfiguration()
    {
    	return $this->configuration;
    }

}
