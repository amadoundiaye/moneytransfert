<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Promo
 * @ORM\Table(name="promo")
 * @ORM\Entity
 */
class Promo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var string
	 * @ORM\Column(name="libelle", type="string", length=100, nullable=true)
	 */
	private $libelle;
	
	/**
	 * @var number
	 * @ORM\Column(name="debut", type="date", nullable=true)
	 */
	private $debut;
	
	/**
	 * @var number
	 * @ORM\Column(name="fin", type="date", nullable=true)
	 */
	private $fin;


}
