<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Utilisateur
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="Guedia\MainBundle\Repository\UtilisateurRepository")
 */
class Utilisateur extends User
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	protected  $id;
	
	/**
	 * @var string
	 * @ORM\Column(name="firstname", type="string", length=100, nullable=false)
	 */
	protected $firstname;
	
	/**
	 * @var string
	 * @ORM\Column(name="lastname", type="string", length=100, nullable=false)
	 */
	protected $lastname;
    
	/**
	 * @var string
	 * @ORM\Column(name="phone", type="string", length=25, nullable=true)
	 */
	protected $phone;
	
	/**
	 * @var string
	 * @ORM\Column(name="address", type="string", length=100, nullable=true)
	 */
	protected $address;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Puce", inversedBy="utilisateur")
     * @ORM\JoinTable(name="utilisateur_has_puce",
     *   joinColumns={
     *     @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="puce_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $puce;
    
    /**
     * @var string
     */
    public $pays;

    /**
     * Constructor
     */
    public function __construct()
    {
    	parent::__construct();
        $this->puce = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set phone
     * @param string $phone
     * @return Utilisateur
     */
    public function setPhone($phone)
    {
    	$this->phone= $phone;
    	return $this;
    }
    
    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
    	return $this->phone;
    }
    
    /**
     * Set address
     * @param string $address
     * @return Utilisateur
     */
    public function setAddress($address)
    {
    	$this->address = $address;
    	return $this;
    }
    
    /**
     * Get adress
     * @return string
     */
    public function getAddress()
    {
    	return $this->address;
    }
    
    /**
     * Add puce
     * @param \Guedia\MainBundle\Entity\Puce $puce
     * @return Utilisateur
     */
    public function addPuce(\Guedia\MainBundle\Entity\Puce $puce)
    {
        $this->puce[] = $puce;
        return $this;
    }

    /**
     * Remove puce
     *
     * @param \Guedia\MainBundle\Entity\Puce $puce
     */
    public function removePuce(\Guedia\MainBundle\Entity\Puce $puce)
    {
        $this->puce->removeElement($puce);
    }

    /**
     * Get puce
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPuce()
    {
        return $this->puce;
    }

    /**
     * Set firstname
     * @param string $firstname
     * @return Utilisateur
     */
    public function setFirstName($firstname)
    {
    	$this->firstname = $firstname;
        return $this;
    }
    
    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstName()
    {
    	return $this->firstname;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    
    /**
     * Set lastname
     *
     * @param string $nom
     * @return Utilisateur
     */
    public function setLastname($lastname)
    {
    	$this->lastname= $lastname;
    	
    	return $this;
    }

    /**
     * get profil
     */
    public function getProfil() {
    	return $this->hasRole('ROLE_ADMIN') ? 'Administrator' : 'Cashier';
    }
    
    /**
     * get profil
     */
    public function getUserRole() {
    	return $this->hasRole('ROLE_ADMIN') ? 'ROLE_ADMIN' : 'ROLE_USER';
    }
    
    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context) {
    	$number = array();
    	foreach($this->puce as $puce) {
    		if(!isset($number[$puce->getPays()->getId()])) {
    			$number[$puce->getPays()->getId()] = 0;
    		}
    		$number[$puce->getPays()->getId()]++;
    	}
    	if(count($number) > 0 && max($number) > 1) {
    		$context->buildViolation('You can only choose one sim by country')->atPath('puce')->addViolation();
    	}
    }
    
    /**
     * @return string
     */
    public function __toString() {
    	return $this->firstname.' '.$this->lastname;
    }
}
