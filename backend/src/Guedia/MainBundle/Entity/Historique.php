<?php

namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historique
 *
 * @ORM\Table(name="historique", indexes={@ORM\Index(name="fk_historique_etat1_idx", columns={"etat_id"}), @ORM\Index(name="fk_historique_depot1_idx", columns={"depot_id"})})
 * @ORM\Entity(repositoryClass="\Guedia\MainBundle\Repository\HistoriqueRepository")
 */
class Historique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;
    
    /**
     * @var \Depot
     * @ORM\ManyToOne(targetEntity="Depot")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="depot_id", referencedColumnName="id")
     * })
     */
    private $depot;
    
    /**
     * @var Utilisateur
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $utilisateur;

    /**
     * @var \Etat
     *
     * @ORM\ManyToOne(targetEntity="Etat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="etat_id", referencedColumnName="id")
     * })
     */
    private $etat;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Historique
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set depot
     *
     * @param \Guedia\MainBundle\Entity\Depot $depot
     * @return Historique
     */
    public function setDepot(\Guedia\MainBundle\Entity\Depot $depot = null)
    {
        $this->depot = $depot;

        return $this;
    }

    /**
     * Get depot
     *
     * @return \Guedia\MainBundle\Entity\Depot 
     */
    public function getDepot()
    {
        return $this->depot;
    }
    
    /**
     * get utilisateur
     * @return Utilisateur
     */
    public function getUtilisateur()
    {
    	return $this->utilisateur;
    }
    
    /**
     * Set etat
     * @param \Guedia\MainBundle\Entity\Etat $etat
     * @return Historique
     */
    public function setEtat(\Guedia\MainBundle\Entity\Etat $etat = null)
    {
    	$this->etat = $etat;
    	return $this;
    }

    /**
     * Get etat
     *
     * @return \Guedia\MainBundle\Entity\Etat 
     */
    public function getEtat()
    {
        return $this->etat;
    }
    
    /**
     * create historique depot
     * @param Depot $depot
     * @return Historique
     */
    static function createFromDepot($depot, $utilisateur) {
    	$historique = new self;
    	$historique->setDepot($depot);
    	$historique->utilisateur = $utilisateur;
    	$historique->setEtat($depot->getEtat());
    	$historique->setDate(new \DateTime('NOW'));
    	return $historique;
    }
}
