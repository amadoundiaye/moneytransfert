<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var string
	 * @ORM\Column(name="iso", type="string", length=2, nullable=true)
	 */
	private $iso;
	
	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=80, nullable=true)
	 */
	private $name;
	
	/**
	 * @var string
	 * @ORM\Column(name="nicename", type="string", length=80, nullable=true)
	 */
	private $nicename;
	
	/**
	 * @var string
	 * @ORM\Column(name="tac_code", type="string", length=80, nullable=true)
	 */
	private $tacCode;
	
	/**
	 * @var string
	 * @ORM\Column(name="classical_code", type="string", length=80, nullable=true)
	 */
	private $classicalCode;
	
	/**
	 * @var string
	 * @ORM\Column(name="iso3", type="string", length=3, nullable=true)
	 */
	private $iso3;
	
	/**
	 * @var string
	 * @ORM\Column(name="numcode", type="smallint", length=6, nullable=true)
	 */
	private $numcode;
	
	/**
	 * @var string
	 * @ORM\Column(name="phonecode", type="string", length=5, nullable=true)
	 */
	private $phonecode;



    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set iso
     * @param string $iso
     * @return Country
     */
    public function setIso($iso)
    {
    	$this->iso = $iso;
    	return $this;
    }
    
    /**
     * Get iso
     * @return string
     */
    public function getIso()
    {
    	return $this->iso;
    }
    
    /**
     * Set tacCode
     * @param string $tacCode
     * @return Country
     */
    public function setTacCode($tacCode)
    {
    	$this->tacCode = $tacCode;
    	return $this;
    }
    
    /**
     * Get tacCode
     * @return string
     */
    public function getTacCode()
    {
    	return $this->tacCode;
    }
    
    /**
     * Set classicalCode
     * @param string $classicalCode
     * @return Country
     */
    public function setClassicalCode($classicalCode)
    {
    	$this->classicalCode = $classicalCode;
    	return $this;
    }
    
    /**
     * Get classicalCode
     * @return string
     */
    public function getClassicalCode()
    {
    	return $this->classicalCode;
    }
    
    /**
     * Set iso3
     * @param string $iso3
     * @return Country
     */
    public function setIso3($iso3)
    {
    	$this->iso3 = $iso3;
    	return $this;
    }
    
    /**
     * Get iso3
     * @return string
     */
    public function getIso3()
    {
    	return $this->iso3;
    }
    
    /**
     * Set name
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
    	$this->name= $name;
    	return $this;
    }
    
    /**
     * Get name
     * @return string
     */
    public function getName()
    {
    	return $this->name;
    }
    
    /**
     * Set phonecode
     * @param string $phonecode
     * @return Country
     */
    public function setPhonecode($phonecode)
    {
    	$this->phonecode= $phonecode;
    	return $this;
    }
    
    /**
     * Get phonecode
     * @return string
     */
    public function getPhonecode()
    {
    	return $this->phonecode;
    }
    
    /**
     * get nicename
     * @return string
     */
    public function __toString() {
    	return $this->nicename;
    }
}
