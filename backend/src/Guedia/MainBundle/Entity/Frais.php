<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Frais
 * @ORM\Table(name="frais")
 * @ORM\Entity
 */
class Frais
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var number
	 * @ORM\Column(name="montant_min", type="decimal", precision=1, nullable=true)
	 */
	private $montantMin;
	
	/**
	 * @var number
	 * @ORM\Column(name="montant_max", type="decimal", precision=1, nullable=true)
	 */
	private $montantMax;
	
	/**
	 * @var number
	 * @ORM\Column(name="tarif", type="decimal", precision=1, nullable=true)
	 */
	private $tarif;


    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
	
	/**
	 * get montant min
	 * @return number
	 */
	public function getMontantMin() {
		return $this->montantMin;
	}
	
	/**
	 * set min montant
	 * @param number $montantMin
	 */
	public function setMontantMin($montantMin) {
		$this->montantMin = $montantMin;
		return $this;
	}
	
	/**
	 * get max montant
	 * @return number
	 */
	public function getMontantMax() {
		return $this->montantMax;
	}
	
	/**
	 * set max montant
	 * @param number $montantMax
	 */
	public function setMontantMax($montantMax) {
		$this->montantMax = $montantMax;
		return $this;
	}
	
	/**
	 * get tarif
	 * @return number
	 */
	public function getTarif() {
		return $this->tarif;
	}
	
	/**
	 * get tarif
	 * @param number $amount
	 * @return number
	 */
	public function getTrueTarif($amount) {
		return $this->tarif < 1 ? $this->tarif * $amount : $this->tarif;
	}
	
	/**
	 * set tarif
	 * @param number $tarif
	 */
	public function setTarif($tarif) {
		$this->tarif = $tarif;
		return $this;
	}

	
}
