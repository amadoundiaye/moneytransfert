<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grille
 * @ORM\Table(name="grille")
 * @ORM\Entity
 */
class Grille
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
	/**
	 * @var string
	 * @ORM\Column(name="libelle", type="string", length=50)
	 */
	private $libelle;
	
	/**
	 * @var Promo
	 * @ORM\ManyToOne(targetEntity="Promo")
	 * @ORM\JoinColumns({
	 * 	@ORM\JoinColumn(name="promo_id", referencedColumnName="id", nullable=false)
	 * })
	 */
	private $promo;
	
	/**
	 * @var \Doctrine\Common\Collections\ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Service")
	 * @ORM\JoinTable(name="service_has_grille", 
	 * 	joinColumns={
	 * 		@ORM\JoinColumn(name="grille_id", referencedColumnName="id")
	 * 	}, 
	 * 	inverseJoinColumns={
	 * 		@ORM\JoinColumn(name="service_id", referencedColumnName="id")
	 * })
	 */
	private $service;

}
