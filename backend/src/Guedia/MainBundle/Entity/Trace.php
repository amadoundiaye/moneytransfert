<?php

namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trace
 *
 * @ORM\Table(name="trace", indexes={@ORM\Index(name="fk_trace_puce1_idx", columns={"puce_id"})})
 * @ORM\Entity
 */
class Trace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="signature_attendue", type="string", length=255, nullable=true)
     */
    private $signatureAttendue;

    /**
     * @var string
     * @ORM\Column(name="signature_recue", type="string", length=255, nullable=true)
     */
    private $signatureRecue;

    /**
     * @var \Puce
     *
     * @ORM\ManyToOne(targetEntity="Puce")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="puce_id", referencedColumnName="id")
     * })
     */
    private $puce;
    
    /**
     * @var string
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;


    static function logRequest($sim, $expected, $given, $message = null) {
    	$entity = new self;
    	$entity->puce = $sim;
    	$entity->signatureAttendue = $expected;
    	$entity->signatureRecue = $given;
    	$entity->date = new \DateTime('NOW');
    	$entity->message = $message;
    	return $entity;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Trace
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set signatureAttendue
     *
     * @param string $signatureAttendue
     * @return Trace
     */
    public function setSignatureAttendue($signatureAttendue)
    {
        $this->signatureAttendue = $signatureAttendue;

        return $this;
    }

    /**
     * Get signatureAttendue
     *
     * @return string 
     */
    public function getSignatureAttendue()
    {
        return $this->signatureAttendue;
    }

    /**
     * Set signatureRecue
     *
     * @param string $signatureRecue
     * @return Trace
     */
    public function setSignatureRecue($signatureRecue)
    {
        $this->signatureRecue = $signatureRecue;
        return $this;
    }
    
    /**
     * Get signatureRecue
     * @return string
     */
    public function getSignatureRecue()
    {
    	return $this->signatureRecue;
    }
    
    /**
     * Get message
     * @return string
     */
    public function getMessage()
    {
    	return $this->message;
    }

    /**
     * Set puce
     *
     * @param \Guedia\MainBundle\Entity\Puce $puce
     * @return Trace
     */
    public function setPuce(\Guedia\MainBundle\Entity\Puce $puce = null)
    {
        $this->puce = $puce;

        return $this;
    }

    /**
     * Get puce
     * @return \Guedia\MainBundle\Entity\Puce 
     */
    public function getPuce()
    {
        return $this->puce;
    }
}
