<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="\Guedia\MainBundle\Repository\ClientRepository")
 */
class Client
{
	
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
	private $id;
	
    /**
     * @var string
     * @ORM\Column(name="prenom", type="string", length=100, nullable=true)
     */
    private $prenom;
    
    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=100, nullable=true)
     */
    private $nom;
    
    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;
    
    /**
     * @var string
     * @ORM\Column(name="telephone", type="string", length=45, nullable=true)
     */
    private $telephone;
    
    /**
     * @var string
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;
    
    /**
     * @var string
     * @ORM\Column(name="identite", type="string", length=100)
     */
    private $identite;
    
    /**
     * @var IdentiteType
     * @ORM\ManyToOne(targetEntity="IdentiteType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="identite_type", referencedColumnName="id")
     * })
     */
    private $identiteType;
    
    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * get prenom
     * @param string $prenom
     * @return Depot
     */
    public function setPrenom($prenom)
    {
    	$this->prenom = $prenom;
    	return $this;
    }
    
    /**
     * Get prenom
     * @return string
     */
    public function getPrenom()
    {
    	return $this->prenom;
    }

    /**
     * Set nom
     * @param string $nom
     * @return Depot
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * Get nom
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Set email
     * @param string $email
     * @return Depot
     */
    public function setEmail($email)
    {
    	$this->email= $email;
    	return $this;
    }
    
    /**
     * Get email
     * @return string
     */
    public function getEmail()
    {
    	return $this->email;
    }
    
    /**
     * Set address
     * @param string $address
     * @return Depot
     */
    public function setAddress($address)
    {
    	$this->address = $address;
    	return $this;
    }
    
    /**
     * Get address
     * @return string
     */
    public function getAddress()
    {
    	return $this->address;
    }

    /**
     * Set telephone
     * @param string $telephone
     * @return Depot
     */
    public function setTelephone($telephone)
    {
    	$this->telephone= $telephone;
        return $this;
    }

    /**
     * Get telephone
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }
    
    /**
     * Set identiteType
     * @param IdentityType $identiteType
     * @return Depot
     */
    public function setIdentiteType($identiteType)
    {
    	$this->identiteType= $identiteType;
    	return $this;
    }
    
    /**
     * Get identiteType
     * @return IdentityType
     */
    public function getIdentiteType()
    {
    	return $this->identiteType;
    }
    
    /**
     * Set identite
     * @param string $identite
     * @return Depot
     */
    public function setIdentite($identite)
    {
    	$this->identite = $identite;
    	return $this;
    }
    
    /**
     * Get identite
     * @return string
     */
    public function getIdentite()
    {
    	return $this->identite;
    }
    
    /**
     * get fullname
     * @return string
     */
    public function __toString() {
    	return $this->prenom.' '.$this->nom;
    }
    
}
