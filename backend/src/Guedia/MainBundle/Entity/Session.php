<?php
namespace Guedia\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 * @ORM\Table(name="session")
 * @ORM\Entity
 */
class Session
{
	static $dashboard = 'DASHBOARD';
	static $depot = 'DEPOT';
	static $trace = 'TRACE';
	
	/**
	 * @var number
     * @ORM\Id
	 * @ORM\Column(name="action", type="string", length=25, nullable=true)
	 */
	private $action;
	
	/**
	 * @var number
	 * @ORM\Column(name="montant_max", type="text", nullable=false)
	 */
	private $data;
	
	/**
	 * @var Utilisateur
     * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="Utilisateur")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
	private $utilisateur;

	/**
	 * @param string $action
	 * @param Utilisateur $utilisateur
	 * @param array $data
	 * @return \Guedia\MainBundle\Entity\Session
	 */
	static function create($action, $utilisateur, $data) {
		$entity = new self;
		$entity->setAction($action);
		$entity->setUtilisateur($utilisateur);
		$entity->setData(serialize($data));
		return $entity;
	}
	
	/**
	 * get action
	 * @return number
	 */
	public function getAction() {
		return $this->action;
	}
	
	/**
	 * set action
	 * @param string $action
	 * @return Session
	 */
	public function setAction($action) {
		$this->action = $action;
		return $this;
	}
	
	/**
	 * get data
	 * @return number
	 */
	public function getData() {
		return $this->data;
	}
	
	/**
	 * get data
	 * @return array
	 */
	public function getDataToArray() {
		return unserialize($this->data);
	}
	
	/**
	 * set data
	 * @param string $data
	 * @return Session
	 */
	public function setData($data) {
		$this->data = $data;
		return $this;
	}
	
	/**
	 * get utilisateur
	 * @return Utilisateur
	 */
	public function getUtilisateur() {
		return $this->utilisateur;
	}
	
	/**
	 * set utilisateur
	 * @param Utilisateur $utilisateur  
	 * @return Session      	
	 */
	public function setUtilisateur(Utilisateur $utilisateur) {
		$this->utilisateur = $utilisateur;
		return $this;
	}
	
	
}
