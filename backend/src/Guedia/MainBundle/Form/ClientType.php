<?php
namespace Guedia\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class ClientType extends AbstractType {
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('prenom')->add('nom')
			->add('telephone')->add('address')
			->add('email')
			->add('identite')->add('identiteType');
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		parent::configureOptions($resolver);
		$resolver->setDefaults(array(
				'data_class' => 'Guedia\MainBundle\Entity\Client'
			));
	}
	
	public function getName()
	{
		return 'client';
	}
}