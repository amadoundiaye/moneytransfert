<?php
namespace Guedia\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\UserBundle\Form\Type\RegistrationFormType;

class RegistrationType extends RegistrationFormType {
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);
		$builder->add('firstname')
			->add('lastname')
			->add('pays')
			->add('address')
			->add('phone')
			->add('puce');
		$builder->add('roles', 'choice', array('choices' => array('ROLE_ADMIN' => 'ROLE_ADMIN'), 'expanded' => true, 'multiple' => true));
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		parent::configureOptions($resolver);
	}
	
	public function getName()
	{
		return 'guedia_registration';
	}
}