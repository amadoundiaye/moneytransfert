<?php
namespace Guedia\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class DepotType extends AbstractType {
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('reference')
			->add('commissionBoutique')->add('commissionSociete')->add('commissionOM')
			->add('client', new ClientType())
			->add('devise')
			->add('transfertType')->add('transfertMode')
			->add('montant')->add('montantPaye')->add('beneficiaireMontant')
			->add('beneficiairePrenom')->add('beneficiaireNom')
			->add('beneficiaireTelephone')->add('beneficiairePays');
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		parent::configureOptions($resolver);
		$resolver->setDefaults(array(
				'data_class' => 'Guedia\MainBundle\Entity\Depot'
			));
	}
	
	public function getName()
	{
		return 'depot';
	}
}