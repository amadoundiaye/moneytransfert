<?php
namespace Guedia\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

class SimType extends AbstractType {
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('telephone')
			->add('motdepasse')
			->add('pays');
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		parent::configureOptions($resolver);
	}
	
	public function getName()
	{
		return 'sim';
	}
}