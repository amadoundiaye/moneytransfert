<?php
namespace Guedia\MainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GuediaMainBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
	/* (non-PHPdoc)
	 * @see \Symfony\Component\HttpKernel\Bundle\Bundle::boot()
	 */
	public function boot() {
		// TODO: Auto-generated method stub
		$ids	= $this->container->getParameter('ids');
		$types = $this->container->getParameter('types');
		\Guedia\MainBundle\Entity\Etat::$ids = $ids['etat'];
		\Guedia\MainBundle\Entity\TransfertType::$ids = $ids['transfertType'];
		\Guedia\MainBundle\Entity\Depot::$transfertModes = $types['transfertMode'];
	}
	
}
