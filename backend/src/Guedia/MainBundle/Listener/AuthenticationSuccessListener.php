<?php
namespace Guedia\MainBundle\Listener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Guedia\MainBundle\Entity\DetailsGrille;

class AuthenticationSuccessListener
{
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $em;
	
	/**
	 * @var array
	 */
	private $ids;
	
	/**
	 * @param \Doctrine\ORM\EntityManager $em
	 * @param array $ids
	 */
	public function __construct($em, $ids) {
		$this->em = $em;
		$this->ids = $ids;
	}
	
	/**
	 * @param AuthenticationSuccessEvent $event
	 */
	public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
	{
		$data = $event->getData();
		$user = $event->getUser();
		if (!$user instanceof UserInterface) 
			return;
		$data['success'] = true; 
		$data['data'] = array(
				'username' 		=> $user->getUsername(),
				'email'  		=> $user->getEmail(),
				'role'    		=> $user->getUserRole(),
				'profil'    	=> $user->getProfil(),
				'address'  		=> $user->getAddress(),
				'firstname'  	=> $user->getFirstname(),
				'lastname'  	=> $user->getLastname(),
				'salt'  		=> $user->getSalt(),
				'phone'  		=> $user->getPhone(),
				'locked'  		=> false
		);
		$data['parameters']	= array('ids' => $this->ids, 'values' => array(
				'maxClassic' => $this->em->getRepository('GuediaMainBundle:DetailsGrille')->getMaxValueByGrille($this->ids['grille']['classic']),
				'maxTac' => $this->em->getRepository('GuediaMainBundle:DetailsGrille')->getMaxValueByGrille($this->ids['grille']['tac'])
			));
		$event->setData($data);
	}
	
}