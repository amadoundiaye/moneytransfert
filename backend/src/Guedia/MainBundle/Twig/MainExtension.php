<?php 
namespace Guedia\MainBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAware;

class MainExtension extends \Twig_Extension {

	/**
	 * @var \Twig_Environment
	 */
	private $twig;
	
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	private $em;
	
	/**
	 * @var array
	 */
	private $ids;
	
	/**
	 * @var array
	 */
	private $states;

	/**
	 * @param ContainerAware $container
	 * @param array $ids
	 * @param array $states
	 * @param array $types
	 */
	public function __construct($container, $ids, $states) {
		$this->twig = $container->get('twig');
		$this->em = $container->get('doctrine.orm.entity_manager');
		$this->ids = $ids;
		$this->states = $states;
	}
	
	/**
	 * {@inheritdoc}
	 */
    public function getFilters() {
        return array(
            'show_status' => new \Twig_Filter_Method($this, 'showStatus', array('is_safe' => array('html'))),
            'get_statut' => new \Twig_Filter_Method($this, 'getStatutForAction', array('is_safe' => array('html'))),
        );
    }
    
    /**
     * @param string $entity
     * @param string $column
     */
    public function stateEntity($entity, $column) {
    	return $this->showStatus($entity, $column);
    }
    
    /**
     * @param string $entity
     * @param string $column
     */
    public function showStatus($entity, $column) {
    	if(!$entity) {
    		return;
    	}
    	$reflect = new \ReflectionClass($entity);
    	$template = $this->twig->loadTemplate('OrangeMainBundle:Extra:status.html.twig');
    	return $template->renderBlock('status_'.strtolower($reflect->getShortName().'_'.$column), array(
    			'entity' => $entity, 'ids' => $this->ids, 'states' => $this->states, 'types' => $this->types
    		));
    }
    
    /**
     * @param \Orange\MainBundle\Entity\Action $entity
     */
    public function getStatutForAction($entity) {
    	return $this->em->getRepository('OrangeMainBundle:Statut')->getStatutForAction($entity);
    }
    
    public function getName() {
        return 'main_extension';
    }
}