<?php
namespace Guedia\MainBundle\Annotation;


/**
 * Annotation for parameter injection in Email contents
 *
 * @author Madiagne Sylla <Madiagne.Sylla@orange-sonatel.com>
 *
 * @Annotation
 */
final class QMLogger
{
	public $message;
	
}
