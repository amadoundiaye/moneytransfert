<?php
namespace Guedia\MainBundle\Repository;

class HistoriqueRepository extends EntityRepository {
	
	
	public function listQueryBuilder($criteria) {
		$queryBuilder = $this->createQueryBuilder('q')
			->innerJoin('q.depot', 'd')
			->innerJoin('q.utilisateur', 'u');
		$queryBuilder->andWhere("STRTODATE(q.date, '%Y-%m-%d') >= :from")->setParameter('from', $this->timestampToDate($criteria['from']))
			->andWhere("STRTODATE(q.date, '%Y-%m-%d') <= :end")->setParameter('end', $this->timestampToDate($criteria['end']));
		return $queryBuilder;
	}
	
	/**
	 * @param number $timestamp
	 * @return string
	 */
	private function timestampToDate($timestamp) {
		$date = new \DateTime(sprintf("@%s", $timestamp));
		return $date->format('Y-m-d');
	}
}