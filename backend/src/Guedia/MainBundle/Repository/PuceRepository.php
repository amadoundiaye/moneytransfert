<?php
namespace Guedia\MainBundle\Repository;

class PuceRepository extends EntityRepository {
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param \Guedia\MainBundle\Entity\Country $country
	 * @return \Guedia\MainBundle\Entity\Puce
	 */
	public function findByCashierAndCountry($user, $country) {
		return $this->createQueryBuilder('q')
			->innerJoin('q.utilisateur', 'u')
			->where('u.id = :userId')->andWhere('IDENTITY(q.pays) = :countryId')
			->setParameters(array('userId' => $user->getId(), 'countryId' => $country->getId()))
			->getQuery()
			->getOneOrNullResult();
	}
}