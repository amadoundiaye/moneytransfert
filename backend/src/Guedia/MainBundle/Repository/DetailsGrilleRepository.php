<?php
namespace Guedia\MainBundle\Repository;

class DetailsGrilleRepository extends EntityRepository {
	
	/**
	 * @return number
	 */
	public function getMaxValueByGrille($grilleId) {
		$data = $this->createQueryBuilder('dd')
			->select('MAX(dd.montantMax) as montant')
			->where('IDENTITY(dd.grille) = :grilleId')
			->setParameter('grilleId', $grilleId)
			->getQuery()
			->getArrayResult();
		return count($data)==0 ? null : (float)$data[0]['montant'];
	}
}