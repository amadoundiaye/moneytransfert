<?php
namespace Guedia\MainBundle\Repository;

class UtilisateurRepository extends EntityRepository {
	
	public function listAllQueryBuilder($criteria = null) {
		$criteria = $criteria ? $criteria : new \Guedia\MainBundle\Entity\Utilisateur();
		return $this->createQueryBuilder('q');
	}
	
	public function findAdmins() {
		return $this->createQueryBuilder('q')
			->where("q.roles LIKE '%ADMIN%'")
			->getQuery()
			->getResult();
	}
}