<?php
namespace Guedia\MainBundle\Repository;

use Guedia\MainBundle\Entity\Depot;

class ClientRepository extends EntityRepository {
	
	/**
	 * @param integer $identiteType
	 * @param integer $identite
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 */
	public function findByCriteria($identiteType, $identite, $user) {
		$queryBuilder = $this->createQueryBuilder('q')
		->andWhere('IDENTITY(q.identiteType) = :identiteType')->setParameter('identiteType', $identiteType)
		->andWhere('q.identite LIKE :identite')->setParameter('identite', $identite.'%');
		$qb = $this->_em->getRepository('GuediaMainBundle:Depot')->createQueryBuilder('r')
		->select('IDENTITY(r.client)')->andWhere('r.vendeur = :vendeur');
		$queryBuilder->andWhere($queryBuilder->expr()->in('q.id', $qb->getDQL()))->setParameter('vendeur', $user)->setMaxResults(1);
		$data = $queryBuilder->getQuery()->getResult();
		return count($data) > 0 ? $data[0] : null;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 */
	public function findByCashier($user) {
		$queryBuilder = $this->createQueryBuilder('q');
		$qb = $this->_em->getRepository('GuediaMainBundle:Depot')->createQueryBuilder('r')
			->select('IDENTITY(r.client)')->andWhere('r.vendeur = :vendeur');
		$queryBuilder->andWhere($queryBuilder->expr()->in('q.id', $qb->getDQL()))->setParameter('vendeur', $user);
		return $queryBuilder->getQuery()->getResult();
	}
	
}