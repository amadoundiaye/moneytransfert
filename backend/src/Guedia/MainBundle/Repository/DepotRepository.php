<?php
namespace Guedia\MainBundle\Repository;

use Guedia\MainBundle\Entity\Country;
use Guedia\MainBundle\Entity\TransfertType;
use Guedia\MainBundle\Entity\Etat;

class DepotRepository extends EntityRepository {
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 */
	public function listForExportBuilder($user, $criteria = array()) {
		$queryBuilder = $this->createQueryBuilder('q');
		if($user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('q.vendeur = :user')->setParameter('user', $user);
		}
		return $queryBuilder->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', Etat::$ids['validate']);
	}
	
	public function listQueryBuilder($criteria) {
		$queryBuilder = $this->createQueryBuilder('q')
			->innerJoin('q.client', 'c')
			->innerJoin('q.vendeur', 'v')
			->innerJoin('q.etat', 'e');
		if($this->_user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('q.vendeur = :user')->setParameter('user', $this->_user);
		}
		$queryBuilder->andWhere("STRTODATE(q.date, '%Y-%m-%d') >= :from")->setParameter('from', $this->timestampToDate($criteria['from']))
			->andWhere("STRTODATE(q.date, '%Y-%m-%d') <= :end")->setParameter('end', $this->timestampToDate($criteria['end']));
		return $queryBuilder;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 * @return array
	 */
	public function synthesePerNumber($user, $criteria) {
		$queryBuilder = $this->createQueryBuilder('q')
			->select('COUNT(DISTINCT q.client) as customer, COUNT(DISTINCT q.id) as transaction, SUM(q.montantPaye) as amount, SUM(q.commissionSociete) as commission')
			->addSelect('COUNT(DISTINCT q.vendeur) as cashier, SUM((case when IDENTITY(q.transfertType) = :tac then 1 else 0 end)) as tac')
			->addSelect('SUM(q.montantPaye * q.devise) as amount_xof, SUM(q.commissionSociete * q.devise) as commission_xof');
		if($user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('IDENTITY(q.vendeur) = :userId')->setParameter('userId', $user->getId());
		}
		$data = $queryBuilder->andWhere("STRTODATE(q.date, '%Y-%m-%d') >= :from")->setParameter('from', $this->timestampToDate($criteria['from']))
			->andWhere("STRTODATE(q.date, '%Y-%m-%d') <= :end")->setParameter('end', $this->timestampToDate($criteria['end']))
			->setParameter('tac', TransfertType::$ids['tac'])
			->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', Etat::$ids['validate'])
			->getQuery()
			->getArrayResult();
		$data = $data[0];
		$data['amount'] = $data['amount'] ? $data['amount'] : 0;
		$data['tac'] = $data['tac'] ? $data['tac'] : 0;
		$data['amount_xof'] = $data['amount_xof'] ? $data['amount_xof'] : 0;
		$data['commission'] = $data['commission'] ? $data['commission'] : 0;
		$data['commission_xof'] = $data['commission_xof'] ? $data['commission_xof'] : 0;
		$data['classical'] = $data['transaction'] - $data['tac'];
		return $data;
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 * @return array
	 */
	public function commissionAndAmountPerMonth($user) {
		$first = \DateTime::createFromFormat('Y-m-d', date('Y-m-d'))->sub(new \DateInterval('P11M'))->format('Ym');
		$queryBuilder = $this->createQueryBuilder('q')
			->select("DATE_FORMAT(q.date, '%Y%m') as yearmonth, SUM(q.montantPaye) as amount, SUM(q.commissionSociete) as commission");
		if($user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('IDENTITY(q.vendeur) = :userId')->setParameter('userId', $user->getId());
		}
		return $queryBuilder->andWhere("DATE_FORMAT(q.date, '%Y%m') >= :firstmonth")->setParameter('firstmonth', $first)
		->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', Etat::$ids['validate'])
			->groupBy('yearmonth')
			->orderBy('yearmonth', 'ASC')
			->getQuery()
			->getArrayResult();
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 * @return array
	 */
	public function commissionAndAmountPerCashier($user, $criteria) {
		$queryBuilder = $this->createQueryBuilder('q')
			->select("u.id as id, CONCAT(u.firstname, ' ', u.lastname) as fullname, u.address as address, SUM(q.montantPaye) as amount, SUM(q.commissionSociete) as commission")
			->innerJoin('q.vendeur', 'u');
		if($user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('IDENTITY(q.vendeur) = :userId')->setParameter('userId', $user->getId());
		}
		return $queryBuilder->andWhere("STRTODATE(q.date, '%Y-%m-%d') >= :from")->setParameter('from', $this->timestampToDate($criteria['from']))
			->andWhere("STRTODATE(q.date, '%Y-%m-%d') <= :end")->setParameter('end', $this->timestampToDate($criteria['end']))
			->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', Etat::$ids['validate'])
			->groupBy('q.vendeur')
			->getQuery()
			->getArrayResult();
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 * @return array
	 */
	public function commissionPerCountry($user, $criteria) {
		$queryBuilder = $this->_em->getRepository('GuediaMainBundle:Country')->createQueryBuilder('c')
			->select('c.id as id, c.name as name, SUM(q.commissionSociete) as value')
			->leftJoin('GuediaMainBundle:Depot', 'q', 'WITH', 'q.beneficiairePays = c');
		if($user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('IDENTITY(q.vendeur) = :userId')->setParameter('userId', $user->getId());
		}
		return $queryBuilder->andWhere("STRTODATE(q.date, '%Y-%m-%d') >= :from")->setParameter('from', $this->timestampToDate($criteria['from']))
			->andWhere("STRTODATE(q.date, '%Y-%m-%d') <= :end")->setParameter('end', $this->timestampToDate($criteria['end']))
			->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', Etat::$ids['validate'])
			->groupBy('c.id')
			->getQuery()
			->getArrayResult();
	}
	
	/**
	 * @param \Guedia\MainBundle\Entity\Utilisateur $user
	 * @param array $criteria
	 * @return array
	 */
	public function evolutionInOneMonth($user) {
		$arrData = array();
		$current = date('Ym');
		$previous = \DateTime::createFromFormat('Y-m-d', date('Y-m-d'))->sub(new \DateInterval('P1M'))->format('Ym');
		$queryBuilder = $this->createQueryBuilder('q')
			->select("DATE_FORMAT(q.date, '%Y%m') as month, COUNT(DISTINCT q.vendeur) as cashier, COUNT(DISTINCT q.clientIdentite) as customer, SUM(q.montantPaye) as amount, SUM(q.commissionSociete) as commission");
		if($user->hasRole('ROLE_ADMIN')==false) {
			$queryBuilder->where('IDENTITY(q.vendeur) = :userId')->setParameter('userId', $user->getId());
		}
		$data = $queryBuilder->andWhere("DATE_FORMAT(q.date, '%Y%m') = :month")->setParameter('month', $previous)
			->andWhere("IDENTITY(q.etat) = :etat")->setParameter('etat', Etat::$ids['validate'])
			->groupBy('month')
			->getQuery()
			->getArrayResult();
		array_push($arrData, count($data) ? $data[0] : array('month'=>$previous, 'cashier'=>0, 'customer'=>0, 'amount'=>0, 'commission'=>0));
		$data= $queryBuilder->setParameter('month', $current)
			->getQuery()
			->getArrayResult();
			array_push($arrData, count($data) ? $data[0] : array('month'=>$current, 'cashier'=>0, 'customer'=>0, 'amount'=>0, 'commission'=>0));
		return $arrData;
	}
	
	/**
	 * @param number $timestamp
	 * @return string
	 */
	private function timestampToDate($timestamp) {
		$date = new \DateTime(sprintf("@%s", $timestamp));
		return $date->format('Y-m-d');
	}
}