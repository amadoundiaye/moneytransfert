<?php 

// The blacklisted ips.
$authorized_ips = array('40.121.219.44');
$mode_dev = true;

// The function to get the visitor's IP.
function getUserIP(){
    //check ip from share internet
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    //to check ip is pass from proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

if(!$mode_dev){
    //The user
    $visitorIp = getUserIP();
    //var_dump($visitorIp);
    // Now let's search if this IP is authorized
    $status = array_search($authorized_ips, $visitorIp);

    // Let's check if $status has a true OR false value.
    if($status !== false){
        die("Unauthorized");
    }else{
?>
    <!doctype html> <html ng-app="minovateApp" ng-controller="MainCtrl" class="no-js {{containerClass}}"> <head> <meta charset="utf-8"> <title>TransMobile</title> <meta name="description" content=""> <meta name="viewport" content="width=device-width"> <!-- Place favicon.ico and apple-touch-icon.png in the root directory --> <link rel="icon" type="image/ico" href="favicon.ico"> <link rel="stylesheet" href="styles/vendor.192026f4.css"> <link rel="stylesheet" href="styles/main.f772ddd8.css"> <link rel="stylesheet" href="styles/style.07a639b5.css"> </head> <body id="minovate" class="{{main.settings.navbarHeaderColor}} {{main.settings.activeColor}} {{containerClass}} header-fixed aside-fixed rightbar-hidden appWrapper" ng-class="{'header-fixed': main.settings.headerFixed, 'header-static': !main.settings.headerFixed, 'aside-fixed': main.settings.asideFixed, 'aside-static': !main.settings.asideFixed, 'rightbar-show': main.settings.rightbarShow, 'rightbar-hidden': !main.settings.rightbarShow}"> <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]--> <!-- Application content --> <div id="wrap" ui-view autoscroll="false"></div> <!-- Page Loader --> <div id="pageloader" page-loader></div> <!--[if lt IE 9]>
    <script src="scripts/oldieshim.94d3e0c0.js"></script>
    <![endif]--> <script src="scripts/vendor.37db1632.js"></script> <script src="scripts/app.a84b1a31.js"></script>
<?php
    }
}else{
?>
    <!doctype html> <html ng-app="minovateApp" ng-controller="MainCtrl" class="no-js {{containerClass}}"> <head> <meta charset="utf-8"> <title>TransMobile</title> <meta name="description" content=""> <meta name="viewport" content="width=device-width"> <!-- Place favicon.ico and apple-touch-icon.png in the root directory --> <link rel="icon" type="image/ico" href="favicon.ico"> <link rel="stylesheet" href="styles/vendor.192026f4.css"> <link rel="stylesheet" href="styles/main.f772ddd8.css"> <link rel="stylesheet" href="styles/style.07a639b5.css"> </head> <body id="minovate" class="{{main.settings.navbarHeaderColor}} {{main.settings.activeColor}} {{containerClass}} header-fixed aside-fixed rightbar-hidden appWrapper" ng-class="{'header-fixed': main.settings.headerFixed, 'header-static': !main.settings.headerFixed, 'aside-fixed': main.settings.asideFixed, 'aside-static': !main.settings.asideFixed, 'rightbar-show': main.settings.rightbarShow, 'rightbar-hidden': !main.settings.rightbarShow}"> <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]--> <!-- Application content --> <div id="wrap" ui-view autoscroll="false"></div> <!-- Page Loader --> <div id="pageloader" page-loader></div> <!--[if lt IE 9]>
    <script src="scripts/oldieshim.94d3e0c0.js"></script>
    <![endif]--> <script src="scripts/vendor.37db1632.js"></script> <script src="scripts/app.a84b1a31.js"></script>
<?php
}
?>


