'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:FormUploadCtrl
 * @description
 * # FormUploadCtrl
 * Controller of the minovateApp
 */
app
  .controller('FormUploadCtrl', ['$scope', 'FileUploader', '$localStorage', 'USER_ROLE', '$location', function($scope, FileUploader, $localStorage, USER_ROLE, $location, UPLOAD_URL) {

      if ($localStorage.currentUser.role !== USER_ROLE.ROLE_ADMIN){
          $location.path('app/dashboard');
      }

    var uploader = $scope.uploader = new FileUploader({
        url: "http://vps423165.ovh.net/calculator/upload.php" //enable this option to get f
    });

    // FILTERS

    uploader.filters.push({
      name: 'customFilter',
      fn: function() {
        return this.queue.length < 10;
      }
    });

    uploader.filters.push({
      name: 'imageFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
          var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
          return '|csv|'.indexOf(type) !== -1;
      }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
      //console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
      //console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
      //console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
      //console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
      //console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
      //console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      //console.info('onSuccessItem', fileItem, response, status, headers);
        $scope.message = response.message;
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
      //console.info('onErrorItem', fileItem, response, status, headers);
        $scope.message = response.message;
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
      //console.info('onCancelItem', fileItem, response, status, headers);
        $scope.message = response.message;
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
      //console.info('onCompleteItem', fileItem, response, status, headers);
        $scope.message = response.message;
    };
    uploader.onCompleteAll = function() {
      //console.info('onCompleteAll');
        location.reload();
    };

    //console.info('uploader', uploader);
  }])

    .controller('FeesTableCtrl', function ($scope, $http, $state, $location, DTOptionsBuilder, DTColumnBuilder, DTInstanceFactory, DTRenderer, $localStorage, $actionService, BASE_URL, $utilService) {
        var vm = this;
        var currentUser = $localStorage.currentUser;
        vm.message = '';
        function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
            angular.element('td', nRow).find('a[callback="editSim"]').unbind('click');
            angular.element('td', nRow).find('a[callback="editSim"]').bind('click', function() {
                angular.element('.row_selected').removeClass('row_selected');
                angular.element(nRow).addClass('row_selected');
            });
            angular.element('td', nRow).find('a[callback="statusSim"]').bind('click', function() {
                angular.element('.row_selected').removeClass('row_selected');
                angular.element(nRow).addClass('row_selected');
            });
            return nRow;
        }
        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
            url: BASE_URL + 'fee/list',
            type: 'GET',
            headers: {
                "Authorization": 'Guedia ' + $localStorage.token
            },
            error: function(xhr, error, thrown) {
                $utilService.locker(xhr);
            }
        })
            .withDataProp(function(json){
                //console.log(json.data);
                return json.data;
            })
            .withBootstrap()
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('responsive', true)
            .withPaginationType('full_numbers')
            .withDOM('rtip')
            .withOption('bInfo', true)
            // Activate col reorder plugin
            .withColReorder()
            .withColReorderCallback(function() {
                //console.log('Columns order has been changed with: ' + this.fnOrder());
            })
            .withOption('rowCallback', rowCallback);

        vm.dtColumns = [
            DTColumnBuilder.newColumn('montant_min').withTitle('Montant Min').withOption('width', '40%'),
            DTColumnBuilder.newColumn('montant_max').withTitle('Montant Max'),
            DTColumnBuilder.newColumn('tarif').withTitle('Tarif')
        ];
        vm.dtInstance = DTInstanceFactory.newDTInstance();

    });
