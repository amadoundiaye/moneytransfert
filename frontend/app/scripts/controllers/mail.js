'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MailComposeCtrl
 * @description
 * # MailComposeCtrl
 * Controller of the minovateApp
 */
app
  .controller('MailCtrl', function ($scope, $localStorage, $actionService, $utilService, USER_ROLE) {
      $scope.page = {
          title: 'E-Mail',
          subtitle: "Send Mail"
      };

    $scope.currentUser = $localStorage.currentUser;

    $scope.availableRecipients = ['support@guedia.ovh'];
    $scope.form = {};
	$scope.contactus = function() {
		if($localStorage.currentUser.role != USER_ROLE.ROLE_ADMIN) {
			$actionService.contactus($scope.form, function (response) {
			  if (response.success === true) {
				  $utilService.notify($scope, '#content', response.message);
			  } else {
				  $scope.error = !response.success;
				  $scope.message = response.message;
				  $scope.formError = response.errors;
			  }
			  $scope.error = !response.success;
			  $scope.message = response.message;
			});
		} else {
			var resultat = $actionService.sendPromo($scope.form, function (response) {
				return response;
			  });
			  if(resultat != -1) {
				  $utilService.notify($scope, '#content', 'The mail will be send to the customer sooner');
				  $scope.error = false;
				  $scope.message = 'The mail will be send to the customer sooner';
			  } else {
				  $scope.error = true;
				  $scope.message = 'Attention!!! An error occured when trying to send promo';
			  }
		}
	};
  });
