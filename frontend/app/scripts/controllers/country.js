'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:SimCtrl
 * @description
 * # SimCtrl
 * Controller of the minovateApp
 */
app
  .controller('CountryCtrl', function ($scope, $location, USER_ROLE, $localStorage) {
    $scope.page = {
      title: 'List',
      subtitle: "Countries list"
    };

      if ($localStorage.currentUser.role !== USER_ROLE.ROLE_ADMIN){
          $location.path('app/dashboard');
      }

  })

  .controller('CountryTableCtrl', function ($scope, $http, $state, $location, DTOptionsBuilder, DTColumnBuilder, DTInstanceFactory, DTRenderer, $localStorage, $actionService, BASE_URL, md5) {
      var vm = this;
      var currentUser = $localStorage.currentUser;
      vm.message = '';
      function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
          angular.element('td', nRow).find('a[callback="editCountry"]').unbind('click');
          angular.element('td', nRow).find('a[callback="editCountry"]').bind('click', function() {
              $scope.$apply(function() {
                  vm.editCountry(aData);
              });
              angular.element('.row_selected').removeClass('row_selected');
              angular.element(nRow).addClass('row_selected');
          });
          angular.element('td', nRow).find('a[callback="statusCountry"]').bind('click', function() {
              $scope.$apply(function() {
                  //vm.changeStatusCountry(aData);

              });
              angular.element('.row_selected').removeClass('row_selected');
              angular.element(nRow).addClass('row_selected');
          });
          return nRow;
      }
      vm.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
	          url: BASE_URL + 'country/list',
	          type: 'GET',
	          headers: {
	              "Authorization": 'Guedia ' + $localStorage.token
	          },
              error: function(xhr, error, thrown) {
                  $utilService.locker(xhr);
              }
      })
      .withDataProp(function(json){
          return json.data;
      })

          .withBootstrap()
          .withOption('processing', true)
          .withOption('serverSide', true)
          .withOption('responsive', true)
          .withPaginationType('full_numbers')
          .withDOM('lfrtip')
          .withOption('bInfo', true)
          // Activate col reorder plugin
          .withColReorder()
          .withColReorderCallback(function() {
              console.log('Columns order has been changed with: ' + this.fnOrder());
          })
          .withOption('lengthMenu', [[10, 25, 50, 100, 10000], [10, 25, 50, 100, "All"]])
          .withOption('rowCallback', rowCallback);

      vm.dtColumns = [
	          DTColumnBuilder.newColumn('name').withTitle('Name').notSortable(),
	          DTColumnBuilder.newColumn('classical').withTitle('Classical code').withOption('width', '20%').notSortable(),
	          DTColumnBuilder.newColumn('tac').withTitle('TAC code').withOption('width', '20%').notSortable(),
	          DTColumnBuilder.newColumn('action').withTitle('Action').withOption('width', '170px').notSortable()
	      ];
      vm.dtInstance = DTInstanceFactory.newDTInstance();



      function editCountry(country) {
          $state.go('app.country.edit', { token: md5.createHash(currentUser.salt + country.id) });
      }

      vm.editCountry = editCountry;
  })
  .controller('CountryEditCtrl', function ($scope, $state, $stateParams, $selectService, $actionService, $utilService) {
      $scope.page = {
          title: 'Edition',
          subtitle: "Edit Country"
      };

      $scope.form = {};

	  $actionService.showCountry($stateParams.token, function (response) {
          if (response.success === true) {
              $scope.country = response.data;
              $scope.form.classicalCode = response.data.classicalCode;
              $scope.form.tacCode = response.data.tacCode;
              //$utilService.log($scope.form);
          }
      });

      $scope.update = function() {
    	  $actionService.updateCountry($stateParams.token, $scope.form, function (response) {
              if (response.success === true) {
            	  $utilService.notify($scope, '#content', response.message);
                  $state.go('app.country.list');
              } else {
                  $scope.error = !response.success;
                  $scope.message = response.message;
                  $scope.formError = response.errors;
              }
          });
      };

  });

