'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:TransactionCtrl
 * @description
 * # TransactionCtrl
 * Controller of the minovateApp
 */
app
    .controller('TransactionCtrl', function ($scope, $authService, $actionService, $moment, $tablehandler, $utilService, $localStorage, USER_ROLE, DOWNLOAD_URL) {
        $scope.page = {
            title: 'List',
            subtitle: "Transaction's list"
        };
        $scope.startDate = $moment().startOf('month').format('MMMM D, YYYY');
        $scope.endDate = $moment().format('MMMM D, YYYY');
        $scope.rangeOptions = {
            ranges: {
                Today: [$moment(), $moment()],
                Yesterday: [$moment().subtract(1, 'days'), $moment().subtract(1, 'days')],
                'Last 7 Days': [$moment().subtract(6, 'days'), $moment()],
                'Last 30 Days': [$moment().subtract(29, 'days'), $moment()],
                'This Month': [$moment().startOf('month'), $moment().endOf('month')],
                'Last Month': [$moment().subtract(1, 'month').startOf('month'), $moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            startDate: $moment().startOf('month'),
            endDate: $moment(),
            parentEl: '#content'
        };
        $scope.$watch("[startDate, endDate]", function(newValue, oldValue) {
            $actionService.filterByDateForTransaction(newValue, function(response) {
                if(response.success === true) {
                    if($tablehandler.getVM().dtInstance.id) {
                        $tablehandler.getVM().dtInstance.rerender();
                    }
                }
            });
        });
        $scope.exportTransaction = function() {
            $utilService.notify($scope, '#content', "Exportation en cours, merci de patientez...");
            $actionService.exportTransaction(function(response) {
                if(response.status==200) {
                    window.location = DOWNLOAD_URL + 'transaction.xlsx';
                }
            });
        };
        $scope.DOWNLOAD_URL = DOWNLOAD_URL;
        $scope.currentUser = $localStorage.currentUser;
        $scope.user_role = USER_ROLE;
    })
    .controller('NewTransactionCtrl', function ($scope, $localStorage, $actionService, $authService, $selectService, $state, $utilService, md5) {
        $scope.page = {
            title: 'New Transaction',
            subtitle: "Transaction's form"
        };

        $scope.EmptyField = function(){
            $scope.form.montant = "";
        };

        var currentUser = $localStorage.currentUser;
        $selectService.getCountry(function (response) { $scope.arrCountry = response; });
        $selectService.getIdentityType(function (response) { $scope.arrIdentityType = response; });
        $selectService.getTransferType(function (response) { $scope.arrTransfertType = response; });
        $scope.ids = $localStorage.parameters.ids;
        $scope.currentUser = $localStorage.currentUser;

        if($utilService.isset($localStorage.fees.classic)) {
            $actionService.showSettings(function (response) { $localStorage.fees = response;});
        }
        $scope.form = {};
        $scope.form.client = { prenom: null, nom: null, email: null, telephone: null, address: null, identiteType: null, identite: null};
        $scope.formExtra = {};

        $scope.form.reference = $utilService.trxRefeference(10, 'N');
        $scope.transfertfees = parseFloat($localStorage.fees.classic.rate.commission)/100;
        $scope.tactransfertfees = parseFloat($localStorage.fees.tac.rate.commission)/100;
        $scope.changefees = parseFloat($localStorage.fees.classic.rate.change)/100;

        $scope.settingClassicCommissionShop = parseFloat($localStorage.fees.classic.percentile.shop)/100;
        $scope.settingClassicCommissionSociety = parseFloat($localStorage.fees.classic.percentile.society)/100;
        $scope.settingClassicFees = parseFloat($localStorage.fees.classic.rate.commission)/100;
        $scope.settingClassicChangeFees = parseFloat($localStorage.fees.classic.rate.change)/100;

        $scope.settingTacCommissionShop = parseFloat($localStorage.fees.tac.percentile.shop)/100;
        $scope.settingTacCommissionSociety = parseFloat($localStorage.fees.tac.percentile.society)/100;
        $scope.settingTacFees = parseFloat($localStorage.fees.tac.rate.commission)/100;
        $scope.settingTacChangefees = parseFloat($localStorage.fees.tac.rate.change)/100;

        $scope.maxClassic = $localStorage.parameters.values.maxClassic;
        $scope.maxTac = $localStorage.parameters.values.maxTac;

        $scope.Calculate = function () {
            if(parseInt($scope.form.transfertType) === parseInt($scope.ids.transfertType.classic)) {
                if($scope.form.transfertMode === "1") {
                    $localStorage.formCalcul = {};
                    $utilService.change(function(response) {
                        $scope.cadtoxof = response.CADtoXOF;
                        var omfees = 0;
                        $scope.form.omfees = omfees;
                        $actionService.getFee(parseFloat($scope.form.montant), function (response) {
                            $scope.form.mobiletransfertfees = parseFloat(response.tarif);
                            $scope.form.mobiletransfertfees = $utilService.normalizeCAD($scope.form.mobiletransfertfees);

                            $scope.form.transfertfees = parseFloat($scope.form.omfees) + parseFloat($scope.form.mobiletransfertfees);
                            $scope.form.transfertfees = $utilService.normalizeCAD($scope.form.transfertfees);

                            $scope.form.amouttopaid = parseFloat($scope.form.montant) + parseFloat($scope.form.mobiletransfertfees);
                            $scope.form.amouttopaid = $utilService.normalizeCAD($scope.form.amouttopaid);

                            $scope.form.exchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));
                            $scope.form.omreceivedamount = $utilService.normalizeFCFA($scope.form.montant * $scope.form.exchangerate);

                            if(parseFloat($scope.form.omreceivedamount) > parseFloat($scope.maxClassic)){
                                $utilService.notify($scope, '#content', "Plafond dépassé!.");
                                $scope.form.montant = 0;
                            }


                            $localStorage.formCalcul.devise = $scope.cadtoxof;
                            $localStorage.formCalcul.commissionOM = $scope.form.omfees;
                            $localStorage.formCalcul.montantPaye = $scope.form.amouttopaid;
                            $localStorage.formCalcul.beneficiaireMontant = $scope.form.omreceivedamount;
                            $localStorage.formCalcul.commissionBoutique = $utilService.normalizeCAD(parseFloat($scope.form.mobiletransfertfees) * $scope.settingClassicCommissionShop);
                            $localStorage.formCalcul.commissionSociete = $utilService.normalizeCAD(parseFloat($scope.form.mobiletransfertfees) * $scope.settingClassicCommissionSociety);
                        });
                    });
                } else {
                    $localStorage.formCalcul = {};
                    $utilService.change(function(response) {
                        $scope.cadtoxof = response.CADtoXOF;
                        var omfees = 0;
                        $scope.form.r_omfees = omfees;
                        $scope.form.r_exchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100/100 - $scope.changefees));

                        $scope.form.r_amounttosend = parseFloat($scope.form.montant) / parseFloat($scope.form.r_exchangerate) ;
                        $scope.form.r_amounttosend = $utilService.normalizeCAD($scope.form.r_amounttosend);
                        $actionService.getFee(parseFloat($scope.form.r_amounttosend), function (response) {
                            $scope.form.r_mobiletransfertfees = parseFloat(response.tarif);
                            $scope.form.r_mobiletransfertfees = $utilService.normalizeCAD($scope.form.r_mobiletransfertfees);

                            $scope.form.r_transfertfees = parseFloat($scope.form.r_omfees) + parseFloat($scope.form.r_mobiletransfertfees);
                            $scope.form.r_transfertfees = $utilService.normalizeCAD($scope.form.r_transfertfees);

                            $scope.form.r_amouttopaid = parseFloat($scope.form.r_amounttosend) + parseFloat($scope.form.r_mobiletransfertfees);
                            $scope.form.r_amouttopaid = $utilService.normalizeCAD($scope.form.r_amouttopaid);

                            if(parseFloat($scope.form.r_amounttosend) > parseFloat($scope.maxClassic)){
                                $utilService.notify($scope, '#content', "Plafond dépassé!.");
                                $scope.form.r_amounttosend = 0;
                            }

                            $localStorage.formCalcul.devise = $scope.cadtoxof;
                            $localStorage.formCalcul.commissionOM = $scope.form.r_omfees;
                            $localStorage.formCalcul.montantPaye = $scope.form.r_amouttopaid;
                            $localStorage.formCalcul.beneficiaireMontant = parseFloat($scope.form.montant);//$utilService.normalizeFCFA(parseFloat($scope.form.r_amounttosend) * parseFloat($scope.form.r_exchangerate));//$scope.form.r_amounttosend;
                            $localStorage.formCalcul.commissionBoutique = $utilService.normalizeCAD(parseFloat($scope.form.r_mobiletransfertfees) * $scope.settingClassicCommissionShop);
                            $localStorage.formCalcul.commissionSociete = $utilService.normalizeCAD(parseFloat($scope.form.r_mobiletransfertfees) * $scope.settingClassicCommissionSociety);
                        });
                    });
                }
            } else {
                /*$scope.form.transfertMode = "2";
                $localStorage.formCalcul = {};
                $utilService.change(function(response) {
                    $scope.cadtoxof = response.CADtoXOF;

                    $scope.form.r_tacexchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100/100 - $scope.changefees));
                    $scope.form.r_tacamounttosend = parseFloat($scope.form.montant) / parseFloat($scope.form.r_tacexchangerate);
                    $scope.form.r_tacamounttosend = $utilService.normalizeCAD($scope.form.r_tacamounttosend);

                    $actionService.getFee(parseFloat($scope.form.r_tacamounttosend), function (response) {
                        $scope.form.r_tacmobiletransfertfees = parseFloat(response.tarif);
                        $scope.form.r_tacmobiletransfertfees = $utilService.normalizeCAD($scope.form.r_tacmobiletransfertfees);

                        $utilService.calculator(parseFloat($scope.form.montant), function (response) {
                            if (response.success === true) {
                                if (response.messages.length == 2) {
                                    $scope.tacFees = response.messages[0].value;
                                } else {
                                    $utilService.notify($scope, '#content', "Plafond dépassé!");
                                }
                                $scope.form.r_tacomfees = $scope.tacFees / $scope.form.r_tacexchangerate;
                                $scope.form.r_tacomfees = $utilService.normalizeCAD($scope.form.r_tacomfees);

                                $scope.form.r_tactransfertfees = $scope.form.r_tacmobiletransfertfees;
                                $scope.form.r_tactransfertfees = $utilService.normalizeCAD($scope.form.r_tactransfertfees);

                                $scope.form.r_tacamouttopaid = $utilService.normalizeCAD($scope.form.r_tacamounttosend + $scope.form.r_tactransfertfees + parseFloat($scope.form.r_tacomfees));
                            }

                            $localStorage.formCalcul.devise = $scope.cadtoxof;
                            $localStorage.formCalcul.commissionOM = $scope.form.r_tacomfees;
                            $localStorage.formCalcul.montantPaye = $scope.form.r_tacamouttopaid;
                            $localStorage.formCalcul.beneficiaireMontant = $utilService.normalizeFCFA(parseFloat($scope.form.montant) + parseFloat($scope.tacFees));//$scope.form.r_tacamounttosend;
                            $localStorage.formCalcul.commissionBoutique = $utilService.normalizeCAD(parseFloat($scope.form.r_tactransfertfees) * $scope.settingTacCommissionShop);
                            $localStorage.formCalcul.commissionSociete = $utilService.normalizeCAD(parseFloat($scope.form.r_tactransfertfees) * $scope.settingTacCommissionSociety);
                        });
                    });
                });*/

                if($scope.form.transfertMode === "1") {
                    $localStorage.formCalcul = {};
                    $utilService.change(function(response) {
                        $scope.cadtoxof = response.CADtoXOF;
                        $actionService.getFee(parseFloat($scope.form.montant), function (response) {
                            $scope.form.tacmobiletransfertfees = parseFloat(response.tarif);
                            $scope.form.tacexchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));
                            $scope.form.tacomreceivedamount = $utilService.normalizeFCFA($scope.form.montant * $scope.form.tacexchangerate);

                            $utilService.calculator(parseFloat($scope.form.tacomreceivedamount), function (response) {
                                if (response.success === true) {
                                    if (response.messages.length > 0) {
                                        $scope.tacFees = response.messages[0].value;
                                    } else {
                                        $utilService.notify($scope, '#content', "Plafond dépassé!.");
                                    }
                                    $scope.form.tacomfees = $scope.tacFees / $scope.form.tacexchangerate;
                                    $scope.form.tacomfees = $utilService.normalizeCAD($scope.form.tacomfees);

                                    $scope.form.tactransfertfees = parseFloat($scope.form.tacomfees) + parseFloat($scope.form.tacmobiletransfertfees);
                                    $scope.form.tactransfertfees = $utilService.normalizeCAD($scope.form.tactransfertfees);

                                    $scope.form.tacamouttopaid = parseFloat($scope.form.montant) + parseFloat($scope.form.tactransfertfees);
                                    $scope.form.tacamouttopaid = $utilService.normalizeCAD($scope.form.tacamouttopaid);

                                }

                                $localStorage.formCalcul.devise = $scope.cadtoxof;
                                $localStorage.formCalcul.commissionOM = $scope.form.tacomfees;
                                $localStorage.formCalcul.montantPaye = $scope.form.tacamouttopaid;
                                $localStorage.formCalcul.beneficiaireMontant = $scope.form.tacomreceivedamount;
                                $localStorage.formCalcul.commissionBoutique = $utilService.normalizeCAD(parseFloat($scope.form.tactransfertfees) * $scope.settingTacCommissionShop);
                                $localStorage.formCalcul.commissionSociete = $utilService.normalizeCAD(parseFloat($scope.form.tactransfertfees) * $scope.settingTacCommissionSociety);
                            });
                        });
                    });
                }
                else{
                    $scope.form.transfertMode = "2";
                    $localStorage.formCalcul = {};
                    $utilService.change(function(response) {
                        $scope.cadtoxof = response.CADtoXOF;

                        $scope.form.r_tacexchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100/100 - $scope.changefees));
                        $scope.form.r_tacamounttosend = parseFloat($scope.form.montant) / parseFloat($scope.form.r_tacexchangerate);
                        $scope.form.r_tacamounttosend = $utilService.normalizeCAD($scope.form.r_tacamounttosend);

                        $actionService.getFee(parseFloat($scope.form.r_tacamounttosend), function (response) {
                            $scope.form.r_tacmobiletransfertfees = parseFloat(response.tarif);
                            $scope.form.r_tacmobiletransfertfees = $utilService.normalizeCAD($scope.form.r_tacmobiletransfertfees);

                            $utilService.calculator(parseFloat($scope.form.montant), function (response) {
                                if (response.success === true) {
                                    if (response.messages.length > 0) {
                                        $scope.tacFees = response.messages[0].value;
                                    } else {
                                        $utilService.notify($scope, '#content', "Plafond dépassé!");
                                    }
                                    $scope.form.r_tacomfees = $scope.tacFees / $scope.form.r_tacexchangerate;
                                    $scope.form.r_tacomfees = $utilService.normalizeCAD($scope.form.r_tacomfees);

                                    $scope.form.r_tactransfertfees = $scope.form.r_tacmobiletransfertfees;
                                    $scope.form.r_tactransfertfees = $utilService.normalizeCAD($scope.form.r_tactransfertfees);

                                    $scope.form.r_tacamouttopaid = $utilService.normalizeCAD($scope.form.r_tacamounttosend + $scope.form.r_tactransfertfees + parseFloat($scope.form.r_tacomfees));
                                }

                                $localStorage.formCalcul.devise = $scope.cadtoxof;
                                $localStorage.formCalcul.commissionOM = $scope.form.r_tacomfees;
                                $localStorage.formCalcul.montantPaye = $scope.form.r_tacamouttopaid;
                                $localStorage.formCalcul.beneficiaireMontant = $utilService.normalizeFCFA(parseFloat($scope.form.montant) + parseFloat($scope.tacFees));//$scope.form.r_tacamounttosend;
                                $localStorage.formCalcul.commissionBoutique = $utilService.normalizeCAD(parseFloat($scope.form.r_tactransfertfees) * $scope.settingTacCommissionShop);
                                $localStorage.formCalcul.commissionSociete = $utilService.normalizeCAD(parseFloat($scope.form.r_tactransfertfees) * $scope.settingTacCommissionSociety);
                            });
                        });
                    });
                }
            }
        };

        $scope.create = function() {
            //console.log($utilService.isset($scope.form.transfertType));
            $scope.error = true;
            if(!$utilService.isset($scope.form.beneficiairePays)){
                $scope.message = "Veuillez choisir le pays";
            }else if(!$utilService.isset($scope.form.transfertType)){
                $scope.message = "Veuillez choisr le type de transfert";
            }else if(!$utilService.isset($scope.form.transfertMode)){
                $scope.message = "Veuillez choisr le mode de transfert";
            }else if(!$utilService.isset($scope.form.montant)){
                $scope.message = "Veuillez saisir le montant à transfèrer";
            }else if(!$utilService.isset($scope.form.client.identiteType)){
                $scope.message = "Veuillez choisr le type pièce d'identification";
            }else if(!$utilService.isset($scope.form.client.identite)){
                $scope.message = "Veuillez saisir le numéro de la pièce d'identification";
            }else if(!$utilService.isset($scope.form.client.prenom)){
                $scope.message = "Veuillez saisir le prénom du client";
            }else if(!$utilService.isset($scope.form.client.nom)){
                $scope.message = "Veuillez saisir le nom du client";
            }else if(!$utilService.isset($scope.form.client.address)){
                $scope.message = "Veuillez saisir l'adresse du client";
            }else if(!$utilService.isset($scope.form.client.telephone)){
                $scope.message = "Veuillez saisir le téléphone du client";
            }else if(!$utilService.isset($scope.form.beneficiairePrenom)){
                $scope.message = "Veuillez saisir le prénom du bénéficiaire";
            }else if(!$utilService.isset($scope.form.beneficiaireNom)){
                $scope.message = "Veuillez saisir le nom du bénéficiaire";
            }else if(!$utilService.isset($scope.form.beneficiaireTelephone)){
                $scope.message = "Veuillez saisir le téléphone du bénéficiaire";//.substring(0, 1)
            }else if(($scope.form.beneficiaireTelephone).length!=9 || ($scope.form.beneficiaireTelephone).substring(0, 1)!= "7"){
                $scope.message = "Veuillez saisir un numéro de téléphone valide pour le bénéficiaire";
            }else{
                $scope.error = false;
                $scope.message = "";

                $scope.formCalcul = $localStorage.formCalcul;
                $scope.formCalcul.transfertType = $scope.form.transfertType; //1=classic; 2=Tac
                $scope.formCalcul.transfertMode = $scope.form.transfertMode; //1=toSend;  2=toReceive
                $scope.formCalcul.reference = $scope.form.reference;
                if($scope.form.transfertMode === "1") {
                    $scope.changefees = parseFloat($localStorage.fees.classic.rate.change) / 100;
                    $scope.exchangerate = $utilService.normalizeEffectifRate(parseFloat($localStorage.formCalcul.devise) * (100 / 100 - $scope.changefees));
                    $scope.formCalcul.montant = parseFloat($scope.form.montant) * parseFloat($scope.exchangerate);
                }else{
                    $scope.formCalcul.montant = parseFloat($scope.form.montant);
                }
                $scope.formCalcul.client = $scope.form.client;
                $scope.formCalcul.client.email = $utilService.isset($scope.form.client.email) ? $scope.form.client.email : null;
                $scope.formCalcul.beneficiairePrenom = $scope.form.beneficiairePrenom;
                $scope.formCalcul.beneficiaireNom = $scope.form.beneficiaireNom;
                $scope.formCalcul.beneficiaireTelephone = $scope.form.beneficiaireTelephone;
                $scope.formCalcul.beneficiairePays = $scope.form.beneficiairePays ? $scope.form.beneficiairePays.id : null;

                if($scope.formCalcul.transfertType == 1){//Classic
                    if($scope.formCalcul.transfertMode == 1){ //toSend CAD

                    }else{//toReceive FCFA

                    }
                }else{                                  //Tac
                    if($scope.formCalcul.transfertMode == 1){ //toSend CAD

                    }else{//toReceive FCFA

                    }
                }


                $actionService.newTransaction($scope.formCalcul, function(result) {

                    if (result.success === true) {
                        $state.go('app.transaction.details', { token: md5.createHash(currentUser.salt + result.data.id) });
                    }
                    $scope.error = !result.success;
                    $scope.message = result.message;
                });
            }




        };

        $scope.actif = false;
        $scope.checked = false;
        $scope.enableSearch = function(){
            $scope.tal = $scope.form.client.identite;
            $scope.tal = $scope.tal.toString();
            if($utilService.isset($scope.form.client.identiteType) && $utilService.isset($scope.form.client.identite) && $scope.tal.length > 5){
                $scope.actif = true;
                return true;
            }
            $scope.actif = false;
            return false;
        };

        $scope.searchInfo = function(){
            $scope.customer = {};
            if($utilService.isset($scope.form.client.identiteType) && $utilService.isset($scope.form.client.identite)){
                $scope.customer.clientIdentiteType = $scope.form.client.identiteType;
                $scope.customer.clientIdentite = $scope.form.client.identite;
                $actionService.searchCustomer($scope.customer, function(result) {
                    if(result.result){
                        $scope.checked = true;
                        $scope.form.client.prenom = result.data.prenom;
                        $scope.form.client.nom = result.data.nom;
                        $scope.form.client.telephone = result.data.telephone;
                        $scope.form.client.email = result.data.email;
                        $scope.form.client.address = result.data.address;
                        $scope.form.client.identite = result.data.identite;
                    }else{
                        $scope.checked = false;
                    }
                });
            }else{
                $utilService.notify($scope, '#content', "Veuillez choisir le type de pièce!");
            }
        };
    })
    .controller('TransactionsTableCtrl', function ($scope, $http, $state, $location, DTOptionsBuilder, DTColumnBuilder, DTInstanceFactory, DTRenderer, $localStorage, $authService, $tablehandler, $utilService, BASE_URL, md5, $stateParams) {
        var vm = this;
        var currentUser = $localStorage.currentUser;
        vm.message = '';
        function rowCallback(nRow, aData) {
            angular.element('td', nRow).find('a[callback="showTransaction"]').unbind('click');
            angular.element('td', nRow).find('a[callback="showTransaction"]').bind('click', function() {
                $scope.$apply(function() {
                    vm.showTransaction(aData);
                });

                angular.element('.row_selected').removeClass('row_selected');
                angular.element(nRow).addClass('row_selected');
            });
            return nRow;
        }
        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
            url: BASE_URL + 'transaction/list',
            type: 'GET',
            headers: {
                "Authorization": 'Guedia ' + $localStorage.token
            },
            error: function(xhr, error, thrown) {
                $utilService.locker(xhr);
            }
        })
            .withDataProp(function(json) {
                return json.data;
            })
            .withBootstrap()
            .withOption('processing', true)
            .withOption('serverSide', true)
            .withOption('responsive', true)
            .withPaginationType('full_numbers')
            .withDOM('lfrtip')
            .withOption('bInfo', true)
            .withOption('order', [3, 'desc'])
            //.withColumnFilter()
            // Activate col reorder plugin
            .withColReorder()
            .withColReorderCallback(function() {
                //console.log('Columns order has been changed with: ' + this.fnOrder());
            })
            .withOption('lengthMenu', [[10, 25, 50, 100, 10000], [10, 25, 50, 100, "All"]])
            .withOption('rowCallback', rowCallback);

        vm.dtColumns = [
            DTColumnBuilder.newColumn('reference').withTitle('Reference').notSortable(),
            DTColumnBuilder.newColumn('sender').withTitle('Sender').notSortable(),
            DTColumnBuilder.newColumn('recipient').withTitle('Recipient').notSortable(),
            DTColumnBuilder.newColumn('date').withTitle('Date'),
            DTColumnBuilder.newColumn('amount').withTitle('Amount').notSortable(),
            DTColumnBuilder.newColumn('status').withTitle('Status').notSortable(),
            DTColumnBuilder.newColumn('resultat').withTitle('Ussd Result').notSortable()
        ];

        vm.dtInstance = DTInstanceFactory.newDTInstance();

        function showTransaction(transaction) {
            $state.go('app.transaction.details', { token: md5.createHash(currentUser.salt + transaction.id) });
        }

        vm.showTransaction = showTransaction;
        $tablehandler.setVM(vm);

    })
    .controller('ShowTransactionCtrl', function ($scope, $stateParams, $localStorage, $actionService, $utilService, $state, md5) {
        $scope.page = {
            title: 'Details',
            subtitle: "Transaction details"
        };
        $scope.colors = {1: "warning", 2: "success", 3: "success", 4: "danger"};

        $actionService.showTransaction($stateParams.token, function (response) {
            $scope.transaction = response.data;
            $scope.changefees = parseFloat($localStorage.fees.classic.rate.change) / 100;
            $scope.exchangerate = $utilService.normalizeEffectifRate(parseFloat($scope.transaction.devise) * (100 / 100 - $scope.changefees));
        });

        $scope.confirm = function() {
            $utilService.notify($scope, '#content', "Please wait ...");
            $scope.transaction.etat.id = 1;
            $actionService.confirmTransaction(md5.createHash($localStorage.currentUser.salt + $scope.transaction.id), $scope.exchangerate, function(response) {
                //console.log(response);
                $scope.transaction = response.data;
                $utilService.notify($scope, '#content', response.message);
            });
        };

        $scope.sendMail = function() {
            $utilService.change(function(response) {
                $scope.cadtoxof = response.CADtoXOF;
                $scope.changefees = parseFloat($localStorage.fees.classic.rate.change) / 100;
                $scope.exchangerate = $utilService.normalizeEffectifRate(parseFloat($scope.test) * (100 / 100 - $scope.changefees));
                $actionService.sendTransaction(md5.createHash($localStorage.currentUser.salt + $scope.transaction.id), $scope.exchangerate, function(response) {
                    $utilService.notify($scope, '#content', response.message);
                });
            });

        };

        $scope.reject = function() {
            $actionService.rejectTransaction(md5.createHash($localStorage.currentUser.salt + $scope.transaction.id), function(response) {
                $scope.transaction = response.data;
                $utilService.notify($scope, '#content', response.message);
            });
        };

        /*$utilService.change(function(response) {
            $scope.cadtoxof = response.CADtoXOF;
            $scope.changefees = parseFloat($localStorage.fees.classic.rate.change) / 100;
            console.log(parseFloat($scope.test));
            $scope.exchangerate = $utilService.normalizeEffectifRate(parseFloat($scope.test) * (100 / 100 - $scope.changefees));
        });*/
    });