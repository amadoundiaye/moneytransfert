'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:ShopOrdersCtrl
 * @description
 * # ShopOrdersCtrl
 * Controller of the minovateApp
 */
app
  .controller('TraceCtrl', function ($scope, $moment, $actionService, $tablehandler, $location, USER_ROLE, $localStorage) {
    $scope.page = {
      title: 'List',
      subtitle: "Trace's list"
    };

      if ($localStorage.currentUser.role !== USER_ROLE.ROLE_ADMIN){
          $location.path('app/dashboard');
      }

    $scope.startDate = $moment().startOf('month').format('MMMM D, YYYY');
    $scope.endDate = $moment().format('MMMM D, YYYY');
    $scope.rangeOptions = {
      ranges: {
        Today: [$moment(), $moment()],
        Yesterday: [$moment().subtract(1, 'days'), $moment().subtract(1, 'days')],
        'Last 7 Days': [$moment().subtract(6, 'days'), $moment()],
        'Last 30 Days': [$moment().subtract(29, 'days'), $moment()],
        'This Month': [$moment().startOf('month'), $moment().endOf('month')],
        'Last Month': [$moment().subtract(1, 'month').startOf('month'), $moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'left',
      startDate: $moment().startOf('month'),
      endDate: $moment(),
      parentEl: '#content'
    };
    $scope.$watch("[startDate, endDate]", function(newValue, oldValue) {
    	$actionService.filterByDateForTrace(newValue, function(response) {
            if($tablehandler.getVM().dtInstance.id) {
                $tablehandler.getVM().dtInstance.rerender();
            }
        });
    });
  })
  .controller('TraceTableCtrl', function ($scope, $http, $state, $location, DTOptionsBuilder, DTColumnBuilder, DTInstanceFactory, DTRenderer, $localStorage, $authService, $tablehandler, $utilService, BASE_URL, md5, $stateParams) {
    var vm = this;
    var currentUser = $localStorage.currentUser;
    vm.message = '';
      function rowCallback(nRow, aData) {
          angular.element('td', nRow).find('a[callback="showTransaction"]').unbind('click');
          angular.element('td', nRow).find('a[callback="showTransaction"]').bind('click', function() {
              $scope.$apply(function() {
                  vm.showTransaction(aData);
              });

              angular.element('.row_selected').removeClass('row_selected');
              angular.element(nRow).addClass('row_selected');
          });
          return nRow;
      }
    vm.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
        url: BASE_URL + 'trace/list',
        type: 'GET',
        headers: {
            "Authorization": 'Guedia ' + $localStorage.token
        },
        error: function(xhr, error, thrown) {
            $utilService.locker(xhr);
        }
    })
        .withDataProp(function(json) {
            return json.data;
        })
        .withBootstrap()
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withOption('responsive', true)
        .withPaginationType('full_numbers')
        .withDOM('lfrtip')
        .withOption('bInfo', true)
        .withOption('order', [2, 'desc'])
        // Activate col reorder plugin
        .withColReorder()
        .withColReorderCallback(function() {
            //console.log('Columns order has been changed with: ' + this.fnOrder());
        })
        .withOption('rowCallback', rowCallback)
        .withOption('lengthMenu', [[10, 25, 50, 100, 1000], [10, 25, 50, 100, "All"]]);

    vm.dtColumns = [
	      DTColumnBuilder.newColumn('reference').withTitle('Reference').notSortable(),
	      DTColumnBuilder.newColumn('user').withTitle('User').notSortable(),
	      DTColumnBuilder.newColumn('date').withTitle('Date'),
	      DTColumnBuilder.newColumn('status').withTitle('Status').withOption('width', '70px').notSortable()
    ];
    vm.dtInstance = DTInstanceFactory.newDTInstance();
      function showTransaction(transaction) {
          $state.go('app.trace.details', { token: md5.createHash(currentUser.salt + transaction.transactionId) });
      }

      vm.showTransaction = showTransaction;
    $tablehandler.setVM(vm);
  })
    .controller('ShowTraceCtrl', function ($scope, $stateParams, $localStorage, $actionService, $utilService, $state, md5) {
        $scope.page = {
            title: 'Details',
            subtitle: "Trace details"
        };
        $scope.colors = {1: "warning", 2: "success", 3: "success", 4: "danger"};

        $actionService.showTransaction($stateParams.token, function (response) {
            $scope.transaction = response.data;
        });

        $scope.confirm = function() {
            $actionService.confirmTransaction(md5.createHash($localStorage.currentUser.salt + $scope.transaction.id), function(response) {
                $scope.transaction = response.data;
                $utilService.notify($scope, '#content', response.message);
            });
        };

        $scope.sendMail = function() {
            $utilService.change(function(response) {
                $scope.cadtoxof = response.CADtoXOF;
                $scope.changefees = parseInt($localStorage.fees.classic.rate.change) / 100;
                $scope.exchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));
                $actionService.sendTransaction(md5.createHash($localStorage.currentUser.salt + $scope.transaction.id), $scope.exchangerate, function(response) {
                    $utilService.notify($scope, '#content', response.message);
                });
            });

        };

        $scope.reject = function() {
            $actionService.rejectTransaction(md5.createHash($localStorage.currentUser.salt + $scope.transaction.id), function(response) {
                $scope.transaction = response.data;
                $utilService.notify($scope, '#content', response.message);
            });
        };

        $utilService.change(function(response) {
            $scope.cadtoxof = response.CADtoXOF;
            $scope.changefees = parseInt($localStorage.fees.classic.rate.change) / 100;
            $scope.exchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));
        });
    });
