'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:SimCtrl
 * @description
 * # SimCtrl
 * Controller of the minovateApp
 */
app
  .controller('SimCtrl', function ($scope, $location, USER_ROLE, $localStorage) {
    $scope.page = {
      title: 'List',
      subtitle: "Sim Card's list"
    };

      if ($localStorage.currentUser.role !== USER_ROLE.ROLE_ADMIN){
          $location.path('app/dashboard');
      }

  })

  .controller('SimTableCtrl', function ($scope, $http, $state, $location, DTOptionsBuilder, DTColumnBuilder, DTInstanceFactory, DTRenderer, $localStorage, $actionService, BASE_URL, md5) {
      var vm = this;
      var currentUser = $localStorage.currentUser;
      vm.message = '';
      function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
          angular.element('td', nRow).find('a[callback="editSim"]').unbind('click');
          angular.element('td', nRow).find('a[callback="editSim"]').bind('click', function() {
              $scope.$apply(function() {
                  vm.editSim(aData);
              });
              angular.element('.row_selected').removeClass('row_selected');
              angular.element(nRow).addClass('row_selected');
          });
          angular.element('td', nRow).find('a[callback="statusSim"]').bind('click', function() {
              $scope.$apply(function() {
                  vm.changeStatusSim(aData);
              });
              angular.element('.row_selected').removeClass('row_selected');
              angular.element(nRow).addClass('row_selected');
          });
          return nRow;
      }
      vm.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
          url: BASE_URL + 'sim/list',
          type: 'GET',
          headers: {
              "Authorization": 'Guedia ' + $localStorage.token
          },
          error: function(xhr, error, thrown) {
              $utilService.locker(xhr);
          }
      })
      .withDataProp(function(json){
          //console.log(json);
          return json.data;
      })
      .withBootstrap()
      .withOption('processing', true)
      .withOption('serverSide', true)
      .withOption('responsive', true)
      .withPaginationType('full_numbers')
      .withDOM('lfrtip')
      .withOption('bInfo', true)
      // Activate col reorder plugin
      .withColReorder()
      .withColReorderCallback(function() {
          console.log('Columns order has been changed with: ' + this.fnOrder());
      })
      .withOption('lengthMenu', [[10, 25, 50, 100, 10000], [10, 25, 50, 100, "All"]])
      .withOption('rowCallback', rowCallback);

      vm.dtColumns = [
	          DTColumnBuilder.newColumn('phonenumber').withTitle('Phone number').notSortable(),
	          DTColumnBuilder.newColumn('country').withTitle('Country').withOption('width', '40%').notSortable(),
	          DTColumnBuilder.newColumn('phonecode').withTitle('Phone code').notSortable(),
	          //DTColumnBuilder.newColumn('status').withTitle('Status').withOption('width', '70px').notSortable(),
	          DTColumnBuilder.newColumn('action').withTitle('Action').withOption('width', '170px').notSortable()
	      ];
      vm.dtInstance = DTInstanceFactory.newDTInstance();

      function editSim(sim) {
          console.log(sim);
          $state.go('app.sim.edit', { token: md5.createHash(currentUser.salt + sim.id) });
      }

      function changeStatusSim(sim) {
          $actionService.changeStatusSim(sim.id, function (response) {
              if (response.success === true) {
                  vm.dtInstance.rerender();
              } else {
                  alert(response.message);
              }
          });
      }
      vm.editSim = editSim;
      vm.changeStatusSim = changeStatusSim;

  })
  .controller('SimAddCtrl', function ($scope, $state, $stateParams, $selectService, $actionService, $utilService) {
	  $scope.page = {
		  title: 'Edition',
	      subtitle: "New Sim Card"
	  };

	  $selectService.getCountry(function (response) { $scope.arrCountry = response; });
      $scope.form = {};
	  $scope.create = function() {
          $scope.myform = {};
          $scope.myform.pays = $scope.form.pays;
          $scope.myform.telephone = $scope.form.telephone;
          $scope.myform.motdepasse = $utilService.crypt($scope.form.motdepasse.toString());

          $actionService.createSim($scope.myform, function (response) {
			  if (response.success === true) {
				  $state.go('app.sim.list');
			  } else {
				  $scope.error = !response.success;
				  $scope.message = response.message;
				  $scope.formError = response.errors;
			  }
		  });
	  };
  })
  .controller('SimEditCtrl', function ($scope, $state, $stateParams, $selectService, $actionService, $utilService) {
      $scope.page = {
          title: 'Edition',
          subtitle: "Edit Sim Card"
      };

      $selectService.getCountry(function (response) { $scope.arrCountry = response; });

      $scope.myform, $scope.form = {};

      function showSim() {
    	  $actionService.showSim($stateParams.token, function (response) {
              if (response.success === true) {
                  $scope.myform = response.data;
                  $scope.form.pays = $scope.myform.pays;
                  $scope.form.telephone = $scope.myform.telephone;
                  $scope.form.motdepasse = parseInt($utilService.decrypt($scope.myform.motdepasse));
              } else {
                  $scope.error = response.success;
                  $scope.message = response.message;
                  $state.go('app.sim.list');
              }
          });
      }

      showSim();

      $scope.update = function() {
          $scope.myform = {};
          $scope.myform.pays = $scope.form.pays;
          $scope.myform.telephone = $scope.form.telephone;
          $scope.myform.motdepasse = $utilService.crypt($scope.form.motdepasse);
          console.log($scope.form.motdepasse);
          console.log($utilService.crypt($scope.form.motdepasse));
    	  $actionService.updateSim($stateParams.token, $scope.myform, function (response) {
    	      //console.log($scope.myform);
              //console.log(response);
              if (response.success === true) {
                  $state.go('app.sim.list');
              } else {
                  $scope.error = !response.success;
                  $scope.message = response.message;
                  $scope.formError = response.errors;
              }
          });
      };

  });

