'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the minovateApp
 */
app
  .controller('UserCtrl', function ($scope, $state, $localStorage, USER_ROLE) {
  	if($localStorage.currentUser.role !== USER_ROLE.ROLE_ADMIN){
  		$state.go('app.dashboard');
	}

    $scope.page = {
      title: 'List',
      subtitle: "User's list"
    };

  })
  .controller('UserTableCtrl', function ($scope, $http, $state, $location, DTOptionsBuilder, DTColumnBuilder, DTInstanceFactory, DTRenderer, $localStorage, $authService, BASE_URL, md5, $utilService) {
	  var vm = this;
	  var currentUser = $localStorage.currentUser;
	  vm.message = '';
	  function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
	      // Unbind first in order to avoid any duplicate handler (see https://github.com/l-lin/angular-datatables/issues/87)
	      angular.element('td', nRow).find('a[callback="editUser"]').unbind('click');
	      angular.element('td', nRow).find('a[callback="statusUser"]').unbind('click');
	      angular.element('td', nRow).find('a[callback="editUser"]').bind('click', function() {
		        $scope.$apply(function() {
		          vm.editUser(aData);
		        });
		        angular.element('.row_selected').removeClass('row_selected');
		        angular.element(nRow).addClass('row_selected');
		      });
	      angular.element('td', nRow).find('a[callback="statusUser"]').bind('click', function() {
		        $scope.$apply(function() {
		          vm.changeStatusUser(aData);
		        });
		        angular.element('.row_selected').removeClass('row_selected');
		        angular.element(nRow).addClass('row_selected');
		      });
	      return nRow;
	  }


	  //vm.dtOptions = {searching:true};

	  vm.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
	        url: BASE_URL + 'user/list',
	        type: 'GET',
	        headers: {
	            "Authorization": 'Guedia ' + $localStorage.token
	        },
		    error: function(xhr, error, thrown) {
				$utilService.locker(xhr);
			}
	      })
		  .withDataProp(function(json){
			  return json.data;
		  })
		  .withBootstrap()
		  .withOption('processing', true)
		  .withOption('serverSide', true)
		  .withOption('responsive', true)
		  .withPaginationType('full_numbers')
		  .withDOM('lfrtip')
		  .withOption('bInfo', true)
		  // Activate col reorder plugin
		  .withColReorder()
		  .withColReorderCallback(function() {
			  console.log('Columns order has been changed with: ' + this.fnOrder());
		  })
		  .withOption('lengthMenu', [[10, 25, 50, 100, 10000], [10, 25, 50, 100, "All"]])
		  .withOption('rowCallback', rowCallback);
	  vm.dtColumns = [
	      DTColumnBuilder.newColumn('firstname').withTitle('First name').withOption('searchable', true),
	      DTColumnBuilder.newColumn('lastname').withTitle('Last name'),
	      DTColumnBuilder.newColumn('email').withTitle('E-mail'),
	      DTColumnBuilder.newColumn('profil').withTitle('Profil'),
	      DTColumnBuilder.newColumn('status').withTitle('Status').notSortable(),
		  DTColumnBuilder.newColumn('action').withTitle('Action').withOption('width', '170px').notSortable()
	  ];
	  vm.dtInstance = DTInstanceFactory.newDTInstance();



	  function editUser(user) {
	  	$state.go('app.user.edit', { token: md5.createHash(currentUser.salt + user.id) });
	  }


	  function changeStatusUser(user) {
		  $authService.changeStatus(user.id, function (response) {
			  if (response.success === true) {
				  vm.dtInstance.rerender();
			  } else {
				  alert(response.message);
			  }
		  });
	  }

	  $scope.reloadData = function() {
		  $scope.dtInstance.rerender();
	  };

	  vm.editUser = editUser;
	  vm.changeStatusUser = changeStatusUser;

  })
  .controller('UserEditCtrl', function ($scope, $state, $http, $stateParams, $authService, $selectService) {
	  $scope.page = {
			  title: 'Edition',
    	      subtitle: "User's edit"
    	  };

      $selectService.getSim(function (response) { $scope.arrPuce = response; });
      
	  function showUser() {
		  $authService.showUser($stateParams.token, function (response) {
			  if (response.success === true) {
				  $scope.form = response.data;
			  } else {
				  $scope.error = response.success;
				  $scope.message = response.message;
				  $state.go('app.user.list');
			  }
		  });
	  }

	  showUser();

	  $scope.update = function() {
		  $authService.update($stateParams.token, $scope.form, function (response) {
		  	console.log($scope.form);
			  console.log(response);
			  if (response.success === true) {
				  $state.go('app.user.list');
			  } else {
				  $scope.error = !response.success;
				  $scope.message = response.message;
	              $scope.formError = response.errors;
			  }
		  });
	  };

  });
