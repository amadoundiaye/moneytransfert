'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the minovateApp
 */
app.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
      }
  })
  .controller('DashboardCtrl', function($scope, $actionService, $moment, $synthesis, $utilService, USER_ROLE, $localStorage) {
    $scope.page = {
      title: 'Dashboard',
      subtitle: ''
    };
    $scope.currentUser = $localStorage.currentUser;
    $scope.user_role = USER_ROLE;
    $scope.numberChart, $scope.evolutionChart, $scope.countryChart, $scope.actuelChart = 0;
    $scope.startDate = $moment().startOf('month').format('MMMM D, YYYY');
    $scope.endDate = $moment().format('MMMM D, YYYY');
    $scope.rangeOptions = {
      ranges: {
        Today: [$moment(), $moment()],
        Yesterday: [$moment().subtract(1, 'days'), $moment().subtract(1, 'days')],
        'Last 7 Days': [$moment().subtract(6, 'days'), $moment()],
        'Last 30 Days': [$moment().subtract(29, 'days'), $moment()],
        'This Month': [$moment().startOf('month'), $moment().endOf('month')],
        'Last Month': [$moment().subtract(1, 'month').startOf('month'), $moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'left',
      startDate: $moment().startOf('month'),
      endDate: $moment(),
      parentEl: '#content'
    };
    $scope.$watch("[startDate, endDate]", function(newValue, oldValue) {
    	$scope.numberChart = 0;
    	$scope.evolutionChart = 0;
    	$scope.actualChart = 0;
    	$scope.countryChart = 0;
    	$scope.cashierChart = 0;
    	$actionService.filterByDate(newValue, function(response) {
    		if(response.success === true) {
		    	$synthesis.setSynthesePerNumber();
		    	$synthesis.setCommissionPerCountry([]);
		    	$synthesis.setCommissionAndAmountPerMonth([]);
		    	$synthesis.setCommissionAndAmountPerCashier([]);
		    	$synthesis.setEvolutionInOneMonth([]);
		    	$scope.numberChart = 1;
		    	$scope.evolutionChart = 1;
		    	$scope.actualChart = 1;
		    	$scope.countryChart = 1;
		    	$scope.cashierChart = 1;
    		}
    	})
    });
    $actionService.showSettings(function (response) { $localStorage.fees = response;});
    $scope.$watch(function () { return $synthesis.getSynthesePerNumber(); }, function (newValue, oldValue) {
    	$scope.number = newValue;
	});
    $scope.$watch(function () { return $synthesis.getCommissionPerCountry(); }, function (newValue, oldValue) {
    	$scope.commission = newValue;
	});
    $scope.$watch(function () { return $synthesis.getCommissionAndAmountPerCashier(); }, function (newValue, oldValue) {
    	$scope.cashiers = newValue;
	});

  })

  .controller('StatisticsChartCtrl', function ($scope, $synthesis, $localStorage) {
    $scope.dataset = [{
      data: [],
      label: 'Commission',
      points: {
        show: true,
        radius: 4
      },
      splines: {
        show: true,
        tension: 0.45,
        lineWidth: 4,
        fill: 0
      }
    }, {
      data: [],
      label: 'Amount',
      bars: {
        show: true,
        barWidth: 0.6,
        lineWidth: 0,
        fillColor: { colors: [{ opacity: 0.3 }, { opacity: 0.8}] }
      }
    }];

    $scope.options = {
      colors: ['#e05d6f','#61c8b8'],
      series: {
        shadowSize: 0
      },
      legend: {
        backgroundOpacity: 0,
        margin: -7,
        position: 'ne',
        noColumns: 2
      },
      xaxis: {
        tickLength: 0,
        font: {
          color: '#fff'
        },
        position: 'bottom',
        ticks: []
      },
      yaxis: {
        tickLength: 0,
        font: {
          color: '#fff'
        }
      },
      grid: {
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 1,
          left: 1
        },
        borderColor: 'rgba(255,255,255,.3)',
        margin:0,
        minBorderMargin:0,
        labelMargin:20,
        hoverable: true,
        clickable: true,
        mouseActiveRadius:6
      },
      tooltip: true,
      tooltipOpts: {
        content: '%s: %y',
        defaultTheme: false,
        shifts: {
          x: 0,
          y: 20
        }
      }
    };

      $scope.currentUser = $localStorage.currentUser;

    $scope.$watch(function () { return $synthesis.getCommissionAndAmountPerMonth(); }, function (newValue, oldValue) {
    	$scope.dataset[0].data = newValue['commission'];
    	$scope.dataset[1].data = newValue['amount'];
    	$scope.options.xaxis.ticks = newValue['month'];
	});


  })
  .controller('ActualStatisticsCtrl',function($scope, $synthesis, $localStorage) {
    $scope.easypiechart = {
      percent: 100,
      options: {
        animate: {
          duration: 3000,
          enabled: true
        },
        barColor: '#418bca',
        scaleColor: false,
        lineCap: 'round',
        size: 140,
        lineWidth: 4
      }
    };
    $scope.easypiechart2 = {
      percent: 75,
      options: {
        animate: {
          duration: 3000,
          enabled: true
        },
        barColor: '#e05d6f',
        scaleColor: false,
        lineCap: 'round',
        size: 140,
        lineWidth: 4
      }
    };
    $scope.easypiechart3 = {
      percent: 46,
      options: {
        animate: {
          duration: 3000,
          enabled: true
        },
        barColor: '#16a085',
        scaleColor: false,
        lineCap: 'round',
        size: 140,
        lineWidth: 4
      }
    };

      $scope.currentUser = $localStorage.currentUser;

    $scope.$watch(function () { return $synthesis.getEvolutionInOneMonth(); }, function (newValue, oldValue) {
    	$scope.easypiechart.percent = 100;
    	$scope.easypiechart2.percent = 75;
    	$scope.easypiechart3.percent = 46;
    	$scope.evolution = newValue;
	});
  })
  .factory('$synthesis', function ($actionService, $utilService) {
	  var arrData = {
			  synthesePerNumber: [],
			  commissionPerCountry: [],
			  commissionAndAmountPerMonth: [],
			  evolutionInOneMonth: []
	  	};
	  var service = {};

	  service.setSynthesePerNumber = function() {
		  $actionService.syntheseNumber(function(response) {
			  if(response.success === true) {
				  arrData['synthesePerNumber'] = response.data;
			  } else {
				  arrData['synthesePerNumber'] = {};
			  }
		  });
	  };
	  
	  service.getSynthesePerNumber = function(key) {
		  return (key && $utilService.isset(arrData['synthesePerNumber'][key])) ? arrData['synthesePerNumber'][key] : arrData['synthesePerNumber'];
	  };

	  service.setCommissionPerCountry = function() {
		  $actionService.commissionPerCountry(function(response) {
			  if(response.success === true) {
				  arrData['commissionPerCountry'] = response.data;
			  } else {
				  arrData['commissionPerCountry'] = [];
			  }
		  });
	  };
	  
	  service.getCommissionPerCountry = function(key) {
		  return (key && $utilService.isset(arrData['commissionPerCountry'][key])) ? arrData['commissionPerCountry'][key] : arrData['commissionPerCountry'];
	  };

	  service.setCommissionAndAmountPerMonth = function() {
		  $actionService.commissionAndAmountPerMonth(function(response) {
			  if(response.success === true) {
				  arrData['commissionAndAmountPerMonth'] = response.data;
			  } else {
				  arrData['commissionAndAmountPerMonth'] = {month: [], amount: [], commission: []};
			  }
		  });
	  };
	  
	  service.getCommissionAndAmountPerMonth = function(key) {
		  return (key && $utilService.isset(arrData['commissionAndAmountPerMonth'][key])) ? arrData['commissionAndAmountPerMonth'][key] : arrData['commissionAndAmountPerMonth'];
	  };

	  service.setEvolutionInOneMonth = function() {
		  $actionService.evolutionInOneMonth(function(response) {
			  if(response.success === true) {
				  arrData['evolutionInOneMonth'] = response.data;
			  } else {
				  arrData['evolutionInOneMonth'] = {amount: [], commission: [], customer: [], cashier: []};
			  }
		  });
	  };
	  
	  service.getEvolutionInOneMonth = function(key) {
		  return (key && $utilService.isset(arrData['evolutionInOneMonth'][key])) ? arrData['evolutionInOneMonth'][key] : arrData['evolutionInOneMonth'];
	  };

	  service.setCommissionAndAmountPerCashier = function() {
		  $actionService.commissionAndAmountPerCashier(function(response) {
			  if(response.success === true) {
				  arrData['commissionAndAmountPerCashier'] = response.data;
			  } else {
				  arrData['commissionAndAmountPerCashier'] = [];
			  }
		  });
	  };
	  
	  service.getCommissionAndAmountPerCashier = function() {
		  return arrData['commissionAndAmountPerCashier'];
	  };
	  return service;
  });

