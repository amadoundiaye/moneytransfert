'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:CommissionsCtrl
 * @description
 * # SecurityCtrl
 * Controller of the minovateApp
 */
app
.controller('SignupCtrl', function ($scope, $localStorage, $utilService, $authService, $selectService, $state) {
	$scope.page = {
			title: 'New account',
			subtitle: "User's registration"
	  	};
	  $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];

    $scope.currentUser = $localStorage.currentUser;

    $scope.form = {};
    $scope.form.puce = [];
    $scope.form.plainPassword = {};
    $scope.form.plainPassword.first = $utilService.randomPassword(10);

    $selectService.getSim(function (response) { $scope.arrPuce = response; });

    $scope.signUp = function() {
  	  $scope.form.plainPassword.second = $scope.form.plainPassword.first;
        $authService.signUp($scope.form, function (result) {
            if (result.success === true) {
                $scope.form = {};
                $state.go('user.list');
            }
            $scope.error = !result.success;
            $scope.message = result.message;
            $scope.formError = result.errors;
        });
    }
})
.controller('ForgotPasswordCtrl', function ($scope, $localStorage, $authService, $utilService) {
    $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];

      $scope.currentUser = $localStorage.currentUser;
      $scope.form = {};
      $scope.forgotPassword = function() {

          $authService.forgotPassword($scope.form.email, $utilService.randomPassword(10), function (result) {
              if (result.success === true) {
                  setTimeout(function () {
                      $authService.logout();
                  }, 2000);
              }
              $scope.error = !result.success;
              $scope.message = result.message;
          });
      }
})
.controller('LoginCtrl', function ($scope, $state, $location, $localStorage, $authService, $actionService, $utilService, $http) {
    if($utilService.isset($localStorage.currentUser)){
    }

    if($localStorage.currentUser && !$localStorage.currentUser.locked) {
        $state.go('app.dashboard');
    } else {
        $scope.login = login;
        initController();
    }
    function initController() {
        // reset login status
        $authService.logout();
    }
    function login() {
        $authService.login($scope.user.email, $scope.user.password, function (response) {
            if(response == null) {
            } else if(response.success === true) {
                $localStorage.currentUser = response.data;
                $localStorage.parameters = response.parameters;
                $localStorage.token = response.token;
                $actionService.showSettings(function (response) { $localStorage.fees = response;});
                $http.defaults.headers.common.Authorization = 'Guedia ' + response.token;
                $state.go('app.dashboard');
            } else {
                  $scope.error = response.success;
                  $scope.message = response.message;
            }
        });
    }
})
.controller('UnlockCtrl', function ($scope, $state, $location, $localStorage, $actionService, $http, $authService) {

    $actionService.showSettings(function (response) { $localStorage.fees = response;});

	$scope.unlock = function() {
		$authService.unlock($localStorage.currentUser.username, $scope.user.password, function (response) {
			if (response.success === true) {
                $actionService.showSettings(function (response) { $localStorage.fees = response;});
				$localStorage.currentUser = response.data;
				$localStorage.token = response.token;
				$http.defaults.headers.common.Authorization = 'Guedia ' + response.token;
				$state.go('app.dashboard');
			} else {
				$scope.error = response.success;
				$scope.message = response.message;
			}
		});
	};
})
.controller('ChangePasswordCtrl', function ($scope, $state, $localStorage, $authService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.page = {
        title: 'Change Password Page',
        subtitle: ''
    };
    $scope.currentUser = $localStorage.currentUser;
    $scope.form = {};
    $scope.changePassword = function() {
        $authService.changePassword($scope.form, function (result) {
           if (result.success === true) {
               setTimeout(function () {
                   $authService.logout();
               }, 2000);
           }
           $scope.error = !result.success;
           $scope.message = result.message;
        });
    }
})
.controller('ProfileCtrl', function ($scope, $localStorage, $handleObject, $authService) {
    $scope.page = {
      title: 'Profile Page',
      subtitle: ''
    };
    $scope.currentUser = $localStorage.currentUser;
    $scope.editUser = $handleObject.copyOnlyAttributes($scope.currentUser, ['firstname', 'lastname', 'address', 'phone', 'email', 'username', 'current_password']);
    $scope.update = function () {
        $authService.updateProfil($localStorage.token, $scope.editUser, function (result) {
            if (result.success === true) {
            	$scope.currentUser = angular.merge($scope.currentUser, $handleObject.copyOnlyAttributes($scope.editUser, ['firstname', 'lastname', 'address', 'phone', 'username']));
            	$localStorage.currentUser = $scope.currentUser;
            }
            $scope.error = !result.success;
            $scope.message = result.message;
        });
    }
});

