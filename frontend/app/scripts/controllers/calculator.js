'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:CalculatorCtrl
 * @description
 * # CalculatorCtrl
 * Controller of the minovateApp
 */
app
    .controller('CalculatorCtrl', function ($scope, $utilService, $actionService, $selectService, $localStorage, USER_ROLE) {
        $actionService.showSettings(function (response) { $localStorage.fees = response;});
        //$actionService.getFee(100, function (response) { if(response.success){console.log(response.tarif);}});

        $scope.page = {
            title: 'Fees Calculator',
            subtitle: ""
        };

        $scope.currentUser = $localStorage.currentUser;
        $scope.user_role = USER_ROLE;

        $selectService.getCountry(function (response) { $scope.arrCountry = response; });
        if($utilService.isset($localStorage.fees) || $utilService.isset($localStorage.fees.classic)){
            $actionService.showSettings(function (response) { $localStorage.fees = response;});
        }
        $scope.transfertfees = parseFloat($localStorage.fees.classic.rate.commission)/100;
        //$scope.tactransfertfees = parseInt($localStorage.fees.tac.rate.commission)/100;

        $scope.changefees = parseFloat($localStorage.fees.classic.rate.change)/100;
        $scope.form = {};

        $scope.CalculateClassic = function () {
            $utilService.change(function(response) {
                $scope.cadtoxof = response.CADtoXOF;
                var omfees = 0;
                $scope.form.omfees = omfees;
                $actionService.getFee(parseFloat($scope.form.amounttosend), function (response) {
                    $scope.form.mobiletransfertfees = parseFloat(response.tarif);
                    $scope.form.mobiletransfertfees = $utilService.normalizeCAD($scope.form.mobiletransfertfees);

                    $scope.form.transfertfees = parseFloat($scope.form.omfees) + parseFloat($scope.form.mobiletransfertfees);
                    $scope.form.transfertfees = $utilService.normalizeCAD($scope.form.transfertfees);

                    $scope.form.amouttopaid = parseFloat($scope.form.amounttosend) + parseFloat($scope.form.mobiletransfertfees);
                    $scope.form.amouttopaid = $utilService.normalizeCAD($scope.form.amouttopaid);
                    $scope.form.exchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));

                    $scope.form.omreceivedamount = $utilService.normalizeFCFA($scope.form.amounttosend * $scope.form.exchangerate);
                });

            });
        };

        $scope.CalculateClassicReceived = function () {
            $utilService.change(function(response) {
                $scope.cadtoxof = response.CADtoXOF;
                var omfees = 0;
                $scope.form.r_omfees = omfees;

                $scope.form.r_exchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));

                $scope.form.r_amounttosend = parseFloat($scope.form.r_omreceivedamount) / parseFloat($scope.form.r_exchangerate);
                $scope.form.r_amounttosend = $utilService.normalizeCAD($scope.form.r_amounttosend);

                $actionService.getFee(parseFloat($scope.form.r_amounttosend), function (response) {
                    $scope.form.r_mobiletransfertfees = parseFloat(response.tarif);
                    $scope.form.r_mobiletransfertfees = $utilService.normalizeCAD($scope.form.r_mobiletransfertfees);

                    $scope.form.r_transfertfees = parseFloat($scope.form.r_omfees) + parseFloat($scope.form.r_mobiletransfertfees);
                    $scope.form.r_transfertfees = $utilService.normalizeCAD($scope.form.r_transfertfees);

                    $scope.form.r_amouttopaid = parseFloat($scope.form.r_amounttosend) + parseFloat($scope.form.r_mobiletransfertfees);
                    $scope.form.r_amouttopaid = $utilService.normalizeCAD($scope.form.r_amouttopaid);
                });
            });
        };

        $scope.CalculateTac = function () {
            $utilService.change(function(response) {
                $scope.cadtoxof = response.CADtoXOF;

                $actionService.getFee(parseFloat($scope.form.tacamounttosend), function (response) {
                    $scope.form.tacmobiletransfertfees = parseFloat(response.tarif);

                    $scope.form.tacexchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100 / 100 - $scope.changefees));

                    $scope.form.tacomreceivedamount = $utilService.normalizeFCFA($scope.form.tacamounttosend * $scope.form.tacexchangerate);

                    $utilService.calculator(parseFloat($scope.form.tacomreceivedamount), function (response) {
                        console.log(response);
                        if (response.success === true) {
                            if (response.messages.length > 0) {
                                $scope.tacFees = response.messages[0].value;
                                console.log($scope.tacFees);
                                $scope.form.tacomfees = $scope.tacFees / $scope.form.tacexchangerate;
                                $scope.form.tacomfees = $utilService.normalizeCAD($scope.form.tacomfees);

                                $scope.form.tactransfertfees = parseFloat($scope.form.tacomfees) + parseFloat($scope.form.tacmobiletransfertfees);
                                $scope.form.tactransfertfees = $utilService.normalizeCAD($scope.form.tactransfertfees);

                                $scope.form.tacamouttopaid = parseFloat($scope.form.tacamounttosend) + parseFloat($scope.form.tactransfertfees);
                                $scope.form.tacamouttopaid = $utilService.normalizeCAD($scope.form.tacamouttopaid);
                            } else {
                                //$utilService.alert($scope, '#content', "Plafond dépassé (Maximum 200.000 FCFA).");
                            }
                        }
                    });
                });
            });
        };

        $scope.CalculateTacReceived = function () {
            $utilService.change(function(response) {
                $scope.cadtoxof = response.CADtoXOF;

                $scope.form.r_tacexchangerate = $utilService.normalizeEffectifRate($scope.cadtoxof * (100/100 - $scope.changefees));
                $scope.form.r_tacamounttosend = parseFloat($scope.form.r_tacomreceivedamount) / parseFloat($scope.form.r_tacexchangerate);
                $scope.form.r_tacamounttosend = $utilService.normalizeCAD($scope.form.r_tacamounttosend);

                $actionService.getFee(parseFloat($scope.form.r_tacamounttosend), function (response) {
                    $scope.form.r_tacmobiletransfertfees = parseFloat(response.tarif);
                    $scope.form.r_tacmobiletransfertfees = $utilService.normalizeCAD($scope.form.r_tacmobiletransfertfees);

                    $utilService.calculator(parseFloat($scope.form.r_tacomreceivedamount), function (response) {
                        console.log(response);
                        if (response.success === true) {
                            if (response.messages.length > 0) {
                                $scope.tacFees = response.messages[0].value;
                                $scope.form.r_tacomfees = $scope.tacFees / $scope.form.r_tacexchangerate;
                                $scope.form.r_tacomfees = $utilService.normalizeCAD($scope.form.r_tacomfees);

                                $scope.form.r_tactransfertfees = parseFloat($scope.form.r_tacomfees) + $scope.form.r_tacmobiletransfertfees;
                                $scope.form.r_tactransfertfees = $utilService.normalizeCAD($scope.form.r_tactransfertfees);

                                $scope.form.r_tacamouttopaid = $utilService.normalizeCAD($scope.form.r_tacamounttosend + $scope.form.r_tactransfertfees);
                                $scope.r_tacamoutfeescashout = parseFloat($scope.form.r_tacomfees) + parseFloat($scope.form.r_tacamounttosend)
                            } else {
                                //$utilService.alert($scope, '#content', "Plafond dépassé (Maximum 200.000 FCFA).");

                            }
                        }
                    });
                });
            });
        };
    });

