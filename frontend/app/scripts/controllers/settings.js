'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:CommissionsCtrl
 * @description
 * # CommissionsCtrl
 * Controller of the minovateApp
 */
app
  .controller('SettingsCtrl', function ($scope, $state, $actionService, $localStorage, md5, $location, USER_ROLE) {
    $scope.page = {
      title: 'Main Setting',
      subtitle: "Commissions & fees"
    };

      if ($localStorage.currentUser.role !== USER_ROLE.ROLE_ADMIN){
          $location.path('app/dashboard');
      }

    $actionService.showSettings(function (response) { $scope.settings = response; });
    var currentUser = $localStorage.currentUser;
    $scope.editConfig = function (id) {
        $state.go('app.settings.config_edit', {token: md5.createHash(currentUser.salt + id), id:id});
    };

  })
  .controller('FeesCtrl', function ($scope, $stateParams, $actionService, $localStorage, $state) {
      $scope.page = {title: 'Edition', subtitle: "Commissions & fees"};

      $actionService.showConfig($stateParams.token, function (response) {
          $scope.form = response;
      });

      $scope. updateSettings = function() {
		  $actionService.updateConfig($stateParams.token, $scope.form, function (response) {
              //console.log($scope.form);
	          if (response.success === true) {
	              $state.go('app.settings.details');
	          } else {
	              $scope.error = !response.success;
	              $scope.message = response.message;
	              $scope.formError = response.errors;
	          }
	      });
      };
  });
