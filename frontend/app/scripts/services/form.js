/**
 * Created by mac on 21/05/2017.
 */

angular
    .module('minovateApp')
    .factory('$handleForm', function () {
        var methods = {};
        methods.showErrorByField = function (objectToCopy, attributes) {
        	var object = {};
        	angular.forEach(attributes, function(value, key) {
        		  this[value] = objectToCopy[value];
        		}, object);
            return object;
        };
        return methods;
    });

     
    