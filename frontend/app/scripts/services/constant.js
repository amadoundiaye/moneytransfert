/**
 * Created by mac on 22/05/2017.
 */


angular.module('minovateApp')
	.constant('USER_ROLE', {
		ROLE_ADMIN: 'ROLE_ADMIN',
		ROLE_SUPER_ADMIN: 'ROLE_SUPER_ADMIN',
		ROLE_CASHIER: 'ROLE_USER',
		ROLE_USER: 'ROLE_USER'
	})
	.constant('STATUS',{
		UNAUTHORIZED: 'Unauthorized'
	})
	.constant('DOWNLOAD_URL', 'http://vps423165.ovh.net/backend/web/download/')
	//.constant('DOWNLOAD_URL', 'http://127.0.0.1/softgal/wbs/web/download/')
	//.constant('BASE_URL', 'http://127.0.0.1/softgal/wbs/web/app_dev.php/api/')
	.constant('BASE_URL', 'http://vps423165.ovh.net/backend/web/app_dev.php/api/')
	.constant('UPLOAD_URL','http://vps423165.ovh.net/calculator/upload.php');
	//.constant('CHANGE_URL','http://vps423165.ovh.net/backend/web/currency.php');