/**
 * Created by mac on 21/05/2017.
 */

angular
    .module('minovateApp')
    .factory('$authService', function($http, $localStorage, $location, $state, BASE_URL, md5) {
        var service = {};

        service.login = function(username, password, callback) {
            $http.post(BASE_URL + 'login_check', { _username: username, _password: password })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.signUp = function(data, callback) {
            $http.post(BASE_URL + 'registration', { fos_user_registration_form: data })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.lock = function() {
            $localStorage.currentUser.locked = true;
            $location.path('/core/locked');

        };

        service.unlock  = function(username, password, callback) {
            $http.post(BASE_URL + 'login_check', { _username: username, _password: password })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.logout = function() {
            delete $localStorage.currentUser;
            delete $localStorage.token;
            delete $localStorage.fees;
            delete $localStorage.formCalcul;
            delete $localStorage.parameters;
            $location.path("/core/login");
        };

        service.updateProfil = function(token, editUser, callback) {
            $http.post(BASE_URL + 'profile/update', { fos_user_profile_form: editUser })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.changePassword = function(data, callback) {
            $http.post(BASE_URL + 'change-password', { fos_user_change_password_form: data })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.forgotPassword = function(email, password, callback) {
            $http.post(BASE_URL + 'forgot-password', { _password: password, _email: email })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.update = function(token, form, callback) {
        	delete(form.role);
            $http.post(BASE_URL + 'update_user/' + token, {guedia_user: form})
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.showUser = function(token, callback) {
            $http.post(BASE_URL + 'show_user/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.changeStatus = function ChangeStatus(id, callback) {
            $http.post(BASE_URL + 'change_status_user/' + md5.createHash($localStorage.currentUser.salt + id))
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };
        
        service.isAuthenticated = function () {
            return !$localStorage.currentUser;
        };
        
        service.isAuthorized = function (authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            return (this.isAuthenticated() && authorizedRoles.indexOf($localStorage.currentUser.role) !== -1);
        };
        
        return service;
    })
    .factory('$selectService', function($http, BASE_URL, $location) {
        var service = {};
        service.getCountry = function(callback) {
            $http({
                method: 'GET',
                url: BASE_URL + 'country/list_select'
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        };
        service.getSim = function(callback) {
            $http({
                method: 'GET',
                url: BASE_URL + 'sim/list_select'
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        };
        service.getTransferType = function(callback) {
            $http({
                method: 'GET',
                url: BASE_URL + 'transaction/list_type_transfer'
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        };
        service.getIdentityType = function(callback) {
            $http({
                method: 'GET',
                url: BASE_URL + 'transaction/list_identity_type'
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        };
        return service;
    })
    .factory('$tablehandler', function () {
    	  var vmTable = null;
    	  var service = {};

    	  service.setVM = function(vm) {
    		  vmTable = vm;
    	  };
    	  
    	  service.getVM = function() {
    		  return vmTable;
    	  };

    	  return service;
    })
    .factory('$actionService', function($http, $localStorage, BASE_URL, $location) {
    	var service = {};

    	service.resetFilter = function() {
    		$http.post(BASE_URL + 'reset_filter');
    	};
    	
    	service.filterByDate = function(criteria, callback) {
    		$http.post(BASE_URL + 'filter_by_date', criteria)
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	                callback(response.data);
	            });
    	};
    	
    	service.filterByDateForTrace = function(criteria, callback) {
    		$http.post(BASE_URL + 'filter_by_date_for_trace', criteria)
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	                callback(response.data);
	            });
    	};
    	
    	service.filterByDateForTransaction = function(criteria, callback) {
    		$http.post(BASE_URL + 'filter_by_date_for_transaction', criteria)
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	                callback(response.data);
	            });
    	};
    	
        service.showSettings = function(callback) {
            $http.post(BASE_URL + 'show_config')
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.updateSettings = function(token, callback) {
            $http.post(BASE_URL + 'show_config/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.createSim = function(data, callback) {
            $http.post(BASE_URL + 'sim/create', { sim: data })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.showSim = function(token, callback) {
            $http.post(BASE_URL + 'show_sim/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };
        
        service.getFee = function(amount, callback) {
            return $http({
                method: 'GET',
                url: BASE_URL + amount + '/fee_get'
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            })
        };

        service.contactus = function(data, callback) {
            $http.post(BASE_URL + 'contact_us', data)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.sendPromo = function(data, callback) {
            $http.post(BASE_URL + 'send_promo', data)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(-1);
                });
        };

        service.updateSim = function(token, form, callback) {
            $http.post(BASE_URL + 'update_sim/' + token, {sim: form})
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.changeStatusSim = function(id, callback) {
            $http.post(BASE_URL + 'change_status_sim/' + md5.createHash($localStorage.currentUser.salt + id))
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.showCountry = function(token, callback) {
            $http.post(BASE_URL + 'show_country/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.updateCountry = function(token, form, callback) {
            $http.post(BASE_URL + 'update_country/' + token, {country: form})
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.showTransaction = function(token, callback) {
            $http.get(BASE_URL + 'show_transaction/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.sendTransaction = function(token, exchangerate, callback) {
            $http.get(BASE_URL + 'send_transaction/' + token + '/' + exchangerate)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.searchCustomer = function(data, callback) {
            $http.post(BASE_URL + 'transaction/customer_by_identity', data)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };


        service.newTransaction = function(data, callback) {
            $http.post(BASE_URL + 'transaction/create', { depot: data })
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.exportTransaction = function(callback) {
            $http.get(BASE_URL + 'transaction/export')
                .then(function successCallback(response) {
                    callback(response);
                }, function errorCallback(response) {
                    callback(response);
                });
        };

        service.confirmTransaction = function(token, exchangerate, callback) {
            $http.get(BASE_URL + 'confirm_transaction/' + token + '/' + exchangerate)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.rejectTransaction = function(token, callback) {
            $http.get(BASE_URL + 'reject_transaction/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.showConfig = function(token, callback) {
            $http.get(BASE_URL + 'show_one_config/' + token)
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                    callback(response.data);
                });
        };

        service.updateConfig = function(token, form, callback) {
            $http.post(BASE_URL + 'update_config/' + token, {configuration: form})
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                });
        };
        
        service.syntheseNumber = function(callback) {
            $http.post(BASE_URL + 'synthese_per_number')
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	            });
        };
        
        service.commissionPerCountry = function(callback) {
            $http.post(BASE_URL + 'commission_per_country')
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	            });
        };
        
        service.commissionAndAmountPerMonth = function(callback) {
            $http.post(BASE_URL + 'commission_and_amount_per_month')
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	            });
        };
        
        service.commissionAndAmountPerCashier = function(callback) {
            $http.post(BASE_URL + 'commission_and_amount_per_cashier')
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	            });
        };
        
        service.evolutionInOneMonth = function(callback) {
            $http.post(BASE_URL + 'evolution_in_one_month')
	            .then(function successCallback(response) {
	                callback(response.data);
	            }, function errorCallback(response) {
	            });
        };

        return service;
    })
    .factory('$utilService', function($http, $localStorage, BASE_URL, $location, $mdToast, $mdDialog, $authService) {
        var service = {};

        var DEBUG = false;

        var Base64={
            _keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
            encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},
            decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}return t},
            _utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},
            _utf8_decode:function(e){var t="";var n=0;var c1=0;var c2=0;var c3=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}
        };

        service.isset = function (variable) {
            return typeof variable !== typeof undefined && variable !== null && variable !== "";
        };
        
        service.booleanToInteger = function (bool) {
            return bool ? 1 : 0;
        };
        
        service.log = function (message) {
            return DEBUG ? console.log(message) : "" ;
        };
        
        service.info = function (message){
            return DEBUG ? console.info(message) : "" ;
        };
        
        service.notify = function ($scope, selector, message) {
            var toast = $mdToast.simple()
	            .parent(angular.element(document.querySelector(selector)))
	            .content(message)
	            .action('OK')
	            .highlightAction(false)
	            .position($scope.getToastPosition());
            $mdToast.show(toast).then(function(response) {
            	if(response == 'ok') {
            		//todo
            	}
            });
        };

        service.alert = function(ev, selector, message) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector(selector)))
                    .clickOutsideToClose(true)
                    .title("Softgal")
                    .content(message)
                    .ariaLabel("Softgal")
                    .ok('Fermer')
                    .targetEvent(ev)
            );
        };

        service.locker = function (response) {
            if(typeof STATUS === typeof undefined){
                $localStorage.currentUser.locked = true;
                $authService.logout();
            }

            if(response.status == 401 && $localStorage.currentUser && $localStorage.currentUser.locked==false &&  response.statusText == STATUS.UNAUTHORIZED) {
                $localStorage.currentUser.locked = true;
                $authService.logout();
            }
        };
        
        service.randomPassword = function (length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$&*()-+ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        };
        
        service.crypt = function (message) {
            return Base64.encode(message.toString());
        };
        
        service.decrypt = function (encodedMessage) {
            return Base64.decode(encodedMessage);
        };

        service.dateFormat = function (date) {
            var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
            var year = date.getFullYear(),
                month = months[date.getMonth() + 1], // months are zero indexed
                day = date.getDate(),
                hour = date.getHours(),
                minute = date.getMinutes(),
                second = date.getSeconds(),
                hourFormatted = hour < 10 ? "0" + hour : hour,
                minuteFormatted = minute < 10 ? "0" + minute : minute,
                secondFormatted = second < 10 ? "0" + second : second;
                return day + " " + month + " " + year + " à " + hourFormatted + ":" + minuteFormatted + ":" + secondFormatted;
        };

        service.trxRefeference = function (len, an){
            an = an&&an.toLowerCase();
            var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
            for(;i++<len;){
                var r = Math.random()*(max-min)+min <<0;
                str += String.fromCharCode(r+=r>9?r<36?55:61:48);
            }
            return str;
        };

        service.checkToken = function(callback) {
            $http.post(BASE_URL + 'token_check')
                .then(function successCallback(response) {
                    callback(response);
                }, function errorCallback(response) {
                    callback(response);
                });
        };

        service.calculator = function(sum, callback) {
            $http.post(BASE_URL + 'calculator/' + sum + '/withdrawal_fee')
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                });
        };

        service.change = function(callback) {
            $http.post(BASE_URL + 'calculator/change')
                .then(function successCallback(response) {
                    callback(response.data);
                }, function errorCallback(response) {
                });
        };

        service.isInt = function isInt(x) {
            return !isNaN(x) && eval(x).toString().length == parseInt(eval(x)).toString().length
        };

        service.isFloat = function isFloat(x) {
            return !isNaN(x) && !this.isInt(eval(x)) && x.toString().length > 0
        };

        service.normalizeFCFA = function(value){
            var sum = value.toFixed(0);
            var lastChar = sum.substr(sum.length - 1);
            return sum - lastChar;
        };

        service.normalizeCAD = function(value){
            if(this.isFloat(value)){
                var sum = value.toFixed(2);
                var tabSum = sum.split(".");
                if(tabSum[1].length == 2){
                    var entier = parseInt(tabSum[0]);
                    var dizaine = parseInt(tabSum[1].substr(0,1));
                    var centaine = parseInt(tabSum[1].substr(1,1));
                    if(dizaine == 9){
                        if(centaine < (5/2)){
                            centaine = 0;
                        }else if(centaine >= (5/2) && centaine < 5){
                            centaine = 5;
                        }else if (centaine > 5){
                            centaine = 0;
                            dizaine = 0;
                            entier = entier + 1;
                        }
                    }else {
                        if(centaine < (5/2)){
                            centaine = 0;
                        }else if(centaine >= (5/2) && centaine < 5){
                            centaine = 5;
                        }else if (centaine > 5){
                            centaine = 0;
                            dizaine = dizaine + 1;
                        }
                    }
                    var new_sum = entier+"."+dizaine+""+centaine;
                    return parseFloat(new_sum);
                }
                return sum;
            }
            return value;
        };

        service.normalizeEffectifRate = function(value){
            var sum = value.toFixed(3);
            return sum;
        };


        return service;
    });

     
    