'use strict';

/**
 * @ngdoc filter
 * @name minovateApp.filter:rounded
 * @description
 * # rounded
 */
app
    .filter("rounded",function(){
        return function(val){
            return val.toFixed(0);
        }
    });