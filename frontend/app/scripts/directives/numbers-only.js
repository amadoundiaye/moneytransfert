'use strict';

/**
 * @ngdoc directive
 * @name minovateApp.directive:setNgAnimate
 * @description
 * # setNgAnimate
 */
app
    .directive('numbersOnly', function () {
        return {
            require: '?ngModel',
            restrict: 'A',
            link: function (scope, elm, attr, ctrl) {
                if (!ctrl) {
                    return;
                }
                function inputValue(val) {
                    if (val) {
                        var digits = val.replace(/[^0-9.]/g, '');

                        if (digits.split('.').length > 2) {
                            digits = digits.substring(0, digits.length - 1);
                        }

                        if (digits !== val) {
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseFloat(digits);
                    }
                    return undefined;
                }
                ctrl.$parsers.push(inputValue);
            }
        };
    })
    .directive("limitTo", [function() {
        return {
            restrict: "A",
            link: function(scope, elem, attrs) {
                var limit = parseInt(attrs.limitTo);
                angular.element(elem).on("keypress", function(e) {
                    if (this.value.length == limit && this.value <= 100 && this.value >= 0) e.preventDefault();
                });
            }
        }
    }]);