'use strict';

/**
 * @ngdoc overview
 * @name minovateApp
 * @description
 * # minovateApp
 *
 * Main module of the application.
 */

  /*jshint -W079 */

var app = angular
  .module('minovateApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    //'ngTouch',
    'ngMessages',
    'picardy.fontawesome',
    'ui.bootstrap',
    'ui.router',
    'ui.utils',
    'angular-loading-bar',
    'angular-momentjs',
    'FBAngular',
    'lazyModel',
    'toastr',
    'angularBootstrapNavTree',
    'ngTable',
    'angularFileUpload',
    'oc.lazyLoad',
    'ui.select',
    'ui.tree',
    'textAngular',
    'colorpicker.module',
    'ngImgCrop',
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.edit',
    'ui.grid.moveColumns',
    'smart-table',
    'angular-flot',
    'angular-rickshaw',
    'easypiechart',
    'uiGmapgoogle-maps',
    'ui.calendar',
    'ngTagsInput',
    'pascalprecht.translate',
    'ngMaterial',
    'localytics.directives',
    'leaflet-directive',
    'wu.masonry',
    'ipsum',
    'angular-intro',
    'dragularModule',
    'ngStorage',
    'ngMd5',
    'datatables',
    'datatables.bootstrap',
    'datatables.colreorder',
    'datatables.colvis',
    'datatables.tabletools',
    'datatables.scroller',
    'datatables.columnfilter'
  ])
  .run(['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function(event, toState) {

      event.targetScope.$watch('$viewContentLoaded', function () {

        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);
        setTimeout(function () {
          angular.element('#wrap').css('visibility','visible');

          if (!angular.element('.dropdown').hasClass('open')) {
            angular.element('.dropdown').find('>ul').slideUp();
          }
        }, 200);
      });
      $rootScope.containerClass = toState.containerClass;

    });
  }])
  .run( function ($rootScope, $http, $location, $localStorage, $interval, $actionService, $utilService) {
        // keep user logged in after page refresh
        if($localStorage.currentUser) {
            $http.defaults.headers.common.Authorization = 'Guedia ' + $localStorage.token;
            $http.defaults.withCredentials = true;
        }
        var stop;
        // redirection vers page de login l'utilisateur nest pas connecté
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/core/login', '/core/forgotpass'];
            var restrictedPage = publicPages.indexOf($location.path()) === -1;
            if (restrictedPage && !$localStorage.currentUser) {
                $location.path('/core/login');
            } else if($utilService.isset($localStorage.currentUser)) {
              if($localStorage.currentUser.locked) {
                $location.path('/core/locked');
              }
            }
        });
        $actionService.resetFilter();
        stop = $interval(function() {
        	if($localStorage.currentUser && $localStorage.currentUser.locked == false) {
                $utilService.checkToken(function (response) {
                	$utilService.log(response);
                    if(response.status == 401) {
                        $localStorage.currentUser.locked = true;
                        $location.path('/core/locked');
                    }
                });
        	}
          }, 60000);
    })

  .config(['uiSelectConfig', function (uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
  }])

  //angular-language
  .config(['$translateProvider', function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
      prefix: 'languages/',
      suffix: '.json'
    });
    $translateProvider.useLocalStorage();
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy(null);
  }])

  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/core/login');

    $stateProvider
    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'views/app.html'
    })
    //dashboard
    .state('app.dashboard', {
      url: '/dashboard',
      controller: 'DashboardCtrl',
      templateUrl: 'views/dashboard.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load([
                'scripts/vendor/datatables/datatables.bootstrap.min.css',
                'scripts/vendor/datatables/datatables.bootstrap.min.css'
            ]);
        }]
      }
    })
    //profile
    .state('app.profile', {
        url: '/profile',
        controller: 'ProfileCtrl',
        templateUrl: 'views/pages/profile.html'
    })
    //forgot password
    .state('app.changepass', {
        url: '/changepass',
        controller: 'ChangePasswordCtrl',
        templateUrl: 'views/pages/changepass.html'
    })
    //transactions
    .state('app.transaction', {
        url: '/transaction',
        abstract: true,
        template: '<div ui-view></div>'
    })
    //transactions/list
    .state('app.transaction.list', {
        url: '/list',
        controller: 'TransactionCtrl',
        templateUrl: 'views/transaction/list.html',
        resolve: {
            plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'scripts/vendor/datatables/datatables.bootstrap.min.css',
                    'scripts/vendor/datatables/Pagination/input.js',
                    'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js',
                    'scripts/vendor/datatables/ColReorder/js/dataTables.colReorder.js'
                ]);
            }]
        }
    })
    //new transactions
    .state('app.transaction.new', {
        url: '/new',
        controller: 'NewTransactionCtrl',
        templateUrl: 'views/transaction/add.html'
    })
    //transaction show
    .state('app.transaction.details', {
        url: '/details/{token}',
        controller: 'ShowTransactionCtrl',
        templateUrl: 'views/transaction/show.html'
    })
    //calculator
    .state('app.calculator', {
        url: '/calculator',
        controller: 'CalculatorCtrl',
        templateUrl: 'views/calculator/calculator.html'
    })
    //app sim
    .state('app.settings', {
        url: '/settings',
        abstract: true,
        template: '<div ui-view></div>'
    })
    //new sim
    .state('app.settings.details', {
        url: '/details',
        controller: 'SettingsCtrl',
        templateUrl: 'views/settings/show.html'
    })
    //fee edit
    .state('app.settings.config_edit', {
        url: '/fee_edit/{token}',
        controller: 'FeesCtrl',
        templateUrl: 'views/settings/fee_edit.html'
    })
    //commission edit
    .state('app.settings.commission_edit', {
        url: '/commission_edit/{token}',
        controller: 'CommissionCtrl',
        templateUrl: 'views/settings/commission_edit.html'
    })
    .state('app.settings.upload', {
        url: '/upload',
        controller: 'FormUploadCtrl',
        templateUrl: 'views/settings/upload.html',
        resolve: {
            plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'scripts/vendor/filestyle/bootstrap-filestyle.min.js'
                ]);
            }]
        }
    })
    //app sim
    .state('app.sim', {
        url: '/sim',
        abstract: true,
        template: '<div ui-view></div>'
    })
    //sim/list
    .state('app.sim.list', {
        url: '/list',
        controller: 'SimCtrl',
        templateUrl: 'views/sim/list.html',
        resolve: {
            plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'scripts/vendor/datatables/datatables.bootstrap.min.css',
                    'scripts/vendor/datatables/Pagination/input.js',
                    'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
                ]);
            }]
        }
    })
    //new sim
    .state('app.sim.new', {
        url: '/new',
        controller: 'SimAddCtrl',
        templateUrl: 'views/sim/add.html'
    })
    //edit
    .state('app.sim.edit', {
        url: '/edit/{token}',
        controller: 'SimEditCtrl',
        templateUrl: 'views/sim/edit.html'
    })
    //app country
    .state('app.country', {
        url: '/country',
        abstract: true,
        template: '<div ui-view></div>'
    })
    //country/list
    .state('app.country.list', {
        url: '/list',
        controller: 'CountryCtrl',
        templateUrl: 'views/country/list.html',
        resolve: {
            plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    'scripts/vendor/datatables/datatables.bootstrap.min.css',
                    'scripts/vendor/datatables/Pagination/input.js',
                    'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
                ]);
            }]
        }
    })
    //new ussd
    .state('app.country.new', {
        url: '/new',
        controller: 'CountryAddCtrl',
        templateUrl: 'views/country/add.html'
    })
    //edit
    .state('app.country.edit', {
        url: '/edit/{token}',
        controller: 'CountryEditCtrl',
        templateUrl: 'views/country/edit.html'
    })
    //user
    .state('app.user', {
      url: '/user',
      abstract: true,
      template: '<div ui-view></div>'
    })
    //user/list
    .state('app.user.list', {
      url: '/list',
      controller: 'UserCtrl',
      templateUrl: 'views/user/list.html',
      resolve: {
          plugins: ['$ocLazyLoad', function($ocLazyLoad) {
              return $ocLazyLoad.load([
                  'scripts/vendor/datatables/datatables.bootstrap.min.css',
                  'scripts/vendor/datatables/Pagination/input.js',
                  'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
              ]);
          }]
        }
    })
    //signup
    .state('app.user.signup', {
      url: '/signup',
      controller: 'SignupCtrl',
      templateUrl: 'views/user/signup.html'
    })
    //edit
    .state('app.user.edit', {
      url: '/edit/{token}',
      controller: 'UserEditCtrl',
      templateUrl: 'views/user/edit.html'
    })
    //reporting
    .state('app.reporting', {
      url: '/reporting',
      controller: 'CalendarCtrl',
      templateUrl: 'views/calendar.html'
    })
    //app core pages (errors, login,signup)
    .state('core', {
      abstract: true,
      url: '/core',
      template: '<div ui-view></div>'
    })
    //login
    .state('core.login', {
      url: '/login',
      controller: 'LoginCtrl',
      templateUrl: 'views/pages/login.html'
    })
    //forgot password
    .state('core.forgotpass', {
      url: '/forgotpass',
      controller: 'ForgotPasswordCtrl',
      templateUrl: 'views/pages/forgotpass.html'
    })
    //page 404
    .state('core.page404', {
      url: '/page404',
      templateUrl: 'views/pages/page404.html'
    })
    //page 500
    .state('core.page500', {
      url: '/page500',
      templateUrl: 'views/pages/page500.html'
    })
    //page offline
    .state('core.page-offline', {
      url: '/page-offline',
      templateUrl: 'views/pages/page-offline.html'
    })
    //locked screen
    .state('core.locked', {
      url: '/locked',
      templateUrl: 'views/pages/locked.html',
      controller: 'UnlockCtrl'
    })
    //trace
    .state('app.trace', {
      url: '/trace',
      abstract: true,
      template: '<div ui-view></div>'
    })
    //trace list
    .state('app.trace.list', {
      url: '/list',
      controller: 'TraceCtrl',
      templateUrl: 'views/trace/list.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/mixitup/jquery.mixitup.js',
            'scripts/vendor/magnific/magnific-popup.css',
            'scripts/vendor/magnific/jquery.magnific-popup.min.js',
              'scripts/vendor/datatables/datatables.bootstrap.min.css',
              'scripts/vendor/datatables/Pagination/input.js',
              'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })
    //trace/single-order
    /*.state('app.trace.details', {
      url: '/details',
      controller: 'TraceDetailsCtrl',
      templateUrl: 'views/trace/show.html',
      resolve: {
        plugins: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'scripts/vendor/datatables/datatables.bootstrap.min.css',
            'scripts/vendor/datatables/Pagination/input.js',
            'scripts/vendor/datatables/ColumnFilter/jquery.dataTables.columnFilter.js'
          ]);
        }]
      }
    })*/
    //transaction show
    .state('app.trace.details', {
        url: '/details/{token}',
        controller: 'ShowTraceCtrl',
        templateUrl: 'views/trace/show.html'
    })
    .state('app.mail', {
        url: '/mail',
        abstract: true,
        template: '<div ui-view></div>'
    })
    //mail/compose
    .state('app.mail.compose', {
        url: '/compose',
        controller: 'MailCtrl',
        templateUrl: 'views/mail/compose.html'
    })

    //documentation
    .state('app.help', {
      url: '/help',
      controller: 'HelpCtrl',
      templateUrl: 'views/help.html'
    });
  }]);

