<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 27/03/2018
 * Time: 08:35
 */

// multiple recipients
$to  = 'aidan@example.com' . ', '; // note the comma
$to .= 'wez@example.com';

// subject
$subject = 'Birthday Reminders for August';

// message
$message = '
<html>
<head>
  <title>Birthday Reminders for August</title>
</head>
<body>
  <p>Here are the birthdays upcoming in August!</p>
</body>
</html>
';

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
$headers .= 'From: TransMobile <support@guedia.ovh> ' . "\r\n";
$headers .= 'Bcc: support@guedia.ovh' . "\r\n";

// Mail it
mail($to, $subject, $message, $headers);


?>